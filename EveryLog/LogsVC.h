//
//  FirstViewController.h
//  EveryLog
//
//  Created by Kamlesh on 04/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import "LoggersModal.h"

@interface LogsVC : RootViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate>
@property (nonatomic, strong) LoggersModal *loggersModal;
@end
