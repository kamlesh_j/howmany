//
//  AppDelegate.h
//  EveryLog
//
//  Created by Kamlesh on 04/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIColor (Support)

+ (NSString *) getColorHexFromString:(NSString *) rgbString;
+ (UIColor *) colorWithHexFromString:(NSString *) rgbString;
+ (UIColor *)colorWithRGBHex:(UInt32)hex;
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;

@end


