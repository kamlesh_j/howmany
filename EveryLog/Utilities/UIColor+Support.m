//
//  AppDelegate.h
//  EveryLog
//
//  Created by Kamlesh on 04/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "UIColor+Support.h"

@implementation UIColor (Support)

+ (NSString *) getColorHexFromString:(NSString *) rgbString {
	
	__block NSString *hexString = @"";
	[[rgbString componentsSeparatedByString:@" "] enumerateObjectsUsingBlock:^(NSString * str, NSUInteger idx, BOOL *stop) {
		//	NSString *dec = @"254";
		NSString *hex = [NSString stringWithFormat:@"%lX",
										 (unsigned long)[str integerValue]];
		
		hexString = [hexString stringByAppendingString:hex];
	}];
	
	hexString = [@"#" stringByAppendingString:hexString];
	
	return hexString;
}

+ (UIColor *) colorWithHexFromString:(NSString *) rgbString {
	
	__block NSString *hexString = @"";
	[[rgbString componentsSeparatedByString:@" "] enumerateObjectsUsingBlock:^(NSString * str, NSUInteger idx, BOOL *stop) {
		//	NSString *dec = @"254";
		NSString *hex = [NSString stringWithFormat:@"%lX",
										 (unsigned long)[str integerValue]];
		
		hexString = [hexString stringByAppendingString:hex];
	}];
	
	return [UIColor colorWithHexString:hexString];
}
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert {
	
	stringToConvert = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""];
	
	NSScanner *scanner = [NSScanner scannerWithString:stringToConvert];
	unsigned hexNum;
	if (![scanner scanHexInt:&hexNum]) return nil;
	return [UIColor colorWithRGBHex:hexNum];

}

+ (UIColor *)colorWithRGBHex:(UInt32)hex {
	int r = (hex >> 16) & 0xFF;
	int g = (hex >> 8) & 0xFF;
	int b = (hex) & 0xFF;
	
	return [UIColor colorWithRed:r / 255.0f
						   green:g / 255.0f
							blue:b / 255.0f
						   alpha:1.0f];
}

@end
