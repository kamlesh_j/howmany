//
// ECGraphItem.m
//
// Create By ECGenerateCode
// Date:2010-05-12 02:27:50
//
//!--
//--

#import "ECGraphItem.h"

@implementation ECGraphItem

@synthesize name;
@synthesize color;
@synthesize yValue;
@synthesize yDateValue;
@synthesize isYDate;
@synthesize isPercentage;
@synthesize width;
@synthesize defaultColor;
@synthesize dateString;
@synthesize month;
@synthesize week;
@synthesize weekDay;
@synthesize day;
@synthesize year;

- (id)init{
	if(self = [super init]){
		name = @"";
		color = nil;
		yDateValue = nil;
	}
	return self;
}

- (void)dealloc{
//	[name release];
//	[color release];
//	[yDateValue release];
//	[super dealloc];
}

@end