//
//  EventCell.m
//  EveryLog
//
//  Created by Kamlesh on 06/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "EventCell.h"
#import "Global.h"

@interface EventCell () {
		
	__weak IBOutlet UIView *eventDisplayView;
	__weak IBOutlet UIView *eventEntryView;

	__weak IBOutlet UIImageView *chooseImageView;
	
	__weak IBOutlet UILabel *addPhotoLbl;
	__weak IBOutlet UIImageView *uploadImageIcon;
	
	// EventEntryView Fields
	__weak IBOutlet UIButton *logItBtn;
	__weak IBOutlet UILabel *userNameLbl;
	__weak IBOutlet UITextField *venueTxt;
	__weak IBOutlet UITextField *eventNameTxt;
	__weak IBOutlet UITextField *locationTxt;
	__weak IBOutlet UITextField *dateTimeTxt;
	__weak IBOutlet UIImageView *currentUserImgIcon;
	
	__weak IBOutlet UIView *venueTxtBGView;
	__weak IBOutlet UIView *eventNameTxtBGView;
	__weak IBOutlet UIView *locationTxtBGView;
	__weak IBOutlet UIView *dateTimeTxtBGView;
	
	__weak IBOutlet UILabel *dateLbl;
	__weak IBOutlet UILabel *userName;
	__weak IBOutlet UILabel *logEventName;
	__weak IBOutlet UIImageView *userImgIcon;
	DisplayDatePickerBlock displayDatePickerBlock;
	__weak IBOutlet UILabel *commentLbl;
	__weak IBOutlet UIImageView *eventIcon;
	__weak IBOutlet UIButton *deleteBtn;
	__weak IBOutlet UITextField *commentTxt;
}

- (IBAction)logItAction:(id)sender;
- (IBAction)deleteAction:(id)sender;
- (IBAction)shareAction:(id)sender;

@end

@implementation EventCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
			[self.contentView.layer setCornerRadius:10.0];

    }
    return self;
}

- (void) setSelectedLogImage:(UIImage *)selectedLogImage_ {
	self.selectedLogImage = selectedLogImage_;
	[addPhotoLbl setHidden:YES];
	[uploadImageIcon setHidden:YES];
	[chooseImageView setImage:selectedLogImage_];
}
- (void) setDisplayDateBlock:(DisplayDatePickerBlock) displayDatePickerBlock_ {
	displayDatePickerBlock = displayDatePickerBlock_;
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch * touch = [touches anyObject];
	if (chooseImageView == [touch view]) {
		[_parentViewController displayPhotoPicker:self];
	}
}
-(void) setLogResultModal:(LogResultModal *)logResultModal {
	_logResultModal = logResultModal;
		
	if (![_logResultModal.userDTO.userID isEqualToString:GLOBAL.userInfo.userID]) {
    [deleteBtn setHidden:YES];
	} else {
		[deleteBtn setHidden:NO];
	}
	NSString *userNameStr = [NSString stringWithFormat:@"%@\n%@",_logResultModal.userDTO.firstName, _logResultModal.userDTO.lastName];
	userName.text = userNameStr;
	
	dateLbl.text = _logResultModal.creationTime;
	logEventName.text = _logResultModal.resultValue;
	commentLbl.text = _logResultModal.subTopic1Value;
	
	[userImgIcon setImage:nil];

	if (![nonNilString(_logResultModal.userDTO.profileImageUrl) isEqualToString:@""]) {
		__block  NSString *userImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_USERIMG,_logResultModal.userDTO.profileImageUrl];
		
		[IMAGEDOWNLOADER getImageForAPILink:userImgLink withCallback:^(UIImage *image, NSString *imgLink) {
			NSLog(@"userImgLink %@", userImgLink);
			NSLog(@"imgLink : %@", imgLink);
			if ([imgLink rangeOfString:_logResultModal.userDTO.profileImageUrl].location != NSNotFound) {
				dispatch_async(dispatch_get_main_queue(), ^{
					[userImgIcon setImage:image];
				});
			}
		}];
	}
	
	[eventIcon setImage:nil];

	if (_logResultModal.resultPicURL != nil) {
		__block  NSString *logImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_RESULTIMG,_logResultModal.resultPicURL];
		[IMAGEDOWNLOADER getImageForAPILink:logImgLink withCallback:^(UIImage *image, NSString *imgLink) {
			if ([imgLink rangeOfString:_logResultModal.resultPicURL].location != NSNotFound) {
				dispatch_async(dispatch_get_main_queue(), ^{
					[eventIcon setImage:image];
				});
			}
		}];
	}
	
	[self getImageResults];
}

-(void) getImageResults {
	return;
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOG_IMAGE_RESULT];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params setObject:ts forKey:@"ts"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	[request setHTTPMethod:@"GET"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setHTTPShouldHandleCookies:YES];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			NSArray *responseArray = responseDict[@"data"];
			if (responseArray.count) {
				NSDictionary *dict = responseArray[0];
				NSString *result = dict[@"resultPicURL"];
				NSLog(@"");
				
				__block  NSString *logImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_RESULTIMG,result];
				
				[IMAGEDOWNLOADER getImageForAPILink:logImgLink withCallback:^(UIImage *image, NSString *imgLink) {
					if ([_logResultModal.logId isEqualToString:dict[@"logId"]]) {
						_logResultModal.resultPicURL = imgLink;
					}
					dispatch_async(dispatch_get_main_queue(), ^{
						[eventIcon setImage:image];
					});
				}];
			}
		} else {
		}
	}];
}


- (void) drawRect:(CGRect)rect {
	[super drawRect:rect];
	[eventDisplayView.layer setCornerRadius:5.0];
	[eventEntryView.layer setCornerRadius:5.0];
	
	[eventDisplayView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[eventDisplayView.layer setBorderWidth:0.5];
	[eventEntryView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[eventEntryView.layer setBorderWidth:0.5];
	
	[logItBtn.layer setCornerRadius:5.0];

	[venueTxtBGView.layer setCornerRadius:5.0];
	[venueTxtBGView.layer setBorderWidth:1.0];
	[venueTxtBGView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	
	[eventNameTxtBGView.layer setCornerRadius:5.0];
	[eventNameTxtBGView.layer setBorderWidth:1.0];
	[eventNameTxtBGView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	
	[locationTxtBGView.layer setCornerRadius:5.0];
	[locationTxtBGView.layer setBorderWidth:1.0];
	[locationTxtBGView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	
	[dateTimeTxtBGView.layer setCornerRadius:5.0];
	[dateTimeTxtBGView.layer setBorderWidth:1.0];
	[dateTimeTxtBGView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void) resignFirstResponderForTxt {
	[venueTxt resignFirstResponder];
	[eventNameTxt resignFirstResponder];
	[locationTxt resignFirstResponder];
	[dateTimeTxt resignFirstResponder];
}

-(void) displayEventEntry {
	userNameLbl.text = [NSString stringWithFormat:@"%@\n%@",GLOBAL.userInfo.firstName, GLOBAL.userInfo.lastName];
	[self.contentView addSubview:eventEntryView];
	[eventEntryView setCenter:self.contentView.center];
	
	NSDate *date = NSDate.date;
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MM'/'dd'/'YY hh':'mm a"];
	dateTimeTxt.text =  [dateFormatter stringFromDate:date];

	if (![nonNilString(GLOBAL.userInfo.profileImageUrl) isEqualToString:@""]) {
		__block  NSString *userImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_USERIMG,GLOBAL.userInfo.profileImageUrl];
		[IMAGEDOWNLOADER getImageForAPILink:userImgLink withCallback:^(UIImage *image, NSString *imgLink) {
			NSLog(@"userImgLink %@", userImgLink);
			NSLog(@"imgLink : %@", imgLink);
			if ([imgLink rangeOfString:GLOBAL.userInfo.profileImageUrl].location != NSNotFound) {
				dispatch_async(dispatch_get_main_queue(), ^{
					[currentUserImgIcon setImage:image];
				});
			}
		}];
	}
}

-(void) displayEvent {
	[self.contentView addSubview:eventDisplayView];
	[eventDisplayView setCenter:self.contentView.center];
}
- (IBAction)logItAction:(id)sender {
	[APIMANAGER addLog:eventNameTxt.text
					andComment:commentTxt.text
					 withImage:self.selectedLogImage
						withDate:nil];
	eventNameTxt.text = @"";
	commentTxt.text = @"";
	[chooseImageView setImage:nil];
}

- (IBAction)deleteAction:(id)sender {
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure you want to delete the log entry?"
																										 delegate:self
																						cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
	[alertView show];
}

- (IBAction)shareAction:(id)sender {
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		[APIMANAGER deleteLogEntry:_logResultModal.logResultId];
	} else {
		// Do nothing..
	}
}
@end
