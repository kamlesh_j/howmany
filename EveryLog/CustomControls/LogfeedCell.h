//
//  LogfeedCell.h
//  EveryLog
//
//  Created by Kamlesh on 19/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogFeedModal.h"

@interface LogfeedCell : UITableViewCell <UIAlertViewDelegate> {
	LogFeedModal *_logFeedModal;
}

@property (nonatomic,strong) LogFeedModal *logFeedModal;
@end
