//
//  ActionpadButton.h
//  Howmany
//
//  Created by Kamlesh on 23/03/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionpadButton : UIButton

- (void) setIcons:(NSString *) imageName andTitle:(NSString *) titleString;

@end
