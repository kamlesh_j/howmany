//
//  ActionpadButton.m
//  Howmany
//
//  Created by Kamlesh on 23/03/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "ActionpadButton.h"

@implementation ActionpadButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setIcons:(NSString *) imageName andTitle:(NSString *) titleString {
	[self.layer setCornerRadius:5.0];
	[self.layer setBorderWidth:1.0];
	[self.layer setBorderColor:COLOR_APP_GREEN.CGColor];
	
	UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
	[iconView setFrame:CGRectMake((self.frame.size.width - 40)/2, 10, 40, 40)];
	if (imageName == nil) {
		[iconView setBackgroundColor:[UIColor grayColor]];
	}
	[self addSubview:iconView];

	UILabel *label = nil;
	if (imageName == nil) {
		label = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(iconView.frame), self.frame.size.width - 40, 40)];
	} else {
		label = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(iconView.frame) - 10, self.frame.size.width - 40, 40)];
	}

	label.numberOfLines = 2;
	label.textColor = COLOR_APP_GREEN;
	label.font = [UIFont fontWithName:FONT_REGULAR size:13.0];
	label.text = self.titleLabel.text;	
	[self addSubview:label];
	label.text = titleString;
	[label setTextAlignment:NSTextAlignmentCenter];
}

@end
