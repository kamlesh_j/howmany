//
//  BadgeUtilityView.h
//  EveryLog
//
//  Created by Kamlesh on 24/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	BadgeTypeIcon = 0,
	BadgeTypeColor
} BadgeType;

typedef void (^SelectedColorBlock) (int colorIndex);
typedef void (^SelectedBadgeBlock) (int badgeIndex);

@interface BadgeUtilityView : UIScrollView

- (void) initializeBadgeWithColor:(SelectedColorBlock) selectedColorBlock;
- (void) initializeBadgeWithIcon:(SelectedBadgeBlock) selectedBadgeBlock;

@end
