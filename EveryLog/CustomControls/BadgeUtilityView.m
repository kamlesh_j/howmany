//
//  BadgeUtilityView.m
//  EveryLog
//
//  Created by Kamlesh on 24/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "BadgeUtilityView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Support.h"

#define TAG_BADGE 333

@interface BadgeUtilityView () {
	SelectedColorBlock selectedColorBlock;
	SelectedBadgeBlock selectedBadgeBlock;
}

@end

@implementation BadgeUtilityView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void) initializeBadgeWithColor:(SelectedColorBlock) selectedColorBlock_ {
	
	selectedColorBlock = selectedColorBlock_;
	
	__block float badgeWidth = 23.0;
	__block float xOrigin = 5.0;
	__block float yOrigin = 5;
	__block int count = 0;
	__block float gap = 7.0;
	
 	NSString *settingPath = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
	NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:settingPath];
	NSArray *colorList = [settings objectForKey:@"BadgeColor"];
	
	[colorList enumerateObjectsUsingBlock:^(NSString *colorString, NSUInteger idx, BOOL *stop) {
		count ++;
		UIButton *iconBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		[iconBtn setBackgroundColor:[UIColor colorWithHexFromString:colorString]];
		[iconBtn addTarget:self action:@selector(selectedColor:)
			forControlEvents:UIControlEventTouchUpInside];
		
		[iconBtn setFrame:CGRectMake(xOrigin, yOrigin, badgeWidth, badgeWidth)];
		[iconBtn.layer setCornerRadius:iconBtn.frame.size.height/2];
		iconBtn.tag = TAG_BADGE + idx;
		[self addSubview:iconBtn];
		xOrigin = badgeWidth + xOrigin + gap;
		
		if (count >= 10) {
			count = 0;
			xOrigin = 5.0;
			yOrigin = badgeWidth + yOrigin + gap;
		}
	}];
	
	if ([colorList count] % 10) {
		yOrigin = badgeWidth + yOrigin + gap;
	}
	
	[self setContentSize:CGSizeMake(self.frame.size.width, yOrigin)];
	
}

- (void) selectedColor:(UIButton*) button {
	int colorIndex = button.tag - TAG_BADGE;
	selectedColorBlock(colorIndex);
}

- (void) initializeBadgeWithIcon:(SelectedBadgeBlock) selectedBadgeBlock_ {
	
	selectedBadgeBlock = selectedBadgeBlock_;
	
	int noOfBadge = 50;
	float badgeWidth = 35.0;
	float xOrigin = 7.0;
	float yOrigin = 5;
	int count = 0;
	for (int index = 0; index < 50; index++) {
		count ++;
		UIButton *iconBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		[iconBtn addTarget:self action:@selector(selectedTab:)
			forControlEvents:UIControlEventTouchUpInside];
		[iconBtn setBackgroundColor:[UIColor blackColor]];
		[iconBtn setFrame:CGRectMake(xOrigin, yOrigin, badgeWidth, badgeWidth)];
		[iconBtn.layer setCornerRadius:iconBtn.frame.size.height/2];
		iconBtn.tag = TAG_BADGE + index;
		[self addSubview:iconBtn];
		xOrigin = badgeWidth + xOrigin + 15.0;
		
		if (count >= 6) {
			count = 0;
			xOrigin = 7.0;
			yOrigin = badgeWidth + yOrigin + 15.0;
		}
	}
	
	if (noOfBadge % 6) {
		yOrigin = badgeWidth + yOrigin + 15.0;
	}
	
	[self setContentSize:CGSizeMake(self.frame.size.width, yOrigin)];

}



- (void) selectedTab:(UIButton*) badgeIconBtn {
	
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
