#import <UIKit/UIKit.h>

@interface UIButton (OlfinnCustomFont)
@property (nonatomic, copy) NSString* fontName;
@end


@implementation UIButton (OlfinnCustomFont)

- (NSString *)fontName {
    return self.titleLabel.font.fontName;
}

- (void)setFontName:(NSString *)fontName {
    self.titleLabel.font = [UIFont fontWithName:fontName size:self.titleLabel.font.pointSize];
}

@end

@interface UILabel (OlfinnCustomFont)
@property (nonatomic, copy) NSString* fontName;
@end

@implementation UILabel (OlfinnCustomFont)

- (NSString *)fontName {
    return self.font.fontName;
}

- (void)setFontName:(NSString *)fontName {
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end


@interface UITextField (OlfinnCustomFont)
@property (nonatomic, copy) NSString* fontName;
@end

@implementation UITextField (OlfinnCustomFont)

- (NSString *)fontName {
    return self.font.fontName;
}

- (void)setFontName:(NSString *)fontName {
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end