//
//  LogsCell.m
//  EveryLog
//
//  Created by Kamlesh on 21/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LogsCell.h"
#import "UIColor+Support.h"
#import "Global.h"

@interface LogsCell () {
	IBOutlet UILabel *rankLbl;
	IBOutlet UIButton *joinBtn;
	IBOutlet UILabel *logCount;
	IBOutlet UILabel *logTitle;
	IBOutlet UIImageView *logIcon;
	IBOutlet UIImageView *logImage;
	IBOutlet UIView *basView;
	ShareCallback shareCallback;
}
- (IBAction)shareAction:(id)sender;
- (IBAction)lockAction:(id)sender;
- (IBAction)starAction:(id)sender;
- (IBAction)joinBtnAction:(id)sender;
@end

@implementation LogsCell

@synthesize logsModal = _logsModal;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) setShareCallback:(ShareCallback)_shareCallback {
	shareCallback = _shareCallback;
}
- (void) drawRect:(CGRect)rect {
	[super drawRect:rect];
	[basView.layer setCornerRadius:5.0];
	[logIcon.layer setCornerRadius:logIcon.frame.size.width/2.0];
}


- (void) setLogsModal:(LogFeedModal *)logsModal {
	_logsModal = logsModal;
	
	rankLbl.text = [NSString stringWithFormat:@"Rank %d", _logsModal.rank];
	logCount.text = _logsModal.resultValue;
	logTitle.text = _logsModal.logTopic;
	[logIcon setBackgroundColor:[UIColor colorWithHexString:_logsModal.logColor]];
	
	if (_logType == LogTypeSelf) {
    [joinBtn setHidden:YES];
	}
}

- (void) setLogName:(NSString*) logName {
	[logTitle setText:logName];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)shareAction:(id)sender {
	shareCallback(_logsModal);
}

- (IBAction)lockAction:(id)sender {
}

- (IBAction)starAction:(id)sender {
    [APIMANAGER addFavouriteLog:_logsModal.logID withCallbackBlock:^(BOOL isSuccess, NSString *errorMessage) {
        NSString *message = nil;
		if (isSuccess) {
			message = @"Favourite added!";
            _logsModal.isfavorite = YES;
		} else {
			message = errorMessage;
		}
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:message delegate:nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil, nil];
		[alertView show];
		alertView = nil;
    }];
}

- (IBAction)joinBtnAction:(id)sender {
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
																											message:@"Send request to join this log?"
																										 delegate:self cancelButtonTitle:@"Send" otherButtonTitles:@"Cancel", nil];
	[alertView show];
	alertView = nil;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		[APIMANAGER logJoin:_logsModal.logID withCallbackBlock:^(BOOL isSuccess, NSString *errorMessage) {
			NSString *message = nil;
			if (isSuccess) {
				message = @"You have successsfull joined the log!";
			} else {
				message = errorMessage;
			}
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
																													message:message delegate:nil cancelButtonTitle:@"OK"
																								otherButtonTitles:nil, nil];
			[alertView show];
			alertView = nil;
		}];
	} else {
		// Do nothing..
	}
}
@end
