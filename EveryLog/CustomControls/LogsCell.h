//
//  LogsCell.h
//  EveryLog
//
//  Created by Kamlesh on 21/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogFeedModal.h"

typedef enum {
	LogTypeSelf = 1,
	LogTypeLogger
} LogType;

typedef void (^ShareCallback) (LogFeedModal *logsModal);

@interface LogsCell : UITableViewCell <UIAlertViewDelegate> {
	LogFeedModal *_logsModal;
}

@property (nonatomic) LogType logType;
@property (nonatomic, strong) LogFeedModal *logsModal;

- (void) setShareCallback:(ShareCallback)shareCallback;

@end
