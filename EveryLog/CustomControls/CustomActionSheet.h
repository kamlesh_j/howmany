//
//  CustomActionSheet.h
//  EveryLog
//
//  Created by Kamlesh on 07/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomActionSheet : UIActionSheet <UIActionSheetDelegate>

@end
