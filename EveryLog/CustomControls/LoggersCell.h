//
//  LoggersCell.h
//  EveryLog
//
//  Created by Kamlesh on 21/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoggersModal.h"

@interface LoggersCell : UITableViewCell {
	LoggersModal* _loggersModal;
}

@property (nonatomic, strong) LoggersModal* loggersModal;
@end
