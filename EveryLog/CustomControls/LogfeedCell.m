//
//  LogfeedCell.m
//  EveryLog
//
//  Created by Kamlesh on 19/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LogfeedCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Support.h"
#import "Global.h"
#import "LogResultModal.h"

@interface LogfeedCell() {
	
	IBOutlet UILabel *starLbl;
	IBOutlet UILabel *subTitleNameLbl;
	IBOutlet UILabel *subTitleLbl;
	IBOutlet UILabel *eventDate;
	IBOutlet UIImageView *eventImage;
	IBOutlet UILabel *eventNameLbl;
	IBOutlet UIButton *joinBtn;
	IBOutlet UILabel *creatorName;
	IBOutlet UIImageView *creatorIcon;
	IBOutlet UILabel *eventCount;
	IBOutlet UIImageView *eventIconImg;
	IBOutlet UIView *titleBg;
	IBOutlet UIView *baseView;
}

- (IBAction)joinBtnAction:(id)sender;
- (IBAction)twitterAction:(id)sender;
- (IBAction)facebookAction:(id)sender;
- (IBAction)commentAction:(id)sender;

@end

@implementation LogfeedCell

@synthesize logFeedModal = _logFeedModal;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
			[self.contentView.layer setCornerRadius:10.0];
    }
    return self;
}

-(void) setLogFeedModal:(LogFeedModal *)logFeedModal {
	_logFeedModal = logFeedModal;
	
	eventNameLbl.text = [_logFeedModal logTopic];
	[eventNameLbl setLineBreakMode:NSLineBreakByWordWrapping];
	creatorName.text = [NSString stringWithFormat:@"%@\n%@", _logFeedModal.firstName, _logFeedModal.lastName];
	eventDate.text = _logFeedModal.serverDate;
	eventCount.text = _logFeedModal.resultValue;
	[eventIconImg setBackgroundColor:[UIColor colorWithHexString:_logFeedModal.logColor]];
	
	CGSize textSize = [eventNameLbl.text sizeWithAttributes:@{ NSFontAttributeName : eventNameLbl.font}];
	
	if (textSize.width > eventNameLbl.frame.size.width) {
    
	}

	subTitleLbl.text = @"";
	subTitleNameLbl.text = @"";
	starLbl.text = @"";
	
	__block NSString *userImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_USERIMG,_logFeedModal.profileImageURL];
	
	[IMAGEDOWNLOADER getImageForAPILink:userImgLink withCallback:^(UIImage *image, NSString *imgLink) {
		//if ([imgLink isEqualToString:userImgLink]) {
			[creatorIcon setImage:image];
		//}
	}];
	
	[self getResults];
	[self getImageResults];
}

//"resultPicURL": "5236594ee4b0b6b12b682959.jpg",

-(void) getResults {
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOGDETAILS];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:_logFeedModal.ownerId forKey:@"userId"];
	[params setObject:_logFeedModal.logID forKey:@"logId"];
	[params setObject:_logFeedModal.ownerId forKey:@"contextOf"];
	[params setObject:@"0" forKey:@"end"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params setObject:ts forKey:@"ts"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
		
	[request setHTTPMethod:@"GET"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setHTTPShouldHandleCookies:YES];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			dispatch_async(dispatch_get_main_queue(), ^{
				//logResult
				BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
				if (isSuccess) {
					
					NSDictionary *dataDict = [responseDict objectForKey:@"data"];
					NSArray *logResult = [dataDict objectForKey:@"logResult"];
					
					NSMutableArray *logResultModals = [NSMutableArray arrayWithCapacity:[logResult count]];
					[logResult enumerateObjectsUsingBlock:^(NSDictionary* dict, NSUInteger idx, BOOL *stop) {
						if (dict[@"resultPicURL"]) {
							NSLog(@"");
						}
						LogResultModal* logResultModal = [[LogResultModal alloc] initWithDictionary:dict];
						[logResultModals addObject:logResultModal];
					}];
					
					NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"creationTime" ascending:NO];
					[logResultModals sortUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor,nil]];
					
					dispatch_async(dispatch_get_main_queue(), ^{
						if (logResultModals.count) {
							LogResultModal * logResultModal = logResultModals.firstObject;
							if ([logResultModal.resultValue isKindOfClass:[NSString class]]) {
								subTitleLbl.text = logResultModal.resultValue;
							} else {
								subTitleLbl.text = @"";
							}
							if ([logResultModal.subTopic1Value isKindOfClass:[NSString class]]) {
								subTitleNameLbl.text = logResultModal.subTopic1Value;
							} else {
								subTitleNameLbl.text = @"";
							}
							if ([logResultModal.subTopic2Value isKindOfClass:[NSString class]]) {
								starLbl.text = logResultModal.subTopic2Value;
							} else {
								starLbl.text = @"";
							}
						} else {
							subTitleLbl.text = @"";
							subTitleNameLbl.text = @"";
							starLbl.text = @"";
						}
					});
				} else {
				}
			});
		} else {
		}
	}];
}

-(void) getImageResults {
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOG_IMAGE_RESULT];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:_logFeedModal.logID forKey:@"logId"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params setObject:ts forKey:@"ts"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	[request setHTTPMethod:@"GET"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setHTTPShouldHandleCookies:YES];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			NSArray *responseArray = responseDict[@"data"];
			if (responseArray.count) {
				NSDictionary *dict = responseArray[0];
				NSString *result = dict[@"resultPicURL"];
				NSLog(@"");
				
				__block  NSString *logImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_RESULTIMG,result];
				
				[IMAGEDOWNLOADER getImageForAPILink:logImgLink withCallback:^(UIImage *image, NSString *imgLink) {
					//if ([imgLink isEqualToString:userImgLink]) {
					dispatch_async(dispatch_get_main_queue(), ^{
						[eventImage setImage:image];
					});
					//}
				}];
			}
		} else {
		}
	}];
}


- (void) drawRect:(CGRect)rect {
	[super drawRect:rect];
	[baseView.layer setCornerRadius:5.0];
	
	[eventIconImg.layer setCornerRadius:eventIconImg.frame.size.width/2];
	[titleBg.layer setCornerRadius:5.0];
	titleBg.layer.borderWidth = 0.5;
	[titleBg.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[titleBg setBackgroundColor:[UIColor clearColor]];
	
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)joinBtnAction:(id)sender {
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
																											message:@"Send request to join this log?"
																										 delegate:self cancelButtonTitle:@"Send" otherButtonTitles:@"Cancel", nil];
	[alertView show];
	alertView = nil;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		[APIMANAGER logJoin:_logFeedModal.logID withCallbackBlock:^(BOOL isSuccess, NSString *errorMessage) {
			NSString *message = nil;
			if (isSuccess) {
				message = @"Favourite added!";
			} else {
				message = errorMessage;
			}
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
																													message:message delegate:nil cancelButtonTitle:@"OK"
																								otherButtonTitles:nil, nil];
			[alertView show];
			alertView = nil;
		}];
	} else {
		// Do nothing..
	}
}

#pragma mark -


- (IBAction)twitterAction:(id)sender {
}

- (IBAction)facebookAction:(id)sender {
}

- (IBAction)commentAction:(id)sender {
}
@end
