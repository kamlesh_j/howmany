//
//  LoggersCell.m
//  EveryLog
//
//  Created by Kamlesh on 21/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LoggersCell.h"
#import "Global.h"
#import "LogFeedModal.h"
#import "UIColor+Support.h"

@interface LoggersCell () {
	
	IBOutlet UILabel *event3Name;
	IBOutlet UILabel *event2Name;
	IBOutlet UILabel *event1Name;
	IBOutlet UIImageView *event3Color;
	IBOutlet UIImageView *event2Color;
	IBOutlet UIImageView *event1Color;
	IBOutlet UILabel *userName;
	IBOutlet UIImageView *userImage;
	IBOutlet UIView *baseView;
}
@end

@implementation LoggersCell

@synthesize loggersModal = _loggersModal;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setLoggersModal:(LoggersModal *)loggersModal {
	_loggersModal = loggersModal;
	
	userName.text = [NSString stringWithFormat:@"%@ %@",_loggersModal.firstName, _loggersModal.lastName];
	[event1Color setHidden:YES];
	[event1Name setHidden:YES];
	[event2Color setHidden:YES];
	[event2Name setHidden:YES];
	[event3Color setHidden:YES];
	[event3Name setHidden:YES];

	[_loggersModal.logFeeds enumerateObjectsUsingBlock:^(LogFeedModal *logFeedModal, NSUInteger idx, BOOL *stop) {
		if (idx == 0) {
			[event1Color setHidden:NO];
			[event1Name setHidden:NO];
			event1Name.text = logFeedModal.logTopic;
			[event1Color setBackgroundColor:[UIColor colorWithHexString:logFeedModal.logColor]];
		} else if (idx == 1){
			[event2Color setHidden:NO];
			[event2Name setHidden:NO];
			event2Name.text = logFeedModal.logTopic;
			[event2Color setBackgroundColor:[UIColor colorWithHexString:logFeedModal.logColor]];
		} else if (idx == 2) {
			[event3Color setHidden:NO];
			[event3Name setHidden:NO];
			event3Name.text = logFeedModal.logTopic;
			[event3Color setBackgroundColor:[UIColor colorWithHexString:logFeedModal.logColor]];
		}
	}];
	
	__block NSString *userImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_USERIMG,_loggersModal.profileImageUrl];
	
	[IMAGEDOWNLOADER getImageForAPILink:userImgLink withCallback:^(UIImage *image, NSString *imgLink) {
		//if ([imgLink isEqualToString:userImgLink]) {
			[userImage setImage:image];
		//}
	}];
}
- (IBAction)addFavouriteAction:(id)sender {
	[APIMANAGER addFavouriteUser:_loggersModal.loggerID withCallbackBlock:^(BOOL isSuccess, NSString *errorMessage) {
		NSString *message = nil;
		if (isSuccess) {
			message = @"Favourite added!";
            _loggersModal.isfavorite = YES;
		} else {
			message = errorMessage;
		}
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
																												message:message delegate:nil cancelButtonTitle:@"OK"
																							otherButtonTitles:nil, nil];
		[alertView show];
		alertView = nil;
	}];
}

- (void) drawRect:(CGRect)rect {
	[super drawRect:rect];
	[baseView.layer setCornerRadius:5.0];
	
}
@end
