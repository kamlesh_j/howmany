//
//  CustomActionSheet.m
//  EveryLog
//
//  Created by Kamlesh on 07/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "CustomActionSheet.h"

@implementation CustomActionSheet

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
			[self setDelegate:self];
    }
    return self;
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
	for (UIView *subview in actionSheet.subviews) {
		if ([subview isKindOfClass:[UILabel class]]) {
			UILabel *actionLbl = (UILabel *)subview;
			[actionLbl setFont:[UIFont fontWithName:FONT_REGULAR size:15.0]];
		}
		if ([subview isKindOfClass:[UIButton class]]) {
			UIButton *button = (UIButton *)subview;
			[button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
		}
	}
}

- (void) layoutSubviews {
	// Drawing code
	for (UIView *subview in self.subviews) {
		if ([subview isKindOfClass:[UILabel class]]) {
			UILabel *actionLbl = (UILabel *)subview;
			[actionLbl setFont:[UIFont fontWithName:FONT_REGULAR size:15.0]];
		}
		if ([subview isKindOfClass:[UIButton class]]) {
			UIButton *button = (UIButton *)subview;
			[button.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:13.0]];
			[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		}
	}
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
