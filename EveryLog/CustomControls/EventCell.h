//
//  EventCell.h
//  EveryLog
//
//  Created by Kamlesh on 06/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogResultModal.h"
#import "EventsViewController.h"

typedef void (^DisplayDatePickerBlock) ();


@interface EventCell : UITableViewCell {
}

@property (nonatomic, strong) UIImage *selectedLogImage;
@property (nonatomic, weak) EventsViewController *parentViewController;
@property (nonatomic, strong) LogResultModal* logResultModal;

-(void) displayEventEntry;
-(void) displayEvent;
-(void) resignFirstResponderForTxt;
-(void) setDisplayDateBlock:(DisplayDatePickerBlock) displayDatePickerBlock_;

@end
