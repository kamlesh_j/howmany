//
//  FirstViewController.m
//  EveryLog
//
//  Created by Kamlesh on 04/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LogsVC.h"
#import "LogsCell.h"
#import "Global.h"
#import "CustomActionSheet.h"
#import "HeaderCellTableViewCell.h"

@interface LogsVC () {
	IBOutlet LogsCell* logsCell;
	IBOutlet UITableView *logsTable;
}

@property (nonatomic, strong) NSArray* logsList;
@property (nonatomic, strong) NSArray* favouriteLogs;

@end

@implementation LogsVC

- (void)viewDidLoad
{
	if (_loggersModal) {
    self.tabName = [NSString stringWithFormat:@"%@'s Logs",_loggersModal.firstName];
	} else {
		self.tabName = @"MY LOGS";
	}
	
    [super viewDidLoad];
	UIImage *selecetdImg = [UIImage imageNamed:@"logsTab.png"];
	[self.navigationController.tabBarItem setSelectedImage:selecetdImg];
	// Do any additional setup after loading the view, typically from a nib.
	
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(makeAPIcall)
//																							 name:N_LOG_DELETE object:nil];
	
	if (_loggersModal) {
    CGRect tableFrame = [logsTable frame];
		tableFrame.size.height += 49;
		[logsTable setFrame:tableFrame];
	}
	[logsTable setBackgroundColor:[UIColor clearColor]];
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self makeAPIcall];
	[logsTable reloadData];
	[logsTable setContentOffset:CGPointMake(0, 80)];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_ {
	
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGS_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogsList withSearchQuery:searchBar_.text];
	}
	
	[searchBar_ resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGS_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogsList withSearchQuery:nil];
	}
}

- (void) makeAPIcall {
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGS_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogsList withSearchQuery:nil];
	}
}
- (void) didDownloadData:(NSNotification *) notification {
	
	[GLOBAL removeActivity:self.view];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:N_LOGS_DOWNLOADED object:nil];
	
	self.logsList = (NSArray*) [notification object];
	[logsTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableView Delegates

#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
    return 80;
	} else {
		return 175;
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
    return 1;
	} else {
		if (_favouriteLogs != nil) {
			return _favouriteLogs.count;
		} else {
			return _logsList.count;
		}
	}
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.section == 0) {
		static NSString *cellID = @"CellIdentHeader";
    HeaderCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (cell == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"HeaderCellTableViewCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			cell = [loadedViews lastObject];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[cell setBackgroundColor:[UIColor clearColor]];
			[cell.favouriteBtn addTarget:self action:@selector(allFavoriteAction:) forControlEvents:UIControlEventValueChanged];
			[cell.searchBar setDelegate:self];
		}
		return cell;
	} else {
		static NSString *cellID = @"CellIdent";
		
		LogsCell *logsCell_ = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (logsCell_ == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"LogsCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			logsCell_ = [loadedViews lastObject];
			[logsCell_ setSelectionStyle:UITableViewCellSelectionStyleNone];
			[logsCell_.contentView setBackgroundColor:[UIColor clearColor]];
			[logsCell_ setBackgroundColor:[UIColor clearColor]];
		}
        if (self.isLoggersLog) {
            [logsCell_ setLogType:LogTypeLogger];
        } else {
            [logsCell_ setLogType:LogTypeSelf];
        }

		if (_favouriteLogs != nil) {
			[logsCell_ setLogsModal:_favouriteLogs[indexPath.row]];
		} else {
			[logsCell_ setLogsModal:[_logsList objectAtIndex:indexPath.row]];
		}
		
		[logsCell_ setShareCallback:^(LogFeedModal *logsModal) {
			CustomActionSheet *actionSheet = [[CustomActionSheet alloc] initWithTitle:@"Share your log" delegate:self
																															cancelButtonTitle:@"Cancel"
																												 destructiveButtonTitle:nil
																				
																															otherButtonTitles:
																				@"Post the log to your timeline",
																				@"Tweet the status of this log", nil];
			actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
			[actionSheet showInView:self.view];
			actionSheet = nil;
		}];
		return logsCell_;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	LogFeedModal *logModal = [_logsList objectAtIndex:indexPath.row];
	GLOBAL.selectedLogModal = logModal;
	
	NSString * storyboardName = @"Events";
	NSString * viewControllerID = @"EventsTab";
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	UITabBarController * tabBarController = (UITabBarController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	[self presentViewController:tabBarController animated:YES completion:nil];
	[tabBarController setSelectedIndex:2];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//	NSInteger actionSheetTag = actionSheet.tag;
//	NSString *selectedOption = [actionSheet buttonTitleAtIndex:buttonIndex];
}

- (IBAction) allFavoriteAction:(UISegmentedControl *) sender {
	
	if (sender.selectedSegmentIndex == 0) {
		self.favouriteLogs = nil;
		[logsTable reloadData];
	} else {
		
		NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(LogFeedModal* logFeedModal, NSDictionary *bindings) {
			//NSLog(@"question.questionID : %d [] [quesID intValue]: %d",question.questionID,[quesID intValue]);
			if (logFeedModal.isfavorite) {
				return YES;
			} else {
				return NO;
			}
		}];
		
		self.favouriteLogs = [_logsList filteredArrayUsingPredicate:predicate];
		[logsTable reloadData];
	}
}

@end
