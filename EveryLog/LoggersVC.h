//
//  SecondViewController.h
//  EveryLog
//
//  Created by Kamlesh on 04/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@interface LoggersVC : RootViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@end
