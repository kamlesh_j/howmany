//
//  SecondViewController.m
//  EveryLog
//
//  Created by Kamlesh on 04/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LoggersVC.h"
#import "LoggersCell.h"
#import "Global.h"
#import "LogsVC.h"
#import "HeaderCellTableViewCell.h"

@interface LoggersVC () {
	IBOutlet LoggersCell *loggersCell;
	NSArray *_loggersList;
	NSArray *_favouriteList;
	IBOutlet UITableView *loggersTable;
	
	UIColor *btnColor;
}
- (IBAction)allFavouriteAction:(UISegmentedControl *)sender;
@property (nonatomic, strong) NSArray *loggersList;
@property (nonatomic, strong) NSArray *favouriteList;
@end

@implementation LoggersVC

@synthesize loggersList = _loggersList;
@synthesize favouriteList = _favouriteList;

- (void)viewDidLoad
{
	self.tabName = @"LOGGERS";
	
	[super viewDidLoad];
	
	UIImage *selecetdImg = [UIImage imageNamed:@"loggersTab.png"];
	[self.navigationController.tabBarItem setSelectedImage:selecetdImg];
	// Do any additional setup after loading the view, typically from a nib.
	
	btnColor = [UIColor colorWithRed:(207.0/255.0) green:(42.0/255.0) blue:(39.0/255.0) alpha:1.0];
	[loggersTable setBackgroundColor:[UIColor clearColor]];

	[loggersTable setContentOffset:CGPointMake(0, 80)];

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_ {
	
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGGERS_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogggersSearch withSearchQuery:searchBar_.text];
	}
	
	[searchBar_ resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGGERS_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogggersSearch withSearchQuery:nil];
	}
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGGERS_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogggers withSearchQuery:nil];
	}
	[loggersTable reloadData];
	[loggersTable setContentOffset:CGPointMake(0, 80)];	
}

- (void) didDownloadData:(NSNotification*) notification {	
	[GLOBAL removeActivity:self.view];
	self.loggersList = (NSArray *) [notification object];
	[loggersTable reloadData];
}

#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
    return 80;
	} else {
		return 137;
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
    return 1;
	} else {
		if (_favouriteList != nil) {
			return [_favouriteList count];
		} else {
			return [_loggersList count];
		}
	}
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.section == 0) {
		static NSString *cellID = @"CellIdentHeader";
    HeaderCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (cell == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"HeaderCellTableViewCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			cell = [loadedViews lastObject];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[cell setBackgroundColor:[UIColor clearColor]];
			[cell.favouriteBtn addTarget:self action:@selector(allFavouriteAction:) forControlEvents:UIControlEventValueChanged];
			[cell.searchBar setDelegate:self];
		}
		return cell;
	} else {
		static NSString *cellID = @"CellIdent";
		
		LoggersCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (cell == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"LoggersCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			cell = [loadedViews lastObject];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[cell.contentView setBackgroundColor:[UIColor clearColor]];
			[cell setBackgroundColor:[UIColor clearColor]];
		}
		if (_favouriteList) {
			[cell setLoggersModal:[_favouriteList objectAtIndex:indexPath.row]];
		} else {
			[cell setLoggersModal:[_loggersList objectAtIndex:indexPath.row]];
		}
		
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
    return;
	}
	LoggersModal *loggersModal = nil;
	if (_favouriteList) {
		loggersModal = [_favouriteList objectAtIndex:indexPath.row];
	} else {
		loggersModal = [_loggersList objectAtIndex:indexPath.row];
	}

	GLOBAL.loggersModal = loggersModal;
	NSString * storyboardName = @"Main_iPhone";
	NSString * viewControllerID = @"NavMyLogs";
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	UINavigationController * controller = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	LogsVC *logVC = (LogsVC*)[controller topViewController];
	logVC.isLoggersLog = YES;
	[logVC setLoggersModal:loggersModal];
	[self presentViewController:controller animated:YES completion:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)allFavouriteAction:(UISegmentedControl *)sender {
	if (sender.selectedSegmentIndex == 0) {
		self.favouriteList = nil;
		[loggersTable reloadData];
	} else {
		NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(LoggersModal* loggersModal, NSDictionary *bindings) {
			//NSLog(@"question.questionID : %d [] [quesID intValue]: %d",question.questionID,[quesID intValue]);
			if (loggersModal.isfavorite) {
				return YES;
			} else {
				return NO;
			}
		}];
		
		self.favouriteList = [_loggersList filteredArrayUsingPredicate:predicate];
		[loggersTable reloadData];
	}
}

@end
