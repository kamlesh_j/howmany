//
//  Global.m
//  EveryLog
//
//  Created by Kamlesh on 18/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "Global.h"
#include <sys/time.h>
#import <FacebookSDK/FacebookSDK.h>

#define TAG_ACTIVITY 7836

@implementation Global


Global *GLOBAL;
FrameConstants *FRAMECONSTANTS;
APIManager* APIMANAGER;
ImageCacheManager *IMAGEDOWNLOADER;

@synthesize isMenuOpen;
@synthesize didLogin;
@synthesize userInfo;

#pragma mark -
#pragma mark Initialize

+(Global *)sharedGlobal{
	
	static Global *global = nil;
	
	if ( global == nil ) {
		global = [[Global alloc] init];
	}
	return global;
}

/*
 Initializes all the Singleton classes
 */
+ (void)initialize {
	GLOBAL = [Global sharedGlobal];
	FRAMECONSTANTS = [FrameConstants sharedFrameConstants];
	APIMANAGER = [APIManager sharedAPIManager];
	IMAGEDOWNLOADER = [ImageCacheManager sharedImageCacheManager];
	
	[FBLoginView class];
}

- (UserInfo *) getStoredUserInfo {
	NSData *userInfoData = [[NSUserDefaults standardUserDefaults] objectForKey:K_UserInfo];
	UserInfo *userInfoStored = [NSKeyedUnarchiver unarchiveObjectWithData:userInfoData];
	return userInfoStored;
}

- (void) storeUserInfo {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSData *userInfoData = [NSKeyedArchiver archivedDataWithRootObject:GLOBAL.userInfo];
	[userDefaults setObject:userInfoData forKey:K_UserInfo];
	[userDefaults synchronize];
}

- (void) clearLogin {
	[[FBSession activeSession] closeAndClearTokenInformation];
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];	
	[userDefaults removeObjectForKey:K_UserInfo];
	[userDefaults synchronize];
	self.userInfo = nil;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:N_SHOW_LOGIN object:nil];
}

- (NSString *) getTimeStamp {
	//
	struct timeval time;
	gettimeofday(&time, NULL);
	long millis = (time.tv_sec * 1000) + (time.tv_usec / 1000);
	
	NSString *ts = [NSString stringWithFormat:@"%ld",millis];
	return ts;
}

-(void) addActivityTo:(UIView *) view with:(UIActivityIndicatorViewStyle) activityIndicatorViewStyle {
	
	UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:activityIndicatorViewStyle];
	[activity setHidesWhenStopped:YES];
	activity.tag = TAG_ACTIVITY;
	[view addSubview:activity];
	[activity startAnimating];
	[activity setCenter:view.center];
}

-(void) removeActivity:(UIView *) view {
	UIActivityIndicatorView *activity = (UIActivityIndicatorView*)[view viewWithTag:TAG_ACTIVITY];
	[activity stopAnimating];
	[activity setHidden:YES];
	[activity removeFromSuperview];
	activity = nil;
}

- (NSString * ) setParameterFor:(NSString *) urlString andDict:(NSDictionary*) params {
	
	__block NSString* urlString_ = urlString;
	[params enumerateKeysAndObjectsUsingBlock:^(NSString* key, NSString* obj, BOOL *stop) {
		urlString_ = [urlString_ stringByAppendingFormat:@"&%@=%@",key,obj];
	}];
	
	if ([urlString_ rangeOfString:@"?&"].location != NSNotFound) {
		urlString_ = [urlString_ stringByReplacingOccurrencesOfString:@"?&" withString:@"?"];
	}
	return urlString_;
}

@end
