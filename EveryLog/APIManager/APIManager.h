//
//  APIManager.h
//  EveryLog
//
//  Created by Kamlesh on 25/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LogFeedModal.h"

typedef void (^APIManagerResponse) (BOOL isSuccess, NSString *message);

@interface APIManager : NSObject {
}

+ (APIManager *) sharedAPIManager;

- (void) doLogin:(NSString *) userName andPassword:(NSString*) password;
- (void) startContentForAPI:(APIType) apiType withSearchQuery:(NSString *) query;
- (BOOL) isMemberLogin;
- (BOOL) getMiniProfile;
- (void) addComment:(NSString *) comment;
- (void) addLog:(NSString *) logResultValue andComment:(NSString *) comment withImage:(UIImage *) logImage withDate:(NSDate*) logDate;
- (void) deleteLogEntry:(NSString *)  resultId;

- (void) createLogWith:(LogFeedModal*) logFeedModal;
- (void) logJoin:(NSString *) logId withCallbackBlock:(APIManagerResponse) apiManagerResponse;
- (void) addFavouriteUser:(NSString *) loggerId withCallbackBlock:(APIManagerResponse) apiManagerResponse;
- (void) addFavouriteLog:(NSString *) loggerId withCallbackBlock:(APIManagerResponse) apiManagerResponse;
- (void) fbLogin:(NSString *) userId and:(NSString *) accessToken;

@end
