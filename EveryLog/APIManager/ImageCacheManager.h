//
//  ImageCacheManager.h
//  EveryLog
//
//  Created by Kamlesh on 27/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ImageDownloadedCallback) (UIImage* image, NSString *imgLink);

@interface ImageCacheManager : NSObject

+ (ImageCacheManager *) sharedImageCacheManager;
- (UIImage *) getImageForAPILink:(NSString *) imageLink withCallback:(ImageDownloadedCallback) imageDownloadedBlock;
- (void) clearImageStorage;

@end
