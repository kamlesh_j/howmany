//
//  APIOperations.m
//  EveryLog
//
//  Created by Kamlesh on 25/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "APIOperations.h"
#import "Global.h"
#import "LogFeedModal.h"
#import "LoggersModal.h"
#import "LogResultModal.h"
#import "LogCommentsModel.h"

@interface APIOperations (){
	APIResponse _apiResponse;
}

@end

@implementation APIOperations

@synthesize apiType;
@synthesize downloadFailed;
@synthesize searchQuery = _searchQuery;

- (id) initWithAPIResponse:(APIResponse) apiResponse {
	
	self = [super init];
	
	if (self != nil) {
		_apiResponse = apiResponse;
	}
	return self;
}

- (void)main {
    NSLog(@"NSOperation : Main Block...");
	
	if (apiType == APITypeLogFeed) {
		[self getLogFeed];
	}else if (apiType == APITypeLogsList) {
		[self getLogLists];
	}else if (apiType == APITypeLogggers){
		[self getLoggersLists];
	} else if (apiType ==  APITypeLogggersSearch) {
		[self getLoggersSearchLists];
	}
	else if (apiType == APITypeResults) {
		[self getResults];
	} else if (apiType == APITypeLoggersForLog) {
		[self getLoggersForLog];
	} else if (apiType == APITypeLogComments) {
		[self getLogComments];
	} else if (apiType == APITypeDeleteLog) {
		[self deleteLog];
	} else if (apiType == APITypeLogChart) {
		[self getLogChart];
	}
}

- (void)cancelDownload {
	
}

-(NSData *) getDataFor:(NSString *) urlString withHTTPType:(NSString *) httpMethod {
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
                                                                cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                            timeoutInterval:30.0];
	
	NSError *error = nil;
	
	[request setHTTPMethod:httpMethod];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	[request setHTTPShouldHandleCookies:YES];
	
	
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	NSURLResponse *response;
	NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
	
	NSLog(@"Request : %@", [request description]);
	NSLog(@"Response : %@",responseData);
    
	
	return urlData;
}

- (void) getResults {
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOGDETAILS];
    
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	[params setObject:GLOBAL.userInfo.userID forKey:@"contextOf"];
	[params setObject:@"0" forKey:@"end"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params setObject:ts forKey:@"ts"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	NSData *urlData = [self getDataFor:urlString withHTTPType:@"GET"];
	
	NSError* error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	//logResult
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSArray *logResult = [dataDict objectForKey:@"logResult"];
		
		NSMutableArray *logResultModals = [NSMutableArray arrayWithCapacity:[logResult count]];
		[logResult enumerateObjectsUsingBlock:^(NSDictionary* dict, NSUInteger idx, BOOL *stop) {
			LogResultModal* logResultModal = [[LogResultModal alloc] initWithDictionary:dict];
			[logResultModals addObject:logResultModal];
		}];
		
		NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"creationTime" ascending:NO];
		[logResultModals sortUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor,nil]];
        
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOGRESULT_DOWNLOADED
                                                                object:logResultModals];
			_apiResponse(YES, apiType);
		});
	} else {
		_apiResponse(NO, apiType);
	}
	//
}

- (void) getLogComments {
	// userId=528d1b89e4b05fa307ce69ba&logId=50a7f82be4b08a4ce29d9758&end=0&contextOf=528d1b89e4b05fa307ce69ba&ts=1391666632028
	
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOG_COMMENTS];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	[params setObject:GLOBAL.userInfo.userID forKey:@"contextOf"];
	[params setObject:@"0" forKey:@"end"];
	[params setObject:[GLOBAL getTimeStamp] forKey:@"ts"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	
	NSData *urlData = [self getDataFor:urlString withHTTPType:@"GET"];
	
	NSError* error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSArray *logsList = [dataDict objectForKey:@"commentDTO"];
		
		NSMutableArray *logsModalList = [NSMutableArray arrayWithCapacity:[logsList count]];
		
		[logsList enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
			LogCommentsModel *logsModal = [[LogCommentsModel alloc] initWithCommentDTO:dict];
			[logsModalList addObject:logsModal];
		}];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOG_COMMENTS
                                                                object:logsModalList];
			_apiResponse(YES, apiType);
		});
		
	} else {
		_apiResponse(NO, apiType);
	}
    
}

-(void) getLoggersForLog {
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOGGERS_FOR_LOG];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	//[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	[params setObject:GLOBAL.userInfo.userID forKey:@"contextOf"];
	[params setObject:@"0" forKey:@"end"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params setObject:ts forKey:@"ts"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	
	NSData *urlData = [self getDataFor:urlString withHTTPType:@"GET"];
	
	NSError* error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSArray *logsList = [dataDict objectForKey:@"loggers"];
		
		NSMutableArray *logsModalList = [NSMutableArray arrayWithCapacity:[logsList count]];
		
		[logsList enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
			LogFeedModal *logsModal = [[LogFeedModal alloc] initWithDict:dict];
			[logsModalList addObject:logsModal];
		}];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOGGER_FOR_LOG
                                                                object:logsModalList];
			_apiResponse(YES, apiType);
		});
		
	} else {
		_apiResponse(NO, apiType);
	}
}

-(void) getLogChart {
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOG_CHART];
	
	//userId=528d1b89e4b05fa307ce69ba&logId=50a7f82be4b08a4ce29d9758&end=0&contextOf=528d1b89e4b05fa307ce69ba&ts=1396581174949
    
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	[params setObject:GLOBAL.userInfo.userID forKey:@"contextOf"];
	[params setObject:@"0" forKey:@"end"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params setObject:ts forKey:@"ts"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	
	NSData *urlData = [self getDataFor:urlString withHTTPType:@"GET"];
	
	NSError* error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSArray *logsList = [dataDict objectForKey:@"logResult"];
		
		NSMutableArray *logResultList = [NSMutableArray arrayWithCapacity:[logsList count]];
		
		[logsList enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
			LogResultModal *logsModal = [[LogResultModal alloc] initWithDictionary:dict];
			[logResultList addObject:logsModal];
		}];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOG_CHART
                                                                object:logResultList];
			_apiResponse(YES, apiType);
		});
		
	} else {
		_apiResponse(NO, apiType);
	}
	
}

#pragma mark -

-(void) deleteLog {
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_DELETE_LOG];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
    
	NSData *urlData = [self getDataFor:urlString withHTTPType:@"DELETE"];
	
	NSError* error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOG_DELETE
                                                                object:nil];
			_apiResponse(YES, apiType);
		});
		
	} else {
		_apiResponse(NO, apiType);
	}
}

#pragma mark -
#pragma mark Home screen API
-(void) getLoggersSearchLists {
	
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOGGERS_SEARCH];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:@"0" forKey:@"end"];
	[params setObject:GLOBAL.userInfo.userID forKey:@"contextOf"];
	[params setObject:self.searchQuery forKey:@"query"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params setObject:ts forKey:@"ts"];
	
	///&query=joe&end=0&contextOf=528d1b89e4b05fa307ce69ba&ts=1397944396340
    
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	NSData *urlData = [self getDataFor:urlString withHTTPType:@"GET"];
	
	NSError* error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSArray *logsList = [dataDict objectForKey:@"users"];
		
		NSMutableArray *loggersModalList = [NSMutableArray arrayWithCapacity:[logsList count]];
		
		[logsList enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
			LoggersModal *loggersModal = [[LoggersModal alloc] initWithDict:dict];
			[loggersModalList addObject:loggersModal];
		}];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOGGERS_DOWNLOADED
                                                                object:loggersModalList];
			_apiResponse(YES, apiType);
		});
		
	} else {
		_apiResponse(NO, apiType);
	}
}


-(void) getLoggersLists {
	
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOGGERS];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:@"0" forKey:@"end"];
	[params setObject:GLOBAL.userInfo.userID forKey:@"contextOf"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params setObject:ts forKey:@"ts"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	NSData *urlData = [self getDataFor:urlString withHTTPType:@"GET"];
	
	NSError* error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSArray *logsList = [dataDict objectForKey:@"users"];
		
		NSMutableArray *loggersModalList = [NSMutableArray arrayWithCapacity:[logsList count]];
		
		[logsList enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
			LoggersModal *loggersModal = [[LoggersModal alloc] initWithDict:dict];
			[loggersModalList addObject:loggersModal];
		}];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOGGERS_DOWNLOADED
                                                                object:loggersModalList];
			_apiResponse(YES, apiType);
		});
		
	} else {
		_apiResponse(NO, apiType);
	}
}



-(void) getLogLists {
	
	NSString *userID = GLOBAL.loggersModal?GLOBAL.loggersModal.loggerID : GLOBAL.userInfo.userID;
	NSMutableDictionary *params1 = [[NSMutableDictionary alloc] init];
	[params1 setObject:userID forKey:@"userId"];
	[params1 setObject:@"0" forKey:@"end"];
	[params1 setObject:userID forKey:@"contextOf"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params1 setObject:ts forKey:@"ts"];
	
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOGLIST];
	if (_searchQuery != nil) {
		urlString = [URL_SERVER stringByAppendingString:URL_LOGFEED_SEARCH];
		[params1 setObject:_searchQuery forKey:@"query"];
		[params1 setObject:@"log" forKey:@"mode"];
	} else {
		urlString = [URL_SERVER stringByAppendingString:URL_LOGLIST];
	}
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params1];
	
	NSURL *url1 = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] initWithURL:url1
                                                                 cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                             timeoutInterval:30.0];
	NSError *error1 = nil;
	
	[request1 setHTTPMethod:@"GET"];
	[request1 addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request1 addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	[request1 setHTTPShouldHandleCookies:YES];
	
	
	[request1 setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	NSURLResponse *response1;
	NSData *urlData1=[NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
	
	NSError *error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData1
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSArray *logsList = [dataDict objectForKey:@"logs"];
		
		NSMutableArray *logsModalList = [NSMutableArray arrayWithCapacity:[logsList count]];
		
		[logsList enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
			LogFeedModal *logsModal = [[LogFeedModal alloc] initWithDict:dict];
			[logsModalList addObject:logsModal];
		}];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOGS_DOWNLOADED
                                                                object:logsModalList];
			_apiResponse(YES, apiType);
		});
		
	} else {
		_apiResponse(NO, apiType);
	}
	NSString *responseData1 = [[NSString alloc]initWithData:urlData1 encoding:NSUTF8StringEncoding];
	
	NSLog(@"Log List Request : %@", [request1 description]);
	NSLog(@"Log List Response : %@",responseData1);
}



-(void) getLogFeed {
	
	NSMutableDictionary *params1 = [[NSMutableDictionary alloc] init];
	[params1 setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params1 setObject:@"0" forKey:@"end"];
	[params1 setObject:GLOBAL.userInfo.userID forKey:@"contextOf"];
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	[params1 setObject:ts forKey:@"ts"];
    
	NSString *urlString = nil;
    
	if (_searchQuery != nil) {
		urlString = [URL_SERVER stringByAppendingString:URL_LOGFEED_SEARCH];
		[params1 setObject:_searchQuery forKey:@"query"];
		[params1 setObject:@"logfeed" forKey:@"mode"];
	} else {
		urlString = [URL_SERVER stringByAppendingString:URL_LOGFEEDS];
	}
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params1];
	
	
	NSURL *url1 = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request1 = [[NSMutableURLRequest alloc] initWithURL:url1
                                                                 cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                             timeoutInterval:30.0];
	
	NSError *error1 = nil;
	
	[request1 setHTTPMethod:@"GET"];
	[request1 addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request1 addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	[request1 setHTTPShouldHandleCookies:YES];
	
	
	[request1 setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	NSURLResponse *response1;
	NSData *urlData1=[NSURLConnection sendSynchronousRequest:request1 returningResponse:&response1 error:&error1];
	
	NSError *error = nil;
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData1
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSArray *logsList = [dataDict objectForKey:@"logs"];
		
		NSMutableArray *logsModalList = [NSMutableArray arrayWithCapacity:[logsList count]];
		
		[logsList enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
			LogFeedModal *logsModal = [[LogFeedModal alloc] initWithDict:dict];
			[logsModalList addObject:logsModal];
		}];
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSNotificationCenter defaultCenter] postNotificationName:N_LOGFEED_DOWNLOADED
                                                                object:logsModalList];
			_apiResponse(YES, apiType);
		});
		
	} else {
		_apiResponse(NO, apiType);
	}
	NSString *responseData1 = [[NSString alloc]initWithData:urlData1 encoding:NSUTF8StringEncoding];
	
	NSLog(@"Log List Request : %@", [request1 description]);
	NSLog(@"Log List Response : %@",responseData1);
}

@end
