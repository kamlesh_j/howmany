//
//  APIOperations.h
//  EveryLog
//
//  Created by Kamlesh on 25/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^APIResponse) (BOOL isSuccess, APIType apiType);

@interface APIOperations : NSOperation {
	NSString *_searchQuery;
}
@property (nonatomic, strong) NSString *searchQuery;
@property (nonatomic) APIType apiType;
@property (nonatomic) BOOL downloadFailed;

- (id) initWithAPIResponse:(APIResponse) apiResponse;
- (void)cancelDownload;

@end
