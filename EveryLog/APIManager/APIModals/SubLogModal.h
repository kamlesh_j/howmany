//
//  SubLogModal.h
//  EveryLog
//
//  Created by Kamlesh on 07/02/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 result": {
 "topicName": null,
 "logUnit": "TEXT",
 "logMeasurement": "COUNT"
 }
 */
 
@interface SubLogModal : NSObject {
	NSString *topicName;
	NSString *logUnit;
	NSString *logMeasurement;
}

@property (nonatomic, strong) NSString *topicName;
@property (nonatomic, strong) NSString *logUnit;
@property (nonatomic, strong) NSString *logMeasurement;

-(id) initWithDict:(NSDictionary*) dict;
@end
