//
//  LoggersModal.h
//  EveryLog
//
//  Created by Kamlesh on 28/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoggersModal : NSObject {
	NSString *loggerID;
	NSString *userName;
	NSString *firstName;
	NSString *lastName;
	NSString *profileImageUrl;
	NSString *gender;
	NSString *facebookId;
	BOOL isfavorite;
	NSMutableArray *logFeeds;
}

@property(nonatomic, strong) NSString *loggerID;
@property(nonatomic, strong) NSString *userName;
@property(nonatomic, strong) NSString *firstName;
@property(nonatomic, strong) NSString *lastName;
@property(nonatomic, strong) NSString *profileImageUrl;
@property(nonatomic, strong) NSString *gender;
@property(nonatomic, strong) NSString *facebookId;
@property(nonatomic) BOOL isfavorite;
@property(nonatomic, strong) NSMutableArray *logFeeds;

/*"id": "507330b5e4b0674b9fdd8e96",
 "userName": "jzjzjz@gmail.com",
 "firstName": "Joe",
 "lastName": "Zaczyk",
 "profileImageUrl": "507330b5e4b0674b9fdd8e96.jpg",
 "gender": "MALE",
 "facebookId": "1068001845",
 "isfavorite": false,
 */


-(id) initWithDict:(NSDictionary *) loggersDict;

@end
