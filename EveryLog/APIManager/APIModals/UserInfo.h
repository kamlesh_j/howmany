//
//  UserInfo.h
//  EveryLog
//
//  Created by Kamlesh on 26/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject <NSCoding> {
}

@property(nonatomic) BOOL isSuccess;
@property(nonatomic, strong) NSString* userID ;
@property(nonatomic, strong) NSString* userName;
@property(nonatomic, strong) NSString* firstName;
@property(nonatomic, strong) NSString* lastName;
@property(nonatomic, strong) NSString* profileImageUrl;
@property(nonatomic, strong) UIImage *profileImage;
@property(nonatomic, strong) NSString* gender;
@property(nonatomic, strong) NSString* authToken;
@property(nonatomic, strong) NSString* facebookId;

-(id) initWithResponseData:(NSData*) responseData;
-(id) initWithUserDTO:(NSDictionary *) userDTO;

@end
