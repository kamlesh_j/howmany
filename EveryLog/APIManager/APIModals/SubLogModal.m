//
//  SubLogModal.m
//  EveryLog
//
//  Created by Kamlesh on 07/02/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "SubLogModal.h"

@implementation SubLogModal

@synthesize topicName;
@synthesize logUnit;
@synthesize logMeasurement;

-(id) initWithDict:(NSDictionary*) dict {
	
	self = [super init];
	/*
	 result": {
	 "topicName": null,
	 "logUnit": "TEXT",
	 "logMeasurement": "COUNT"
	 }
	 */
	if (self) {
		self.topicName = dict[@"topicName"];
		self.logUnit = dict[@"logUnit"];
		self.logMeasurement = dict[@"logMeasurement"];
	}
	
	return self;
}
@end
