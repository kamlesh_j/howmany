//
//  LogResultModal.m
//  EveryLog
//
//  Created by Kamlesh on 02/02/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LogResultModal.h"


@implementation LogResultModal 

- (id) initWithDictionary:(NSDictionary *) dictionary {
	
	self = [super init];
	
	if (self) {
		/*
		 "id": "52ee599ae4b06a69fd39305a",
		 "loggerId": "52b8b348e4b0d3fbfb53cff6",
		 "logId": "50a7f82be4b08a4ce29d9758",
		 "creationTime": 1391274000000,
		 "resultValue": 10.0,
		 "subTopic1Value": "knees",
		 "subTopic2Value": null,
		 "resultPicURL": "",
		 */
		self.logResultId = [dictionary objectForKey:@"id"];
		self.loggerId = [dictionary objectForKey:@"loggerId"];
		self.logId = [dictionary objectForKey:@"logId"];
		
		double doubleValue = [[dictionary objectForKey:@"creationTime"] doubleValue];
		NSDate *date = [NSDate dateWithTimeIntervalSince1970:(doubleValue/1000.0)];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"MM'/'dd'/'YY hh':'mm a"];
		self.creationTime =  [dateFormatter stringFromDate:date];

		
		if ([[dictionary objectForKey:@"resultValue"] isKindOfClass:[NSNumber class]]) {
				self.resultValue = [[dictionary objectForKey:@"resultValue"] stringValue];
		} else {
				self.resultValue = [dictionary objectForKey:@"resultValue"];
		}

		
		self.subTopic1Value = [self getStringValue:dictionary forKey:@"subTopic1Value"];
		self.subTopic2Value = [self getStringValue:dictionary forKey:@"subTopic2Value"];
		self.resultPicURL = [self getStringValue:dictionary forKey:@"resultPicURL"];

		
		NSDictionary *logDTODict = [dictionary objectForKey:@"logDTO"];
		self.logDTO = [[LogFeedModal alloc] initWithDict:logDTODict];
		
		NSDictionary *userDTODict = [dictionary objectForKey:@"userDTO"];
		self.userDTO = [[UserInfo alloc] initWithUserDTO:userDTODict];
	}
	
	return self;
}

- (NSString *) getStringValue:(NSDictionary *) dict forKey:(NSString *) key {

	id valueForKey = dict[key];
	if ([valueForKey isKindOfClass:[NSNumber class]]) {
    return [valueForKey stringValue];
	} else if ([valueForKey isKindOfClass:[NSNull class]]) {
		return nil;
	} else if ([valueForKey isKindOfClass:[NSString class]]) {
		return valueForKey;
	}
	return valueForKey;
}

@end
