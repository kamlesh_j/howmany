//
//  LogResultModal.h
//  EveryLog
//
//  Created by Kamlesh on 02/02/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LogFeedModal.h"
#import "UserInfo.h"

@interface LogResultModal : NSObject {
	
}

@property (nonatomic, strong) NSString *logResultId;
@property (nonatomic, strong) NSString *loggerId;
@property (nonatomic, strong) NSString *logId;
@property (nonatomic, strong) NSString *creationTime;
@property (nonatomic, strong) NSString *resultValue;
@property (nonatomic, strong) NSString *subTopic1Value;
@property (nonatomic, strong) NSString *subTopic2Value;
@property (nonatomic, strong) NSString *resultPicURL;

@property (nonatomic, strong) LogFeedModal *logDTO;
@property (nonatomic, strong) UserInfo *userDTO;

// Added from ResultImage call
@property (nonatomic, strong) NSString *resultImageURL;

- (id) initWithDictionary:(NSDictionary *) dictionary;

@end
