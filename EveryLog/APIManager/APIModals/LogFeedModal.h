//
//  LogsModal.h
//  EveryLog
//
//  Created by Kamlesh on 26/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubLogModal.h"

@interface LogFeedModal : NSObject {

	NSString* logID;
	NSString* ownerId;
 	NSString* logTopic;
	NSString* logIcon;
	NSString* logColor;
	NSString* type;
	NSString* visibility;
	NSString* resultValue;
	int rank;
	BOOL isfavorite;
	NSString* firstName;
	NSString* lastName;
	NSString* profileImageURL;
	NSString* userId;
	NSString* gender;
	NSString* serverDate;
	
	NSString* logUnit;
	NSString* logMeasurement;
	
	SubLogModal* resultLog;
	SubLogModal* subTopic1;
	SubLogModal* subTopic2;
	
	// For Create Log
	NSString *logTopicGoal;
	NSMutableArray *tags;
}

@property(nonatomic, strong) NSString* logUnit;
@property(nonatomic, strong) NSString* logMeasurement;

@property(nonatomic, strong) NSString *logTopicGoal;
@property(nonatomic, strong) NSMutableArray *tags;

@property(nonatomic, strong) SubLogModal* resultLog;
@property(nonatomic, strong) SubLogModal* subTopic1;
@property(nonatomic, strong) SubLogModal* subTopic2;

@property(nonatomic, strong) NSString* serverDate;

@property(nonatomic, strong) NSString* logID;
@property(nonatomic, strong) NSString* ownerId;
@property(nonatomic, strong) NSString* logTopic;
@property(nonatomic, strong) NSString* logIcon;
@property(nonatomic, strong) NSString* logColor;
@property(nonatomic, strong) NSString* type;
@property(nonatomic, strong) NSString* visibility;
@property(nonatomic) NSString* resultValue;
@property(nonatomic) int rank;
@property(nonatomic) BOOL isfavorite;
@property(nonatomic, strong) NSString* firstName;
@property(nonatomic, strong) NSString* lastName;
@property(nonatomic, strong) NSString* profileImageURL;
@property(nonatomic, strong) NSString* userId;
@property(nonatomic, strong) NSString* gender;

/*
 "id": "50a023dde4b07762529d7a22",

 "ownerId": "507330b5e4b0674b9fdd8e96",
 "result": {
 "topicName": null,
 "logUnit": "TEXT",
 "logMeasurement": "COUNT"
 },
 "subTopic1": {
 "topicName": "Director",
 "logUnit": "TEXT",
 "logMeasurement": null
 },
 "subTopic2": {
 "topicName": "Stars 1-5",
 "logUnit": "TEXT",
 "logMeasurement": null
 },
 "logTopicGoal": "",
 "endDate": null,
 "serverDate": 1390764018799,
 "resultValue": "18",
 "rank4Logger": "50957d36e4b0cdf88810d9c2",
 "rank": 8,
 "isfavorite": false,
 "hasUpdates": false,
 "lastUpdatedBy": "50957d36e4b0cdf88810d9c2",
 "firstName": "Joseph",
 "lastName": "Zaczyk",
 "userId": "50957d36e4b0cdf88810d9c2",
 "profileImageURL": "50957d36e4b0cdf88810d9c2.jpg",
 "gender": "MALE"
 */
-(id) initWithDict:(NSDictionary *) dictionary;

@end
