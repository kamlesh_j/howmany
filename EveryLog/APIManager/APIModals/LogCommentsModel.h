//
//  LogCommentsModel.h
//  EveryLog
//
//  Created by Kamlesh on 06/02/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"

@interface LogCommentsModel : NSObject  {
	/*
	 "id": "52e7d604e4b024091c847436",
	 "creationTime": 1390925316035,
	 "comment": "welcome Kamlesh",
	 "logOwnerId": "509421a5e4b0b4ec662fa712",
	 "userDTO": {
	 "id": "509421a5e4b0b4ec662fa712",
	 "userName": "mjz157@hotmail.com",
	 "firstName": "Mike",
	 "lastName": "Zaczyk",
	 "profileImageUrl": "509421a5e4b0b4ec662fa712.jpg",
	 "gender": "MALE",
	 "facebookId": null
	 }
	 */
	NSString *commentId;
	NSString *creationTime;
	NSString *comment;
	NSString *logOwnerId;
	UserInfo *userInfo;
}

@property (nonatomic, strong) NSString *commentId;
@property (nonatomic, strong) NSString *creationTime;
@property (nonatomic, strong) NSString *comment;
@property (nonatomic, strong) NSString *logOwnerId;
@property (nonatomic, strong) UserInfo *userInfo;

- (id) initWithCommentDTO:(NSDictionary *) dict;

@end
