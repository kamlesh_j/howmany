//
//  LoggersModal.m
//  EveryLog
//
//  Created by Kamlesh on 28/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LoggersModal.h"
#import "LogFeedModal.h"

@implementation LoggersModal


@synthesize loggerID;
@synthesize userName;
@synthesize firstName;
@synthesize lastName;
@synthesize profileImageUrl;
@synthesize gender;
@synthesize facebookId;
@synthesize isfavorite;
@synthesize logFeeds;

-(id) initWithDict:(NSDictionary *) loggersDict {
	
	self = [super init];
	
	if (self) {
		
		self.loggerID = [loggersDict objectForKey:@"id"];
		self.userName = [loggersDict objectForKey:@"userName"];
		self.firstName = [loggersDict objectForKey:@"firstName"];
		self.lastName = [loggersDict objectForKey:@"lastName"];
		self.profileImageUrl = [loggersDict objectForKey:@"profileImageUrl"];
		self.gender = [loggersDict objectForKey:@"gender"];
		self.facebookId = [loggersDict objectForKey:@"facebookId"];
		self.isfavorite = [[loggersDict objectForKey:@"isfavorite"] boolValue];
		
		NSArray *logfeedDicts = [loggersDict objectForKey:@"logDTO"];
		
		if ([logfeedDicts isKindOfClass:[NSArray class]]) {
			self.logFeeds = [NSMutableArray arrayWithCapacity:[logfeedDicts count]];
			
			[logfeedDicts enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
				LogFeedModal *logFeedModal = [[LogFeedModal alloc] initWithDict:dict];
				[self.logFeeds addObject:logFeedModal];
			}];
		} else if ([logfeedDicts isKindOfClass:[NSArray class]]) {
			self.logFeeds = [NSMutableArray arrayWithCapacity:1];
			LogFeedModal *logFeedModal = [[LogFeedModal alloc] initWithDict:(NSDictionary*)logfeedDicts];
			[self.logFeeds addObject:logFeedModal];
		}
		
		/*"id": "507330b5e4b0674b9fdd8e96",
		 "userName": "jzjzjz@gmail.com",
		 "firstName": "Joe",
		 "lastName": "Zaczyk",
		 "profileImageUrl": "507330b5e4b0674b9fdd8e96.jpg",
		 "gender": "MALE",
		 "facebookId": "1068001845",
		 "isfavorite": false,
		 */
	}
	
	return self;
}

@end
