//
//  UserInfo.m
//  EveryLog
//
//  Created by Kamlesh on 26/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "UserInfo.h"
#import "Global.h"

@implementation UserInfo


-(id) initWithUserDTO:(NSDictionary *) userDTO {
	self = [super init];
	if (self) {
		//		NSDictionary *userDTO = [userTokenDTO objectForKey:@"userDTO"];
		self.userID = [userDTO objectForKey:@"id"];
		self.userName = [userDTO objectForKey:@"userName"];
		self.profileImageUrl = [userDTO objectForKey:@"profileImageUrl"];
		self.gender = [userDTO objectForKey:@"gender"];
		self.facebookId = [userDTO objectForKey:@"facebookId"];
		
		self.firstName = [userDTO objectForKey:@"firstName"];
		self.lastName = [userDTO objectForKey:@"lastName"];

//		if (![nonNilString(self.profileImageUrl) isEqualToString:@""]) {
//			__block  NSString *userImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_USERIMG,self.profileImageUrl];
//			[IMAGEDOWNLOADER getImageForAPILink:userImgLink withCallback:^(UIImage *image, NSString *imgLink) {
//				if ([imgLink rangeOfString:self.profileImageUrl].location != NSNotFound) {
//					self.profileImage = image;
//				}
//			}];
//		}
	}
	return self;
}

-(id) initWithResponseData:(NSData*) responseData {
	
	self = [super init];
	if (self) {
		// Initialization code
		/*
		 {
		 "success": true,
		 "errorString": null,
		 "data":
		 {
			 "serverTimezoneOffset": 360,
			 "userTokenDTO":
			 {
				 "userDTO":
				 {
					 "id": "528d1b89e4b05fa307ce69ba",
					 "userName": "kamleshkatios@gmail.com",
					 "firstName": "Kamlesh",
					 "lastName": "Jain",
					 "profileImageUrl": null,
					 "gender": "MALE",
					 "facebookId": null
				 },
				 "authToken": "a2FtbGVzaGthdGlvc0BnbWFpbC5jb206cG9va2FtMzEwMA=="
			 },
			 "firstLogin": true
		 }
		 }
		 */
		NSError *error = nil;
		NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData
																																 options:NSJSONReadingMutableContainers
																																	 error:&error];
		
		self.isSuccess = [[responseDict objectForKey:@"success"] boolValue];
		NSDictionary *dataDict = [responseDict objectForKey:@"data"];
		NSDictionary *userTokenDTO = [dataDict objectForKey:@"userTokenDTO"];
		
		NSDictionary *userDTO = [userTokenDTO objectForKey:@"userDTO"];
		self.userID = [userDTO objectForKey:@"id"];
		self.userName = [userDTO objectForKey:@"userName"];
		self.profileImageUrl = [userDTO objectForKey:@"profileImageUrl"];
		self.gender = [userDTO objectForKey:@"gender"];
		self.facebookId = [userDTO objectForKey:@"facebookId"];
		
		self.firstName = [userDTO objectForKey:@"firstName"];
		self.lastName = [userDTO objectForKey:@"lastName"];

		self.authToken = [userTokenDTO objectForKey:@"authToken"];
		
		if (![nonNilString(self.profileImageUrl) isEqualToString:@""]) {
			__block  NSString *userImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_USERIMG,self.profileImageUrl];
			[IMAGEDOWNLOADER getImageForAPILink:userImgLink withCallback:^(UIImage *image, NSString *imgLink) {
				if ([imgLink rangeOfString:self.profileImageUrl].location != NSNotFound) {
					self.profileImage = image;
				}
			}];
		}		
	}
	
	return self;
	
}

- (void)encodeWithCoder:(NSCoder *)enCoder {
	
	[enCoder encodeObject:_userID forKey:@"userID"];
	[enCoder encodeObject:_userName forKey:@"userName"];
	[enCoder encodeObject:_firstName forKey:@"firstName"];
	[enCoder encodeObject:_lastName forKey:@"lastName"];
	[enCoder encodeObject:_profileImageUrl forKey:@"profileImageUrl"];
	[enCoder encodeObject:_profileImage forKey:@"profileImage"];
	[enCoder encodeObject:_gender forKey:@"gender"];
	[enCoder encodeObject:_authToken forKey:@"authToken"];
	[enCoder encodeObject:_facebookId forKey:@"facebookId"];
	
}
- (id)initWithCoder:(NSCoder *)aDecoder {
	
	if(self = [super init]) {
		self.userID = [aDecoder decodeObjectForKey:@"userID"];
		self.userName = [aDecoder decodeObjectForKey:@"userName"];
		self.firstName = [aDecoder decodeObjectForKey:@"firstName"];
		self.lastName = [aDecoder decodeObjectForKey:@"lastName"];
		self.profileImageUrl = [aDecoder decodeObjectForKey:@"profileImageUrl"];
		self.profileImage = [aDecoder decodeObjectForKey:@"profileImage"];
		self.gender = [aDecoder decodeObjectForKey:@"gender"];
		self.authToken = [aDecoder decodeObjectForKey:@"authToken"];
		self.facebookId = [aDecoder decodeObjectForKey:@"facebookId"];
	}
	
	return self;
}

+ (BOOL)supportsSecureCoding {
	return YES;
}

@end
