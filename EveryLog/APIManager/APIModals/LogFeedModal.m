//
//  LogsModal.m
//  EveryLog
//
//  Created by Kamlesh on 26/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LogFeedModal.h"

@implementation LogFeedModal

@synthesize logID;
@synthesize ownerId;
@synthesize logTopic;
@synthesize logIcon;
@synthesize logColor;
@synthesize type;
@synthesize visibility;
@synthesize resultValue;
@synthesize rank;
@synthesize isfavorite;
@synthesize firstName;
@synthesize lastName;
@synthesize profileImageURL;
@synthesize userId;
@synthesize gender;
@synthesize serverDate;

@synthesize resultLog;
@synthesize subTopic1;
@synthesize subTopic2;

@synthesize logTopicGoal;
@synthesize tags;

@synthesize logUnit;
@synthesize logMeasurement;

-(id) initWithDict:(NSDictionary *) dictionary {
	
	self = [super init];
	
	if (self != nil) {
		
		self.logID = [dictionary objectForKey:@"id"];
		self.ownerId = [dictionary objectForKey:@"ownerId"];
		self.logTopic = [dictionary objectForKey:@"logTopic"];
		self.logIcon = [dictionary objectForKey:@"logIcon"];
		self.logColor = [dictionary objectForKey:@"logColor"];
		self.type = [dictionary objectForKey:@"type"];
		self.visibility = [dictionary objectForKey:@"visibility"];

		if (![[dictionary objectForKey:@"resultValue"] isKindOfClass:[NSNull class]]) {
			if ([dictionary[@"resultValue"] isKindOfClass:[NSString class]]) {
				self.resultValue = [dictionary objectForKey:@"resultValue"];
			} else {
				self.resultValue = [[dictionary objectForKey:@"resultValue"] stringValue];
			}
		}
		
		if (![[dictionary objectForKey:@"rank"] isKindOfClass:[NSNull class]]) {
			self.rank = [[dictionary objectForKey:@"rank"] intValue];
		}
		
		self.isfavorite = [[dictionary objectForKey:@"isfavorite"] boolValue];
		self.firstName  = [dictionary objectForKey:@"firstName"];
		self.lastName = [dictionary objectForKey:@"lastName"];
		self.userId = [dictionary objectForKey:@"userId"];
		self.profileImageURL = [dictionary objectForKey:@"profileImageURL"];
		self.gender = [dictionary objectForKey:@"gender"];
		self.isfavorite = [[dictionary objectForKey:@"isfavorite"] boolValue];

		NSString* refDate = [dictionary objectForKey:@"serverDate"];

		if (refDate != nil && ![refDate isKindOfClass:[NSNull class]]) {
//			NSDate *logDate = [NSDate dateWithTimeIntervalSinceNow:[refDate longLongValue]];
//			NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//			[dateFormatter setDateFormat:@"dd'/'MM'/'YY hh':'mm a"];
//			self.serverDate = [dateFormatter stringFromDate:logDate];
			
			double doubleValue = [refDate doubleValue];
			NSDate *date = [NSDate dateWithTimeIntervalSince1970:(doubleValue/1000.0)];
			NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
			[dateFormatter setDateFormat:@"MM'/'dd'/'YY hh':'mm a"];
			self.serverDate = [dateFormatter stringFromDate:date];

		}
		/*
		 "id": "50a023dde4b07762529d7a22",
		 "logTopic": "Movies",
		 "logIcon": "icon_box_c xy_206",
		 "logColor": "#c34a2c",
		 "ownerId": "507330b5e4b0674b9fdd8e96",
		 "type": "COMPETI",
		 "visibility": "PUBLICLOG",
		 "logTopicGoal": "",
		 "endDate": null,
		 "serverDate": 1390764018799,
		 "resultValue": "18",
		 "rank4Logger": "50957d36e4b0cdf88810d9c2",
		 "rank": 8,
		 "isfavorite": false,
		 "hasUpdates": false,
		 "lastUpdatedBy": "50957d36e4b0cdf88810d9c2",
		 "firstName": "Joseph",
		 "lastName": "Zaczyk",
		 "userId": "50957d36e4b0cdf88810d9c2",
		 "profileImageURL": "50957d36e4b0cdf88810d9c2.jpg",
		 "gender": "MALE"
		 */

		self.logUnit = dictionary[@"result"][@"logUnit"];
		self.logMeasurement = dictionary[@"result"][@"logMeasurement"];
		
		self.subTopic1 = [[SubLogModal alloc] initWithDict:dictionary[@"subTopic1"]];
		self.subTopic2 = [[SubLogModal alloc] initWithDict:dictionary[@"subTopic2"]];
		
		/*
		 "result": {
		 "topicName": null,
		 "logUnit": "TEXT",
		 "logMeasurement": "COUNT"
		 },
		 "subTopic1": {
		 "topicName": "Provider",
		 "logUnit": "TEXT",
		 "logMeasurement": null
		 },
		 "subTopic2": {
		 "topicName": "Format",
		 "logUnit": "TEXT",
		 "logMeasurement": null
		 },
		 */
		
		
	}
	return self;
}
@end
