//
//  LogCommentsModel.m
//  EveryLog
//
//  Created by Kamlesh on 06/02/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LogCommentsModel.h"

@implementation LogCommentsModel


@synthesize commentId;
@synthesize creationTime;
@synthesize comment;
@synthesize logOwnerId;
@synthesize userInfo;

/*
 "id": "52e7d604e4b024091c847436",
 "creationTime": 1390925316035,
 "comment": "welcome Kamlesh",
 "logOwnerId": "509421a5e4b0b4ec662fa712",
 "userDTO": {
 "id": "509421a5e4b0b4ec662fa712",
 "userName": "mjz157@hotmail.com",
 "firstName": "Mike",
 "lastName": "Zaczyk",
 "profileImageUrl": "509421a5e4b0b4ec662fa712.jpg",
 "gender": "MALE",
 "facebookId": null
 }
 */
- (id) initWithCommentDTO:(NSDictionary *) dict {
	
	self = [super init];
	
	if (self) {
		self.commentId = [dict objectForKey:@"id"];
		//self.creationTime = [dict objectForKey:@"creationTime"];

		double doubleValue = [[dict objectForKey:@"creationTime"] doubleValue];
		NSDate *date = [NSDate dateWithTimeIntervalSince1970:(doubleValue/1000.0)];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"MM'/'dd'/'YY\nhh':'mm a"];
		self.creationTime =  [dateFormatter stringFromDate:date];
		
		self.comment  = [dict objectForKey:@"comment"];
		self.logOwnerId = [dict objectForKey:@"logOwnerId"];
		self.userInfo = [[UserInfo alloc] initWithUserDTO:[dict objectForKey:@"userDTO"]];
	}
	
	return self;
}

@end
