//
//  ImageCacheManager.m
//  EveryLog
//
//  Created by Kamlesh on 27/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "ImageCacheManager.h"
#import "Global.h"
#import "Reachability.h"
#import <CommonCrypto/CommonHMAC.h>


@interface ImageCacheManager () {
	NSMutableDictionary *_imageCacheDict;
}
@property (nonatomic, strong) NSMutableDictionary *imageCacheDict;
@end

@implementation ImageCacheManager

@synthesize imageCacheDict = _imageCacheDict;

#pragma mark - Constant Tag

#define NOOFDAYS_IMAGECLEAR 7
#define IMAGE_DIR @"Images"
#define LIBRARY @"Library"
#define PRIVATE_DOCUMENT @"Private Documents"


static ImageCacheManager *imageCacheManager = nil;

+ (ImageCacheManager *) sharedImageCacheManager
{
	if (imageCacheManager == nil) {
		imageCacheManager = [[ImageCacheManager alloc]init];
	}
	return imageCacheManager;
}

-(void) clearImageStorage {
	NSString *filePath = [[NSHomeDirectory() stringByAppendingPathComponent:LIBRARY]
												stringByAppendingPathComponent:(PRIVATE_DOCUMENT)];
	
	filePath = [filePath stringByAppendingPathComponent:IMAGE_DIR];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	BOOL fDir = YES;
	if ([fileManager fileExistsAtPath:filePath isDirectory:&fDir]) {
		NSError *error = nil;
		
		//
#define IMAGE_CLEAR_DATE @"ImageClearDate"
		
		NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"MMMM dd, yyy"];
		
		NSString *lastVisitedDayStr = [userDefaults objectForKey:IMAGE_CLEAR_DATE];
		NSDate *lastVisitedDay = [dateFormatter dateFromString:lastVisitedDayStr];
		
		NSDate *currentDate = [NSDate date];
		NSString *currentDayStr = [dateFormatter stringFromDate:currentDate];
		NSDate *currentDay = [dateFormatter dateFromString:currentDayStr];
		
		NSInteger days = 0;
		
		if (lastVisitedDay != nil) {
			NSDateComponents *components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit
																																		 fromDate: lastVisitedDay toDate: currentDay options: 0];
			days = [components day];
		} else {
			days = -1;
		}
		
		[userDefaults setObject:currentDayStr forKey:IMAGE_CLEAR_DATE];
		[userDefaults synchronize];
		//
		
		if (days != 0 && days % NOOFDAYS_IMAGECLEAR == 0) {
			NSArray *imagesDataList = [fileManager contentsOfDirectoryAtPath:filePath error:&error];
			
			[imagesDataList enumerateObjectsUsingBlock:^(NSString* imageName, NSUInteger idx, BOOL *stop) {
				NSString *imagepath = [filePath stringByAppendingPathComponent:imageName];
				[fileManager removeItemAtPath:imagepath error:nil];
			}];
		}
	}
}

- (UIImage *) getImageForAPILink:(NSString *) imageLink_ withCallback:(ImageDownloadedCallback) imageDownloadedBlock {
	
	__block NSString *imageLink = imageLink_;
	
	NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
	
	if ([imageLink rangeOfString:@"resultImage"].location != NSNotFound) {
		imageLink = [NSString stringWithFormat:@"%@&resultImageFlag=1&nts=%@",imageLink,ts];
	} else {
		imageLink = [NSString stringWithFormat:@"%@&ts=%@",imageLink,ts];
	}
	
	if ([nonNilString(imageLink) isEqualToString:@""]) {
		return nil;
	}
	@synchronized (self) {
		__block UIImage* image = nil;
		
		NSString* urlKey = [self fileKeyForURL:imageLink_];
		
		image = [_imageCacheDict objectForKey:urlKey];
		
		if (image != nil) {
			imageDownloadedBlock(image, imageLink_);
			//return image;
		}
		else {
			if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
				//[GLOBAL displayNetworkAlertWithForce:NO];
				return [self getImageFromFileFor:imageLink];
			} else {
				/*
				 If image is present in file system, display image from file system,
				 And also make a network call to get updated image if present.
				 */
				image = [self getImageFromFileFor:imageLink];
				if (image) {
					if (_imageCacheDict == nil) {
						self.imageCacheDict = [NSMutableDictionary dictionary];
					}
					[_imageCacheDict setObject:image forKey:urlKey];
				}
				
				// Download data on block
				dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
				dispatch_async(queue, ^(void) {
					
//					NSString *ts = [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
//					imageLink = [NSString stringWithFormat:@"%@&ts=%@",imageLink,ts];
					
					NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageLink]];
					UIImage* downloadedImg = [[UIImage alloc] initWithData:imageData];
					
					dispatch_async(dispatch_get_main_queue(), ^{
						imageDownloadedBlock(downloadedImg, imageLink);
						if (downloadedImg) {
							if (_imageCacheDict == nil) {
								self.imageCacheDict = [NSMutableDictionary dictionary];
							}
							[_imageCacheDict setObject:downloadedImg forKey:urlKey];
							
							NSString *filePath = [self getFilePathForAPILink:imageLink];
							filePath = [filePath stringByAppendingPathExtension:@"jpg"];
							NSFileManager *fileManager= [NSFileManager defaultManager];
							[fileManager createFileAtPath:filePath contents:UIImageJPEGRepresentation(downloadedImg, 1)
																 attributes:nil];
						}
					});
					downloadedImg = nil;
				});
			}
		}
		
		return image;
	}
}

-(UIImage *) getImageFromFileFor:(NSString *) imageUrl {
	
	NSString* urlKey = [self fileKeyForURL:imageUrl];
	
	NSString *filePath = [self getFilePathForAPILink:imageUrl];
	filePath = [filePath stringByAppendingPathExtension:@"jpg"];
	NSData *imageData = nil;
	NSFileManager *fileManager= [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:filePath]) {
		imageData = [fileManager contentsAtPath:filePath];
		if (imageData != nil && [imageData length]){
			[_imageCacheDict setObject:imageData forKey:urlKey];
		}
	}
	return [UIImage imageWithData:imageData];
	
}

#pragma mark -
#pragma mark Construct Filepath

/*
 Create Path for URL, Each Response for API calls are stored as a File
 */

-(NSString *) getFilePathForAPILink:(NSString *) url {
	@synchronized (self) {
		NSLog(@"getFilePathFor : %@" , url);
		NSString *filePath = [[NSHomeDirectory() stringByAppendingPathComponent:LIBRARY]
													stringByAppendingPathComponent:(PRIVATE_DOCUMENT)];
		
		filePath = [filePath stringByAppendingPathComponent:IMAGE_DIR];
		
		NSFileManager *fileManager = [NSFileManager defaultManager];
		
		BOOL fDir = YES;
		if (![fileManager fileExistsAtPath:filePath isDirectory:&fDir]) {
			[fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:nil];
		}
		NSString* urlKey = [self fileKeyForURL:url];
		filePath = [filePath stringByAppendingPathComponent:urlKey];
		return filePath;
	}
}

/*
 Get Unique File name for URL
 */

- (NSString *) fileKeyForURL:(NSString *)urlString
{
	if ([nonNilString(urlString) isEqualToString:@""]) {
		return @"";
	}
	@synchronized (self) {
		NSLog(@"keyForURL : %@" , urlString);
		if ([[urlString substringFromIndex:[urlString length]-1] isEqualToString:@"/"]) {
			urlString = [urlString substringToIndex:[urlString length]-1];
		}
		const char *cStr = [urlString UTF8String];
		unsigned char result[16];
		CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
		return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
						result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
						result[8], result[9], result[10], result[11],result[12], result[13], result[14],
						result[15]];
	}
}


@end
