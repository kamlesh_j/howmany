//
//  APIManager.m
//  EveryLog
//
//  Created by Kamlesh on 25/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "APIManager.h"
#import "Global.h"
#import "Reachability.h"
#import "APIOperations.h"
#import "LogCommentsModel.h"

@interface APIManager () {
	NSMutableDictionary* _operations;
	NSOperationQueue *contentDownloadOperationQueue;
}

@property (nonatomic, strong) NSMutableDictionary* operations;

@end

@implementation APIManager

@synthesize operations = _operations;
/*
 Operation queue limit
 if panel API call then operation queue limit is 3
 else 1 (Location or weather API Call)
 */
#define OPERATION_QUEUE_LIMIT(isMax) isMax ? 3 : 1

#pragma mark -
#pragma mark Singleton Object Creation

static APIManager *downloadManager = nil;

+ (APIManager *) sharedAPIManager
{
	if (downloadManager == nil) {
		downloadManager = [[APIManager alloc]init];
	}
	return downloadManager;
}

- (BOOL) getMiniProfile {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	
	UserInfo *userInfo = [GLOBAL getStoredUserInfo];
//	NSString *userID = [userDefaults objectForKey:K_USERID];
//	NSString *authToken = [userDefaults objectForKey:K_AUTHTOKEN];
	if (userInfo != nil) {
		///services/user/miniprofile/528d1b89e4b05fa307ce69ba?ts=1391378746732
		NSString *urlString = [URL_SERVER stringByAppendingString:URL_MINIPROFILE];
		
		urlString = [urlString stringByAppendingPathComponent:userInfo.userID];
		
		NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
		[params setObject:[GLOBAL getTimeStamp] forKey:@"ts"];
		
		urlString = [GLOBAL setParameterFor:urlString andDict:params];
		
		
		NSURL *url = [NSURL URLWithString:urlString];
		
		NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																																cachePolicy:NSURLRequestReloadIgnoringCacheData
																														timeoutInterval:30.0];
		
		NSError *error = nil;
		
		[request setHTTPMethod:@"GET"];
		[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
		[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
		
		[request setHTTPShouldHandleCookies:YES];
		
		[request setValue:userInfo.authToken forHTTPHeaderField:@"auth-token"];
		
		
		//no cookies.
		[request setHTTPShouldHandleCookies:NO];
		
		NSURLResponse *response;
		NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
		
		NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
		
		NSLog(@"Login Request : %@", [request description]);
		NSLog(@"Login Response : %@",responseData);
		
		return NO;
		GLOBAL.userInfo = [[UserInfo alloc] initWithResponseData:urlData];
		
		if (GLOBAL.userInfo.isSuccess) {
			[[NSNotificationCenter defaultCenter] postNotificationName:N_DIDLOGIN object:nil];
			[GLOBAL storeUserInfo];
		}
		
		
		return YES;
	}
	return NO;
	
}

- (void) fbLogin:(NSString *) userId and:(NSString *) accessToken {
	
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_FB_LOGIN];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:userId forKey:@"userID"];
	[params setObject:accessToken forKey:@"fbAccessToken"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	
	[request setHTTPMethod:@"POST"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	[request setHTTPShouldHandleCookies:YES];
	
	
	NSURLResponse *response;
	NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
																															 options:NSJSONReadingMutableContainers
																																 error:&error];
	
	BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
	if (isSuccess) {
		GLOBAL.userInfo = [[UserInfo alloc] initWithResponseData:urlData];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:N_DIDLOGIN object:nil];
				
		[GLOBAL storeUserInfo];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:N_DIDLOGIN object:nil];
		
	} else {
		[[NSNotificationCenter defaultCenter] postNotificationName:N_LOGIN_FAILED object:nil];
	}
}
- (BOOL) isMemberLogin {
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
//	NSString *userID = [userDefaults objectForKey:K_USERID];
//	NSString *authToken = [userDefaults objectForKey:K_AUTHTOKEN];
//	NSString *userName = [userDefaults objectForKey:K_USERNAME];
//	
//	NSString *firstName = [userDefaults objectForKey:K_FIRSTNAME];
//	NSString *lastName = [userDefaults objectForKey:K_LASTNAME];
	
	UserInfo *unArchUserInfo = [GLOBAL getStoredUserInfo];
	
	if (unArchUserInfo != nil) {
		//logId=&userId=528d1b89e4b05fa307ce69ba&ts=1391378746571
		NSString *urlString = [URL_SERVER stringByAppendingString:URL_ISMEMBER];
		
		NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
		[params setObject:@"" forKey:@"logId"];
		[params setObject:unArchUserInfo.userID forKey:@"userId"];
		[params setObject:[GLOBAL getTimeStamp] forKey:@"ts"];
		
		urlString = [GLOBAL setParameterFor:urlString andDict:params];
		
		NSURL *url = [NSURL URLWithString:urlString];
		
		NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																																cachePolicy:NSURLRequestReloadIgnoringCacheData
																														timeoutInterval:30.0];
		
		NSError *error = nil;
		
		[request setHTTPMethod:@"GET"];
		[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
		[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
		
		[request setHTTPShouldHandleCookies:YES];
		
		[request setValue:unArchUserInfo.authToken forHTTPHeaderField:@"auth-token"];
		
		
		NSURLResponse *response;
		NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
		
		NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
		
		NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:urlData
																																 options:NSJSONReadingMutableContainers
																																	 error:&error];
		
		BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
		if (isSuccess) {
			[self getMiniProfile];
			GLOBAL.userInfo = unArchUserInfo;
		} else {
			
		}
		
		NSLog(@"Login Request : %@", [request description]);
		NSLog(@"Login Response : %@",responseData);
		
		
		return isSuccess;
	}
	return NO;
}


- (void) cropImage:(NSString *) imageLink {
}

-(void) createLogWith:(LogFeedModal*) logFeedModal {
	logFeedModal.ownerId = GLOBAL.userInfo.userID;
	logFeedModal.logTopicGoal = @"";
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:@"" forKey:@"id"];
	[params setObject:logFeedModal.logTopic forKey:@"logTopic"];
	[params setObject:logFeedModal.logIcon forKey:@"logIcon"];
	[params setObject:logFeedModal.logColor forKey:@"logColor"];
	[params setObject:logFeedModal.ownerId forKey:@"ownerId"];
	[params setObject:logFeedModal.type forKey:@"type"];
	[params setObject:logFeedModal.visibility forKey:@"visibility"];
	[params setObject:@"" forKey:@"endDate"];
	[params setObject:logFeedModal.logTopicGoal forKey:@"logTopicGoal"];
	[params setObject:logFeedModal.tags forKey:@"tags"];
	//	[params setObject:@"" forKey:@"subTopic1"];
	//	[params setObject:@"" forKey:@"subTopic1"];
	
	NSDictionary *resultDict = @{@"logUnit":logFeedModal.logUnit,@"logMeasurement":logFeedModal.logMeasurement};
	[params setObject:resultDict forKey:@"result"];
	
	NSDictionary *subTopic1 = @{@"topicName":@"",@"logUnit":@""};
	[params setObject:subTopic1 forKey:@"subTopic1"];
	
	NSDictionary *subTopic2 = @{@"topicName":@"",@"logUnit":@""};
	[params setObject:subTopic2 forKey:@"subTopic2"];
	
	
	__block NSString *urlString = [URL_SERVER stringByAppendingString:URL_CREATE_LOG];
	
	//		urlString = @"http://www.everylog.com/EveryLog-Core/services/log/addUpdate";
	
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	NSData *paraData = [NSJSONSerialization dataWithJSONObject:params
																										 options:NSJSONWritingPrettyPrinted
																											 error:&error];
	
	NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[paraData length]];
	
	[request setHTTPMethod:@"PUT"];
	[request addValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	//	Origin	file://
	[request addValue:@"file://" forHTTPHeaderField:@"Origin"];
	
	//X-Requested-With	XMLHttpRequest
	//[request addValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
	
	[request setValue:length forHTTPHeaderField:@"Content-Length"];
	[request setHTTPBody:paraData];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	//no cookies.
	[request setHTTPShouldHandleCookies:YES];
	//[request setValue:@"" forKey:<#(NSString *)#>]
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
			if (isSuccess) {
				//[self startContentForAPI:APITypeResults withSearchQuery:nil];
			} else {
			}
		} else {
			
		}
	}];
}

- (void) uploadIMG:(UIImage *) image {
	__block NSString *urlString = [URL_SERVER stringByAppendingString:URL_UPLOAD_IMG];
	//userId=528d1b89e4b05fa307ce69ba
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	
	NSURL *url = [NSURL URLWithString:urlString];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	
	/////////////
	// create request
	[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
	[request setHTTPShouldHandleCookies:NO];
	[request setTimeoutInterval:30];
	[request setHTTPMethod:@"POST"];
	
	// set Content-Type in HTTP header
	NSString *boundary = @"0xKhTmLbOuNdArY";
	NSString *contentType = [NSString stringWithFormat:@"multipart/json"];
	[request setValue:contentType forHTTPHeaderField: @"Content-Type"];
	
	// post body
	NSMutableData *body = [NSMutableData data];
	
	// add image data
	NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
	
	if (imageData) {
		[body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
		[body appendData:[[NSString stringWithFormat:@"Size: %lu bytes\r\n\r\n",(unsigned long)[imageData length]] dataUsingEncoding:NSUTF8StringEncoding]];
		
		[body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
		[body appendData:imageData];
		[body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
	}
	
	[body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
	
	// setting the body of the post to the reqeust
	[request setHTTPBody:body];
	
	//	[request setHTTPMethod:@"POST"];
	//	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	//	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	//no cookies.
	[request setHTTPShouldHandleCookies:NO];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
			if (isSuccess) {
				[self startContentForAPI:APITypeResults withSearchQuery:nil];
			} else {
			}
		} else {
			
		}
	}];
	
}

- (void) deleteLogEntry:(NSString *)  resultId {
	//resultId=53584cb0e4b021703e4e9224&userId=528d1b89e4b05fa307ce69ba&logId=518ed84ae4b0f38d6608e18e
	NSString *urlString = [URL_SERVER stringByAppendingString:URL_DELETE_LOG_ENTRY];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	[params setObject:resultId forKey:@"resultId"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	NSURL *url = [NSURL URLWithString:urlString];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	NSData *paraData = [NSJSONSerialization dataWithJSONObject:params
																										 options:NSJSONWritingPrettyPrinted
																											 error:&error];
	
	NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[paraData length]];
	
	[request setHTTPMethod:@"DELETE"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:length forHTTPHeaderField:@"Content-Length"];
	[request setHTTPBody:paraData];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	//no cookies.
	[request setHTTPShouldHandleCookies:NO];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
			if (isSuccess) {
				
			} else {
			}
			
			dispatch_async(dispatch_get_main_queue(), ^{
				[[NSNotificationCenter defaultCenter] postNotificationName:N_LOGRESULT_DELETED
																														object:@(isSuccess)];
			});
		} else {
			
		}
	}];
	
}



- (void) addLog:(NSString *) logResultValue andComment:(NSString *) comment withImage:(UIImage *) logImage withDate:(NSDate*) logDate {
	
	if (logImage != nil) {
		[self uploadIMG:logImage];
	}
	
	__block NSString *urlString = [URL_SERVER stringByAppendingString:URL_ADD_LOG];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"loggerId"];
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	[params setObject:logResultValue forKey:@"resultValue"];
	[params setObject:comment forKey:@"subTopic1Value"];
	[params setObject:@"" forKey:@"resultPicURL"];
	//
	if (logDate == nil) {
		[params setObject:[GLOBAL getTimeStamp] forKey:@"creationTime"];
	} else {
	}
	
	NSURL *url = [NSURL URLWithString:urlString];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	NSData *paraData = [NSJSONSerialization dataWithJSONObject:params
																										 options:NSJSONWritingPrettyPrinted
																											 error:&error];
	
	NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[paraData length]];
	
	[request setHTTPMethod:@"PUT"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:length forHTTPHeaderField:@"Content-Length"];
	[request setHTTPBody:paraData];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	//no cookies.
	[request setHTTPShouldHandleCookies:NO];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
			if (isSuccess) {
				[self startContentForAPI:APITypeResults withSearchQuery:nil];
			} else {
			}
		} else {
			
		}
	}];
}

- (void) addComment:(NSString *) comment {
	
	__block NSString *urlString = [URL_SERVER stringByAppendingString:URL_ADD_COMMENT];
	
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:GLOBAL.selectedLogModal.logID forKey:@"logId"];
	[params setObject:comment forKey:@"comment"];
	
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	NSData *paraData = [NSJSONSerialization dataWithJSONObject:params
																										 options:NSJSONWritingPrettyPrinted
																											 error:&error];
	
	NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[paraData length]];
	
	[request setHTTPMethod:@"PUT"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:length forHTTPHeaderField:@"Content-Length"];
	[request setHTTPBody:paraData];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	//no cookies.
	[request setHTTPShouldHandleCookies:NO];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
			if (isSuccess) {
				[self startContentForAPI:APITypeLogComments withSearchQuery:nil];
			} else {
				//_apiResponse(NO, apiType);
			}
		} else {
		}
	}];
}



- (void) doLogin:(NSString *) userName andPassword:(NSString*) password {
	
	__block NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOGIN];
	
	userName = @"kamleshkatios@gmail.com";
	password = @"pookam3100";
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:userName forKey:@"userName"];
	[params setObject:password forKey:@"password"];
	[params setObject:@"1" forKey:@"flag"];
	
	urlString = [GLOBAL setParameterFor:urlString andDict:params];
	
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	NSData *paraData = [NSJSONSerialization dataWithJSONObject:params
																										 options:NSJSONWritingPrettyPrinted
																											 error:&error];
	
	NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[paraData length]];
	
	[request setHTTPMethod:@"POST"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:length forHTTPHeaderField:@"Content-Length"];
	[request setHTTPBody:paraData];
	
	//no cookies.
	[request setHTTPShouldHandleCookies:NO];
	
	NSURLResponse *response;
	NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
	
	NSLog(@"Login Request : %@", [request description]);
	NSLog(@"Login Response : %@",responseData);
	
	GLOBAL.userInfo = [[UserInfo alloc] initWithResponseData:urlData];
	
	if (GLOBAL.userInfo.isSuccess) {
		[[NSNotificationCenter defaultCenter] postNotificationName:N_DIDLOGIN object:nil];		
		[GLOBAL storeUserInfo];		
	} else {
		//
		UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@"Oops!"
																											 message:@"Error occured. Please try again."
																											delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		[alerView show];
		alerView = nil;
	}
	//////////////////////////
	
}

- (void) logJoin:(NSString *) logId withCallbackBlock:(APIManagerResponse) apiManagerResponse {
	
	__block NSString *urlString = [URL_SERVER stringByAppendingString:URL_LOG_JOIN];
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:logId forKey:@"logId"];
	
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	NSData *paraData = [NSJSONSerialization dataWithJSONObject:params
																										 options:NSJSONWritingPrettyPrinted
																											 error:&error];
	
	NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[paraData length]];
	
	[request setHTTPMethod:@"PUT"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:length forHTTPHeaderField:@"Content-Length"];
	[request setHTTPBody:paraData];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	//no cookies.
	[request setHTTPShouldHandleCookies:NO];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
			dispatch_async(dispatch_get_main_queue(), ^{
				if (isSuccess) {
					apiManagerResponse(YES, nil);
				} else {
					NSString *errorString = responseDict[@"errorString"];
					apiManagerResponse(NO, errorString);
				}
			});
		} else {
			apiManagerResponse(NO, nil);
		}
	}];
}

- (void) addFavouriteLog:(NSString *) loggerId withCallbackBlock:(APIManagerResponse) apiManagerResponse {
	
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:loggerId forKey:@"logId"];
	[self addFavouriteWithParam:params andURLPath:URL_FAVOURITE_LOG withCallbackBlock:apiManagerResponse];
}

- (void) addFavouriteWithParam:(NSMutableDictionary *) params andURLPath:(NSString *) urlPath withCallbackBlock:(APIManagerResponse) apiManagerResponse {
	__block NSString *urlString = [URL_SERVER stringByAppendingString:urlPath];
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																															cachePolicy:NSURLRequestReloadIgnoringCacheData
																													timeoutInterval:30.0];
	
	NSError *error = nil;
	NSData *paraData = [NSJSONSerialization dataWithJSONObject:params
																										 options:NSJSONWritingPrettyPrinted
																											 error:&error];
	
	NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[paraData length]];
	
	[request setHTTPMethod:@"PUT"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
	[request setValue:length forHTTPHeaderField:@"Content-Length"];
	[request setHTTPBody:paraData];
	[request setValue: GLOBAL.userInfo.authToken forHTTPHeaderField:@"auth-token"];
	
	//no cookies.
	[request setHTTPShouldHandleCookies:NO];
	
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];
			dispatch_async(dispatch_get_main_queue(), ^{
				if (isSuccess) {
					apiManagerResponse(YES, nil);
				} else {
					NSString *errorString = responseDict[@"errorString"];
					apiManagerResponse(NO, errorString);
				}
			});
		} else {
			apiManagerResponse(NO, nil);
		}
	}];
	
}
- (void) addFavouriteUser:(NSString *) loggerId withCallbackBlock:(APIManagerResponse) apiManagerResponse {
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:GLOBAL.userInfo.userID forKey:@"userId"];
	[params setObject:loggerId forKey:@"loggerId"];
	[self addFavouriteWithParam:params andURLPath:URL_FAVOURITE_URER withCallbackBlock:apiManagerResponse];
}


#pragma mark -
#pragma mark -

- (void)startContentForAPI:(APIType) apiType withSearchQuery:(NSString *) query {
	//NSLog(@"startContentDownloadForURL...");
	if (![Reachability isNetworkReachable]) {
		return ;
	}
	if (apiType) {
		NSNumber *apiTypeObj = [NSNumber numberWithInt:apiType];
		
		__block APIOperations *contentDownloaderHelper = [_operations objectForKey:apiTypeObj];
		if (contentDownloaderHelper == nil)  {
			if (self.operations == nil) {
				self.operations = [NSMutableDictionary dictionary];
			}
			contentDownloaderHelper =  [[APIOperations alloc] initWithAPIResponse:^(BOOL isSuccess, APIType apiType) {
				[self removeOperation:apiType];
			}];
			contentDownloaderHelper.searchQuery = query;
			contentDownloaderHelper.apiType = apiType;
			if (![self.operations objectForKey:apiTypeObj]) {
				[self.operations setObject:contentDownloaderHelper forKey:apiTypeObj];
				[self addOperationInQueue:contentDownloaderHelper];
			}
			contentDownloaderHelper = nil;
		}
		else {
			if (contentDownloaderHelper.downloadFailed) {
				[self removeOperation:contentDownloaderHelper.apiType];
				return;
			}
		}
	}
	else {
		
	}
}

- (void) removeOperation:(APIType) apiType {
	NSNumber *apiTypeObj = [NSNumber numberWithInt:apiType];
	if ([_operations objectForKey:apiTypeObj]) {
		[_operations removeObjectForKey:apiTypeObj];
	}
}

-(void)downloadOperationQueue
{
	if(contentDownloadOperationQueue==nil) {
		contentDownloadOperationQueue = [[NSOperationQueue alloc] init];
		[contentDownloadOperationQueue setMaxConcurrentOperationCount:OPERATION_QUEUE_LIMIT(YES)];
		if (!self.operations)
			self.operations = [NSMutableDictionary dictionary];
	}
}

- (void)addOperationInQueue:(APIOperations *) apiOperation {
	//NSLog(@"addOperationInQueue...");
	[self downloadOperationQueue];
	
	for (APIOperations * operation in [contentDownloadOperationQueue operations]) {
		if (apiOperation.apiType == operation.apiType && ![operation isCancelled])
			return;
	}
	[contentDownloadOperationQueue setSuspended:NO];
	if (!apiOperation.isFinished) {
		if (![apiOperation isCancelled]){
			[apiOperation setQueuePriority:NSOperationQueuePriorityNormal];
			[contentDownloadOperationQueue addOperation:apiOperation];
		}
	}
	
}

#pragma mark -
#pragma mark Start Download Process


@end
