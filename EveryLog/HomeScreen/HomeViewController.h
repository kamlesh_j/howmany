//
//  HomeRootViewViewController.h
//  EveryLog
//
//  Created by Kamlesh on 13/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoggersModal.h"

@interface HomeViewController : UIViewController <UIGestureRecognizerDelegate>
@property (nonatomic) BOOL isLoggersLog;
@property (nonatomic, strong) LoggersModal *loggersModal;
@end
