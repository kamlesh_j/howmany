//
//  FindLogRootViewController.m
//  EveryLog
//
//  Created by Kamlesh on 13/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "FindLogRootViewController.h"
#import "Global.h"

@interface FindLogRootViewController () {
	UILabel *titleLbl;
}

@end

@implementation FindLogRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) setNavBarTitle:(NSString*) titleStr {
	[titleLbl setText:titleStr];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self.view setBackgroundColor:COLOR_BG];
	UIView* navBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, FRAMECONSTANTS.appWidth, FRAMECONSTANTS.navHeight)];
	[navBar setBackgroundColor:COLOR_NAV];
	[self.view addSubview:navBar];
	
	titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 20, FRAMECONSTANTS.appWidth - 2 * 50, FRAMECONSTANTS.navHeight - 20)];
	[titleLbl setTextAlignment:NSTextAlignmentCenter];
	[titleLbl setTextColor:[UIColor whiteColor]];
	[titleLbl setFont:[UIFont fontWithName:FONT_LIGHT size:35.0]];
	[navBar addSubview:titleLbl];
	
	//new_log.png
	//menu-icon.png
	UIImage *buttonImage = [UIImage imageNamed:@"backWhite"];
	UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
	[backBtn setFrame:CGRectMake(0, 20 + (50 - 44)/2, 44, 44)];
	[backBtn setImage:buttonImage forState:UIControlStateNormal];
	[navBar addSubview:backBtn];
	
	buttonImage = [UIImage imageNamed:@"closeWhite"];
	UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[closeBtn addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
	[closeBtn setFrame:CGRectMake(FRAMECONSTANTS.appWidth - 44 - 4, 20 + (50 - 44)/2, 44, 44)];
	[closeBtn setImage:buttonImage forState:UIControlStateNormal];
	[navBar addSubview:closeBtn];
	
	UIViewController *rootViewCnt = [[self.navigationController viewControllers] firstObject];
	
	if ([rootViewCnt isEqual:self]) {
		[backBtn setHidden:YES];
	}
	// Do any additional setup after loading the view.
}

-(void) backBtnAction {
	[self.navigationController popViewControllerAnimated:YES];
}
-(void) closeAction {
	[self dismissViewControllerAnimated:YES completion:^{
	}];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
