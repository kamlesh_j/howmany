//
//  SearchViewController.m
//  EveryLog
//
//  Created by Kamlesh on 13/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "SearchViewController.h"
#import "Global.h"
#import "LogsCell.h"

@interface SearchViewController () {
	IBOutlet UITableView *baseTableView;
	IBOutlet UISearchBar *searchBar;
}
@property (nonatomic, strong) NSArray *logsList;
@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) willShowKeyboard {
	CGRect tableFrame = [baseTableView frame];
	tableFrame.size.height -= FRAMECONSTANTS.keyboardHeight;
	[baseTableView setFrame:tableFrame];
}

-(void) willHideKeyboard {
	CGRect tableFrame = [baseTableView frame];
	tableFrame.size.height += FRAMECONSTANTS.keyboardHeight;
	[baseTableView setFrame:tableFrame];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
		[self setNavBarTitle:@"FIND BY SEARCHING"];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowKeyboard) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willHideKeyboard) name:UIKeyboardWillHideNotification object:nil];

	// Do any additional setup after loading the view.
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[searchBar resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_ {
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGFEED_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogFeed withSearchQuery:searchBar_.text];
		[searchBar resignFirstResponder];
	}
}

- (void) didDownloadData:(NSNotification *) notification {
	
	[GLOBAL removeActivity:self.view];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:N_LOGS_DOWNLOADED object:nil];
	
	self.logsList = (NSArray*) [notification object];
	[baseTableView reloadData];
}
#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView setBackgroundColor:[UIColor clearColor]];
	if (indexPath.row == 0) {
		return 40;
	}
	return 175;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_logsList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
		
	if (indexPath.row == 0) {
		NSString *cellIDdummy = @"CellIdentDummy";
		UITableViewCell *cellDummy = [tableView dequeueReusableCellWithIdentifier:cellIDdummy];
		if (cellDummy == nil) {
			cellDummy = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDdummy];
			[cellDummy.contentView setBackgroundColor:[UIColor clearColor]];
			[cellDummy setBackgroundColor:[UIColor clearColor]];
			cellDummy.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
		}
		return cellDummy;
	}
	else {
		static NSString *cellID = @"CellIdent";
		
		LogsCell *logsCell_ = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (logsCell_ == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"LogsCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			logsCell_ = [loadedViews lastObject];
			[logsCell_ setSelectionStyle:UITableViewCellSelectionStyleNone];
			[logsCell_.contentView setBackgroundColor:[UIColor clearColor]];
			[logsCell_ setBackgroundColor:[UIColor clearColor]];
		}
		
		[logsCell_ setLogsModal:[_logsList objectAtIndex:indexPath.row]];
		
		return logsCell_;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	LogFeedModal *logModal = [_logsList objectAtIndex:indexPath.row];
	GLOBAL.selectedLogModal = logModal;
	
	NSString * storyboardName = @"Events";
	NSString * viewControllerID = @"EventsTab";
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	UITabBarController * tabBarController = (UITabBarController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	[self presentViewController:tabBarController animated:YES completion:nil];
	[tabBarController setSelectedIndex:2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
