//
//  FindLogRootViewController.h
//  EveryLog
//
//  Created by Kamlesh on 13/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindLogRootViewController : UIViewController

- (void) setNavBarTitle:(NSString*) titleStr;

@end
