//
//  SearchViewController.h
//  EveryLog
//
//  Created by Kamlesh on 13/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindLogRootViewController.h"

@interface SearchViewController : FindLogRootViewController<UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate>

@end
