//
//  FindLogDetailViewController.m
//  EveryLog
//
//  Created by Kamlesh on 13/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "FindLogDetailViewController.h"
#import "LogsCell.h"
#import "Global.h"

@interface FindLogDetailViewController () {
	IBOutlet LogsCell* logsCell;
	IBOutlet UITableView *logsTable;
}
@property (nonatomic, strong) NSArray *logsList;
@end

@implementation FindLogDetailViewController

@synthesize categoryName = _categoryName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
		[self setNavBarTitle:_categoryName];
	
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGFEED_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogFeed withSearchQuery:_categoryName];
	}
	// Do any additional setup after loading the view.
}

- (void) didDownloadData:(NSNotification *) notification {
	
	[GLOBAL removeActivity:self.view];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:N_LOGS_DOWNLOADED object:nil];
	
	self.logsList = (NSArray*) [notification object];
	[logsTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView setBackgroundColor:[UIColor clearColor]];
	return 175;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_logsList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellID = @"CellIdent";
	
	LogsCell *logsCell_ = [tableView dequeueReusableCellWithIdentifier:cellID];
	if (logsCell_ == nil) {
		UINib* loadedNib = [UINib nibWithNibName:@"LogsCell" bundle:nil];
		NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
		logsCell_ = [loadedViews lastObject];
		[logsCell_ setSelectionStyle:UITableViewCellSelectionStyleNone];
		[logsCell_.contentView setBackgroundColor:[UIColor clearColor]];
		[logsCell_ setBackgroundColor:[UIColor clearColor]];
	}
	
	[logsCell_ setLogsModal:[_logsList objectAtIndex:indexPath.row]];
	
	return logsCell_;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	LogFeedModal *logModal = [_logsList objectAtIndex:indexPath.row];
	GLOBAL.selectedLogModal = logModal;
	
	NSString * storyboardName = @"Events";
	NSString * viewControllerID = @"EventsTab";
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	UITabBarController * tabBarController = (UITabBarController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	[self presentViewController:tabBarController animated:YES completion:nil];
	[tabBarController setSelectedIndex:2];
}

@end
