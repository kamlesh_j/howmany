//
//  CategoryViewController.m
//  EveryLog
//
//  Created by Kamlesh on 13/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "CategoryViewController.h"
#import "FindLogDetailViewController.h"

@interface CategoryViewController () {
	NSArray* _categoryList;
}

@property (nonatomic, strong) NSArray* categoryList;
@end

@implementation CategoryViewController

@synthesize categoryList = _categoryList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setNavBarTitle:@"FIND BY CATEGORY"];
	
	NSString *settingPath = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
	NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:settingPath];
	self.categoryList = [settings objectForKey:@"LogsCategory"];

	// Do any additional setup after loading the view.
}

#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView setBackgroundColor:[UIColor clearColor]];
	return 52.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_categoryList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellID = @"CellIdent";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		[cell setBackgroundColor:[UIColor clearColor]];
		[cell.contentView setBackgroundColor:[UIColor clearColor]];
		[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
	}
	[cell.textLabel setText:[_categoryList objectAtIndex:indexPath.row]];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString * storyboardName = @"FindLogs";
	NSString * viewControllerID = @"FindLogDetailViewController";
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	FindLogDetailViewController * controller = (FindLogDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	[controller setCategoryName:[_categoryList objectAtIndex:indexPath.row]];
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


@end
