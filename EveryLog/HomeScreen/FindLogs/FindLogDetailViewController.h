//
//  FindLogDetailViewController.h
//  EveryLog
//
//  Created by Kamlesh on 13/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindLogRootViewController.h"

@interface FindLogDetailViewController : FindLogRootViewController <UITableViewDelegate, UITableViewDataSource> {
	NSString* _categoryName;
}

@property (nonatomic, strong) NSString* categoryName;
@end
