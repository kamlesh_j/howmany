//
//  HeaderCellTableViewCell.h
//  Howmany
//
//  Created by Kamlesh on 19/04/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISegmentedControl *favouriteBtn;
@end
