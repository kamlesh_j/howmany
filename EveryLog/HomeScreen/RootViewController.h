//
//  RootViewController.h
//  EveryLog
//
//  Created by Kamlesh on 18/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController {
	NSString *tabName;
}
@property (nonatomic) BOOL isLoggersLog;
@property (nonatomic, strong) NSString *tabName;
@end
