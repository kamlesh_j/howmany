//
//  MenuViewController.m
//  EveryLog
//
//  Created by Kamlesh on 17/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "MenuViewController.h"
#import "Global.h"

@interface MenuViewController () {
	NSMutableArray *_optionsList;
	IBOutlet UITableView *menuTable;
}
@property (nonatomic, strong) NSMutableArray *optionsList;
@end

@implementation MenuViewController

@synthesize optionsList = _optionsList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:)
																							 name:N_DIDLOGIN object:nil];

	
	/*
	 Joe Zaczyk
	 Profile
	 Settings
	 Sign Out
	 */
	self.optionsList = [NSMutableArray arrayWithObjects:@"Joe Zaczyk",@"Find Logs to join",
											@"Settings",@"Sign Out",nil];
	// Do any additional setup after loading the view.
}

- (void) didLogin:(NSNotification*) notification{
	BOOL isSuccess = [[notification object] boolValue];
	[menuTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Tableview 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_optionsList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellID = @"CellIdent";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	}
	NSString *optionString = [_optionsList objectAtIndex:indexPath.row];
	if (indexPath.row == 0) {
		optionString = [NSString stringWithFormat:@"%@ %@",GLOBAL.userInfo.firstName, GLOBAL.userInfo.lastName];
	}
	[cell.textLabel setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
	[cell.textLabel setTextColor:[UIColor whiteColor]];
	[cell setBackgroundColor:[UIColor clearColor]];

	[cell.textLabel setText:[NSString stringWithFormat:@"%@",optionString]];
	[cell.imageView setImage:[UIImage imageNamed:@"settingsIcon.png"]];
		
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == 1) {
		NSString * storyboardName = @"FindLogs";
		NSString * viewControllerID = @"FindLogsTab";
		UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
		UIViewController * controller = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
		[self presentViewController:controller animated:YES completion:nil];
	} else if (indexPath.row == 3) {
		[GLOBAL clearLogin];
	}
}

@end
