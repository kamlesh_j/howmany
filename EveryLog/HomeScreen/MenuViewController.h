//
//  MenuViewController.h
//  EveryLog
//
//  Created by Kamlesh on 17/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
