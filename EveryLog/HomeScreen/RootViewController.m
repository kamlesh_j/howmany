//
//  RootViewController.m
//  EveryLog
//
//  Created by Kamlesh on 18/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "RootViewController.h"
#import "Global.h"
#import <FacebookSDK/FacebookSDK.h>

@interface RootViewController () {
	UIButton *closeMenuBtn;
}

@end

@implementation RootViewController

@synthesize tabName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[self.view setBackgroundColor:COLOR_BG];
	UIView* navBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, FRAMECONSTANTS.appWidth, FRAMECONSTANTS.navHeight)];
	[navBar setBackgroundColor:COLOR_NAV];
	[self.view addSubview:navBar];
	
	UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 20, FRAMECONSTANTS.appWidth - 2 * 50, FRAMECONSTANTS.navHeight - 20)];
	
	if (_isLoggersLog) {
		[titleLbl setFrame:CGRectMake(30, 20, FRAMECONSTANTS.appWidth - 2 * 30, FRAMECONSTANTS.navHeight - 20)];
	}

	[titleLbl setTextAlignment:NSTextAlignmentCenter];
	[titleLbl setTextColor:[UIColor whiteColor]];
	[titleLbl setFont:[UIFont fontWithName:FONT_LIGHT size:35.0]];
	[titleLbl setText:tabName];
	[navBar addSubview:titleLbl];
	
	//new_log.png
	//menu-icon.png
	UIImage *buttonImage = nil;
	if (_isLoggersLog == NO) {
		buttonImage = [UIImage imageNamed:@"menuBtn"];
		UIButton *settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		[settingBtn addTarget:self action:@selector(showMenuView) forControlEvents:UIControlEventTouchUpInside];
		[settingBtn setFrame:CGRectMake(10, 20 + (50 - 30)/2, 35, 30)];
		[settingBtn setImage:buttonImage forState:UIControlStateNormal];
		[navBar addSubview:settingBtn];
	}
	
	if (_isLoggersLog) {
		UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		[closeBtn addTarget:self action:@selector(closeLog) forControlEvents:UIControlEventTouchUpInside];
		[closeBtn setFrame:CGRectMake(FRAMECONSTANTS.appWidth - 44 - 4, 20 + (50 - 44)/2, 44, 44)];
		[navBar addSubview:closeBtn];
		
		UIImage *buttonImage = [UIImage imageNamed:@"closeWhite"];
		[closeBtn setImage:buttonImage forState:UIControlStateNormal];
	} else {
		
		buttonImage = [UIImage imageNamed:@"addBtn"];
		UIButton *addLogBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		
		[addLogBtn addTarget:self action:@selector(createLog) forControlEvents:UIControlEventTouchUpInside];
		[addLogBtn setFrame:CGRectMake(FRAMECONSTANTS.appWidth - 44 - 4, 20 + (50 - 44)/2, 44, 44)];
		[addLogBtn setImage:buttonImage forState:UIControlStateNormal];
		//		[addLogBtn setBackgroundImage:buttonImage forState:UIControlStateNormal];
		[navBar addSubview:addLogBtn];
	}
	
	/*
	 Adding a Transparent button on Main view when the menu tray is open, to close the 
	 Menu view
	 */
	closeMenuBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, FRAMECONSTANTS.openMenuWidth, FRAMECONSTANTS.appHeight)];
	[closeMenuBtn addTarget:self action:@selector(showMenuView) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:closeMenuBtn];
	[closeMenuBtn setHidden:YES];
	// Do any additional setup after loading the view.
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoginScreen)
																							 name:N_SHOW_LOGIN object:nil];
}
- (void) showMenuView {
	[closeMenuBtn setHidden:!closeMenuBtn.isHidden];
	GLOBAL.isMenuOpen = !closeMenuBtn.isHidden;
	[[NSNotificationCenter defaultCenter] postNotificationName:N_SHOW_MENU object:nil];
}


-(void) viewWillAppear:(BOOL)animated {
	if (GLOBAL.didLogin == NO) {
		if ([APIMANAGER isMemberLogin]) {
			GLOBAL.didLogin = YES;
		} else {
			if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
				
				// If there's one, just open the session silently, without showing the user the login UI
				[FBSession openActiveSessionWithReadPermissions:@[@"basic_info"]
																					 allowLoginUI:NO
																			completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
																				// Handler for session state changes
																				// This method will be called EACH time the session state changes,
																				// also for intermediate states and NOT just when the session open
																				if (error == nil) {
																					FBAccessTokenData *temp = session.accessTokenData;
																					NSLog(@"temp :%@", temp.dictionary);
																					if (state == FBSessionStateOpen) {
																						FBRequest *me = [[FBRequest alloc] initWithSession:session
																																										 graphPath:@"me"];
																						[me startWithCompletionHandler:^(FBRequestConnection *connection,
																																						 NSDictionary<FBGraphUser> *aUser,
																																						 NSError *error) {
																							
																							[APIMANAGER fbLogin:aUser.id and:session.accessTokenData.accessToken];
																						}];
																					}
																				} else {
																					NSLog(@"FB Access token : %@",session.accessTokenData.accessToken);
																				}
																			}];
			} else {
							[self showLoginScreen];
			}
		}
	} else {
		//	[self showLoginScreen];
	}
	
}

- (void) showLoginScreen {
	NSString * storyboardName = @"Login";
	NSString * viewControllerID = @"LoginNav";
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	UIViewController * controller = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	[self presentViewController:controller animated:YES completion:nil];
	
	GLOBAL.didLogin = YES;
}

-(void) viewWillDisappear:(BOOL)animated {
	if (closeMenuBtn.isHidden == NO) {
		[self showMenuView];
	}
}
- (void) closeLog {
	GLOBAL.loggersModal = nil;
	[self.navigationController dismissViewControllerAnimated:YES completion:^{
	}];
}
- (void) createLog {
	NSString * storyboardName = @"CreateLogStoryBoard";
	NSString * viewControllerID = @"CreateLogNav";
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	UIViewController * controller = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	[self presentViewController:controller animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
