//
//  HomeRootViewViewController.m
//  EveryLog
//
//  Created by Kamlesh on 13/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "HomeViewController.h"
#import "LogFeedsVC.h"
#import "LoggersVC.h"
#import "LogsVC.h"
#import "MenuViewController.h"
#import "Global.h"

@interface HomeViewController () {
//	IBOutlet LogFeedsVC *logFeedsVC;
//	IBOutlet LoggersVC *loggersVC;
//	IBOutlet LogsVC *logsVC;
	IBOutlet MenuViewController *menuViewController;
	UIViewController *mainViewController;
}

@property (nonatomic, strong) UIScreenEdgePanGestureRecognizer *swipeRight;
@property (nonatomic, strong) UIScreenEdgePanGestureRecognizer *swipeLeft;

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
			
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
	
	if (mainViewController == nil) {
		UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
		//
		UITabBarController *mainTabbar = [self.navigationController tabBarController];
		//[storyBoard instantiateViewControllerWithIdentifier:@"roottabbarc"];
		if (mainTabbar.selectedIndex == 0) {
			LogsVC* logsVC = [storyBoard instantiateViewControllerWithIdentifier:@"logsvc"];
			logsVC.isLoggersLog = _isLoggersLog;
			logsVC.loggersModal = _loggersModal;
			mainViewController = logsVC;
		} else if (mainTabbar.selectedIndex == 1) {
			LogFeedsVC* logsVC = [storyBoard instantiateViewControllerWithIdentifier:@"logfeed"];
			mainViewController = logsVC;
		} else {
			LoggersVC* logsVC = [storyBoard instantiateViewControllerWithIdentifier:@"loggers"];
			mainViewController = logsVC;
		}
				
		[self.navigationController.tabBarController.tabBar setTintColor:COLOR_APP_GREEN];

		[self addChildViewController:mainViewController];
		[mainViewController didMoveToParentViewController:self];
		[self.view addSubview:mainViewController.view];
	}
	[self addRighGesture];
	[self addLeftGesture];
}

- (void) addRighGesture {
	UITabBarController *mainTabbar = [self.navigationController tabBarController];
	[mainTabbar.view removeGestureRecognizer:_swipeRight];
	self.swipeRight = [[UIScreenEdgePanGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeRight:)];
	[_swipeRight setEdges:UIRectEdgeLeft];
	[mainTabbar.view addGestureRecognizer:_swipeRight];
}
- (void) addLeftGesture {
	UITabBarController *mainTabbar = [self.navigationController tabBarController];
	[mainTabbar.view removeGestureRecognizer:_swipeLeft];
	self.swipeLeft = [[UIScreenEdgePanGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeLeft:)];
	[_swipeLeft setEdges:UIRectEdgeRight];
	[mainTabbar.view addGestureRecognizer:_swipeLeft];
}
- (void) handleSwipeRight:(UIScreenEdgePanGestureRecognizer*) gesture{
	UITabBarController *mainTabbar = [self.navigationController tabBarController];
	if (mainTabbar.selectedIndex > 0) {
    [mainTabbar setSelectedIndex:mainTabbar.selectedIndex-1];
	}
	[self addRighGesture];
}
- (void) handleSwipeLeft:(UIScreenEdgePanGestureRecognizer*) gesture {
	UITabBarController *mainTabbar = [self.navigationController tabBarController];
	if (mainTabbar.selectedIndex < 2) {
    [mainTabbar setSelectedIndex:mainTabbar.selectedIndex+1];
	}
		[self addLeftGesture];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
		[self.view setBackgroundColor:COLOR_BG];
	
		UIStoryboard* storyBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
		menuViewController = [storyBoard instantiateViewControllerWithIdentifier:@"menuview"];

		[self addChildViewController:menuViewController];
		[menuViewController didMoveToParentViewController:self];
		[self.view addSubview:menuViewController.view];
	
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMenu)
																							 name:N_SHOW_MENU object:nil];
}


- (void) showMenu {
	
	CGRect mainFrame = mainViewController.view.frame;
		
	if (mainFrame.origin.x == 0) {
		mainFrame.origin.x = FRAMECONSTANTS.appWidth - FRAMECONSTANTS.openMenuWidth;
		[UIView animateWithDuration:0.5 animations:^{
			[mainViewController.view setFrame:mainFrame];
		} completion:^(BOOL finished) {
		}];
	} else {
		mainFrame.origin.x = 0;
		[UIView animateWithDuration:0.5 animations:^{
			[mainViewController.view setFrame:mainFrame];
		} completion:^(BOOL finished) {
		}];
	}
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
