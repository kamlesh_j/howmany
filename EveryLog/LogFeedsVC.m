//
//  LogFeedsVC.m
//  EveryLog
//
//  Created by Kamlesh on 05/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LogFeedsVC.h"
#import "LogfeedCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Global.h"
#import "LogFeedModal.h"
#import "HeaderCellTableViewCell.h"

@interface LogFeedsVC () {
	IBOutlet LogfeedCell *logfeedCell;
	IBOutlet UISearchBar *searchBar;
	UIColor *btnColor;
	NSArray *_logsList;
	NSArray *_favouriteLogs;
	IBOutlet UITableView *loggersTable;
	IBOutlet UIView *headerView;
}

@property (nonatomic, strong) NSArray *logsList;
@property (nonatomic, strong) NSArray *favouriteLogs;

- (IBAction)allFavoriteAction:(UISegmentedControl *) sender;

@end

@implementation LogFeedsVC

@synthesize logsList =_logsList;
@synthesize favouriteLogs = _favouriteLogs;

- (void)viewDidLoad
{
		[loggersTable setBackgroundColor:[UIColor clearColor]];

		self.tabName = @"LOG FEED";

    [super viewDidLoad];
		UIImage *selecetdImg = [UIImage imageNamed:@"logFeedTab.png"];
		[self.navigationController.tabBarItem setSelectedImage:selecetdImg];
		
	btnColor = [UIColor colorWithRed:(207.0/255.0) green:(42.0/255.0) blue:(39.0/255.0) alpha:1.0];
	
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(makeAPICall)
//																							 name:N_LOG_DELETE object:nil];
	// Do any additional setup after loading the view.
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
	NSLog(@"Scrollview offset: %f", scrollView.contentOffset.y);
	
	if (scrollView.contentOffset.y < -30) {
		if ([headerView isHidden]) {
			[loggersTable setTableHeaderView:headerView];
			[headerView setHidden:NO];
		}
	} else if (scrollView.contentOffset.y > 100) {
		[loggersTable setTableHeaderView:nil];
		[headerView setHidden:YES];
	}
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar_ {
	
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGFEED_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogFeed withSearchQuery:searchBar_.text];
	}
	
	[searchBar_ resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGFEED_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogFeed withSearchQuery:nil];
	}
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[loggersTable setTableHeaderView:nil];
	[headerView setHidden:YES];
	[loggersTable setContentOffset:CGPointMake(0, 80)];
	[self makeAPICall];
}

- (void) makeAPICall {
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGFEED_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeLogFeed withSearchQuery:nil];
	}
}

- (void) didDownloadData:(NSNotification *) notification {
	
	[GLOBAL removeActivity:self.view];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:N_LOGS_DOWNLOADED object:nil];
	
	self.logsList = (NSArray*) [notification object];
	[loggersTable reloadData];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[searchBar resignFirstResponder];
}
#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
    return 80;
	} else {
		return 250;
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 1;
	} else {
		if (self.favouriteLogs != nil) {
			return [_favouriteLogs count];
		} else {
			return [_logsList count];
		}
	}
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.section == 0) {
		static NSString *cellID = @"CellIdentHeader";
    HeaderCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (cell == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"HeaderCellTableViewCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			cell = [loadedViews lastObject];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[cell setBackgroundColor:[UIColor clearColor]];
			[cell.favouriteBtn addTarget:self action:@selector(allFavoriteAction:) forControlEvents:UIControlEventValueChanged];
			[cell.searchBar setDelegate:self];
		}
		return cell;
	} else {
		static NSString *cellID = @"CellIdent";
		
		LogfeedCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (cell == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"LogfeedCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			cell = [loadedViews lastObject];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[cell.contentView setBackgroundColor:[UIColor clearColor]];
			[cell setBackgroundColor:[UIColor clearColor]];
		}
		
		if (self.favouriteLogs != nil) {
			[cell setLogFeedModal:[_favouriteLogs objectAtIndex:indexPath.row]];
		} else {
			[cell setLogFeedModal:[_logsList objectAtIndex:indexPath.row]];
		}
		
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
    return;
	}
	LogFeedModal *logModal = [_logsList objectAtIndex:indexPath.row];
	GLOBAL.selectedLogModal = logModal;

	[searchBar resignFirstResponder];
	
	NSString * storyboardName = @"Events";
	NSString * viewControllerID = @"EventsTab";
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	UITabBarController * tabBarController = (UITabBarController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	[self presentViewController:tabBarController animated:YES completion:nil];
	[tabBarController setSelectedIndex:2];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)allFavoriteAction:(UISegmentedControl *) sender {

	if (sender.selectedSegmentIndex == 0) {
		self.favouriteLogs = nil;
		[loggersTable reloadData];
	} else {
		
		NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(LogFeedModal* logFeedModal, NSDictionary *bindings) {
			//NSLog(@"question.questionID : %d [] [quesID intValue]: %d",question.questionID,[quesID intValue]);
			if (logFeedModal.isfavorite) {
				return YES;
			} else {
				return NO;
			}
		}];
		
		self.favouriteLogs = [_logsList filteredArrayUsingPredicate:predicate];
		[loggersTable reloadData];
	}
}
@end
