//
//  Global.h
//  EveryLog
//
//  Created by Kamlesh on 18/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameConstants.h"
#import "APIManager.h"
#import "UserInfo.h"
#import "ImageCacheManager.h"
#import "LoggersModal.h"

@interface Global : NSObject {
	BOOL isMenuOpen;
	BOOL didLogin;
	UserInfo* userInfo;
}

@property (nonatomic, strong) UserInfo* userInfo;
@property (nonatomic) BOOL didLogin;
@property (nonatomic) BOOL isMenuOpen;
@property (nonatomic, strong) LoggersModal *loggersModal;
@property (nonatomic, strong) LogFeedModal *selectedLogModal;

extern Global *GLOBAL;
extern FrameConstants *FRAMECONSTANTS;
extern APIManager* APIMANAGER;
extern ImageCacheManager *IMAGEDOWNLOADER;

/**
 Singleton object declaration
 */
+ (Global *)sharedGlobal;
- (NSString * ) setParameterFor:(NSString *) urlString andDict:(NSDictionary*) params;

- (void) addActivityTo:(UIView *) view with:(UIActivityIndicatorViewStyle) activityIndicatorViewStyle;
- (void) removeActivity:(UIView *) view;
- (NSString *) getTimeStamp;
- (void) clearLogin;
- (UserInfo *) getStoredUserInfo;
- (void) storeUserInfo;
@end
