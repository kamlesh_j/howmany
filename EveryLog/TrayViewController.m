//
//  TrayViewController.m
//  EveryLog
//
//  Created by Kamlesh on 13/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "TrayViewController.h"

@interface TrayViewController ()

@end

@implementation TrayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
