//
//  LogChart.h
//  Howmany
//
//  Created by Kamlesh on 02/04/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECGraph.h"

typedef enum
{
	GraphFormatDaily = 1,
	GraphFormatWeek,
	GraphFormatMonth,
	GraphFormatYear
} GraphFormat;

typedef enum {
	GraphTypeBar = 0,
	GraphTypeLine,
	GraphTypeCicle
} GraphType;

@interface LogChart : UIView <ECGraphDelegate>
- (void) setGraphWith:(NSArray *) logResultList GraphType:(GraphType) graphType GraphFormat:(GraphFormat) graphFormat;
@end
