//
//  ConversationThreadCell.h
//  EveryLog
//
//  Created by Kamlesh on 09/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogCommentsModel.h"

@interface ConversationThreadCell : UITableViewCell {
	LogCommentsModel* _logCommentsModel;
}

@property (nonatomic, strong) LogCommentsModel* logCommentsModel;
@end
