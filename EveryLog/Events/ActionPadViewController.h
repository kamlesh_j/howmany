//
//  ActionPadViewController.h
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsRootViewController.h"

@interface ActionPadViewController : EventsRootViewController

@end
