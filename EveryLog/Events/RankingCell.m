//
//  RankingCell.m
//  EveryLog
//
//  Created by Kamlesh on 09/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "RankingCell.h"
#import "Global.h"

@interface RankingCell () {
	
	IBOutlet UIImageView *leaderIcon;
	IBOutlet UILabel *eventCountLbl;
	IBOutlet UILabel *userNameLbl;
	IBOutlet UIImageView *userThumbnail;
	IBOutlet UILabel *rankCount;
	IBOutlet UILabel *rankLbl;
}
@end

@implementation RankingCell

@synthesize logFeedModal = _logFeedModal;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		}
    return self;
}


- (void) setLogFeedModal:(LogFeedModal *)logFeedModal {
	_logFeedModal = logFeedModal;
	if (logFeedModal.rank == 1) {
		[leaderIcon setHidden:NO];
		[leaderIcon setImage:[UIImage imageNamed:@"LogLeader"]];
		[rankCount setHidden:YES];
		[rankLbl setText:@"Leader!"];
	} else {
		[leaderIcon setHidden:YES];
		[rankCount setHidden:NO];
	}
	
	/*
	 IBOutlet UIImageView *leaderIcon;
	 IBOutlet UILabel *eventCountLbl;
	 IBOutlet UILabel *userNameLbl;
	 IBOutlet UIImageView *userThumbnail;
	 IBOutlet UILabel *rankCount;
	 IBOutlet UILabel *rankLbl;
	 */
	
	eventCountLbl.text = _logFeedModal.resultValue;
	userNameLbl.text = [NSString stringWithFormat:@"%@\n%@",_logFeedModal.firstName,_logFeedModal.lastName];
	
	rankCount.text = [NSString stringWithFormat:@"%d",_logFeedModal.rank];

	__block NSString *userImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_USERIMG,_logFeedModal.profileImageURL];
	[IMAGEDOWNLOADER getImageForAPILink:userImgLink withCallback:^(UIImage *image, NSString *imgLink) {
		//if ([imgLink isEqualToString:userImgLink]) {
			[userThumbnail setImage:image];
		//}
	}];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
