//
//  LogChart.m
//  Howmany
//
//  Created by Kamlesh on 02/04/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LogChart.h"
#import "ECGraphItem.h"
#import "LogResultModal.h"
#import "UIColor+Support.h"

typedef enum {
	Jan = 1,
	Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,
	Dec
} Month;

@interface LogChart () {
	GraphType graphType;
	GraphFormat graphFormat;
	ECGraph *graph;
}
@property (nonatomic, strong) NSArray *logResultList;
@end

@implementation LogChart

- (NSDictionary *) monthString {
	return @{@(Jan) : @"Jan", @(Feb) : @"Feb",
					 @(Mar) : @"Mar",@(Apr) : @"Apr",
					 @(May) : @"May",@(Jun) : @"Jun",
					 @(Jul) : @"Jul",@(Aug) : @"Aug",
					 @(Sep) : @"Sep",@(Oct) : @"Oct",
					 @(Nov) : @"Nov",@(Dec) : @"Dec"
					 };
}
- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		[self setBackgroundColor:[UIColor clearColor]];
	}
	return self;
}

- (void) setGraphWith:(NSArray *) logResultList GraphType:(GraphType) graphType_ GraphFormat:(GraphFormat) graphFormat_ {
	self.logResultList = logResultList;
	graphFormat = graphFormat_;
	graphType = graphType_;
	[self setNeedsDisplay];
}

- (NSMutableArray *) getMonthsList:(NSMutableArray *) barItems {
	NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"month" ascending:YES];
	[barItems sortUsingDescriptors:@[sortDescriptor]];
	
	NSMutableArray *dummyItems = [NSMutableArray array];
	__block NSUInteger nextMonth = 0;
	[barItems enumerateObjectsUsingBlock:^(ECGraphItem* graphItem, NSUInteger idx, BOOL *stop) {
		if (idx == 0) {
			nextMonth = graphItem.month + 1;
		} else {
		jumpLabel:
			if (nextMonth == graphItem.month) {
				nextMonth = graphItem.month + 1;
				
				ECGraphItem* lastItem = barItems.lastObject;
				if (nextMonth > lastItem.month) {
					*stop = YES;
				}
			} else {
				ECGraphItem * item1 = [[ECGraphItem alloc] init];
				item1.isPercentage = YES;
				item1.month = nextMonth;
				item1.week = 0;
				item1.width = 1;
				item1.name = [self monthString][@(nextMonth)];
				item1.year = graphItem.year;
				[dummyItems addObject:item1];
				nextMonth ++;
				goto jumpLabel;
			}
		}
	}];
	
	[barItems addObjectsFromArray:dummyItems];
	sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"month" ascending:YES];
	[barItems sortUsingDescriptors:@[sortDescriptor]];
	
	return barItems;
}

- (NSInteger)weeksOfMonth:(int)month inYear:(int)year{
	NSString *dateString=[NSString stringWithFormat:@"%4d/%d/1",year,month];
	
	NSDateFormatter *dfMMddyyyy=[NSDateFormatter new];
	[dfMMddyyyy setDateFormat:@"yyyy/MM/dd"];
	NSDate *date=[dfMMddyyyy dateFromString:dateString];
	
	NSCalendar *calender = [NSCalendar currentCalendar];
	NSRange weekRange = [calender rangeOfUnit:NSWeekCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
	NSInteger weeksCount=weekRange.length;
	
	return weeksCount;
}

- (NSMutableArray *) getWeekObject:(NSArray *) barItems {
		
	NSSortDescriptor *sortDis = [NSSortDescriptor sortDescriptorWithKey:@"dateValue" ascending:YES];
	
	barItems = [barItems sortedArrayUsingDescriptors:@[sortDis]];
	
	NSCalendar *calendar = [NSCalendar currentCalendar];
	//declare your unitFlags
	
	int unitFlags = NSWeekOfYearCalendarUnit;
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[(ECGraphItem*)[barItems firstObject] dateValue]];
	[comp setDay:1];
	//NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
	
	NSDate *startDate = [gregorian dateFromComponents:comp];
	
	NSDateComponents* comps1 = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit
																				 fromDate:[(ECGraphItem*)[barItems lastObject] dateValue]]; // Get necessary date components
	
	// set last of month
	[comps1 setMonth:[comps1 month]+1];
	[comps1 setDay:0];
	NSDate *lastDate = [calendar dateFromComponents:comps1];

	
	NSDateComponents *dateComponents = [calendar components:unitFlags
																								 fromDate:startDate
																									 toDate:lastDate
																									options:0];
	
	NSInteger noOfWeeks = [dateComponents weekOfYear];
	

	
	NSMutableArray *missingDate = [NSMutableArray array];
	
	for (int i = 0; i <= noOfWeeks + 1; i++) {
		__block BOOL isAvailable = NO;
		[barItems enumerateObjectsUsingBlock:^(ECGraphItem* item, NSUInteger idx, BOOL *stop) {
			if ([self doesBelongToWeek:startDate referenceDate:item.dateValue]) {
				isAvailable = YES;
			}
		}];
		if (isAvailable == NO) {
			ECGraphItem * item1 = [[ECGraphItem alloc] init];
			item1.isPercentage = YES;
			item1.dateValue = startDate;
			item1.width = 1;
			item1.yValue = 2;
			
			[missingDate addObject:item1];
		}
		NSDateComponents *weekdayComponents = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:startDate];
		NSInteger currentWeekday = [weekdayComponents weekday]; //[1;7] ... 1 is sunday, 7 is saturday in gregorian calendar
		
		NSDateComponents *comp = [[NSDateComponents alloc] init];
		[comp setDay:8 - currentWeekday];
		
		startDate = [[NSCalendar currentCalendar] dateByAddingComponents:comp toDate:startDate options:0];
	}
	
	[missingDate addObjectsFromArray:barItems];
	
	sortDis = [NSSortDescriptor sortDescriptorWithKey:@"dateValue" ascending:YES];
	missingDate = [[missingDate sortedArrayUsingDescriptors:@[sortDis]] mutableCopy];
	
	return missingDate;
}
-(BOOL) doesBelongToWeek:(NSDate *)date referenceDate:(NSDate*) comparingDate{
	NSDate *start;
	NSTimeInterval extends;
	NSCalendar *cal=[NSCalendar autoupdatingCurrentCalendar];
	// NSDate *today = [NSDate date];
	BOOL success= [cal rangeOfUnit:NSWeekCalendarUnit startDate:&start
												interval: &extends forDate:date];
	if(!success)return NO;
	
	NSTimeInterval dateInSecs = [comparingDate timeIntervalSinceReferenceDate];
	NSTimeInterval dayStartInSecs= [start timeIntervalSinceReferenceDate];
	if(dateInSecs > dayStartInSecs && dateInSecs < (dayStartInSecs+extends)){
		return YES;
	}
	else {
		return NO;
	}
}
- (NSMutableArray *) getGraphObject {
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	NSMutableArray *barItems = [NSMutableArray array];
	
	__block NSString *previousDate = nil;
	__block ECGraphItem *item1 = nil;
	[self.logResultList enumerateObjectsUsingBlock:^(LogResultModal *logResult, NSUInteger idx, BOOL *stop) {
		[formatter setDateFormat:@"MM/dd/yy hh:mm a"];
		
		if (idx == 0) {
			logResult.creationTime = @"04/03/14 11:12 PM";
		} else if (idx == 2) {
			logResult.creationTime = @"06/03/14 11:12 PM";
		}
		NSDate * date = [formatter dateFromString:logResult.creationTime];
		NSString *formatForName = nil;
		if (graphFormat == GraphFormatYear) {
			[formatter setDateFormat:@"yyyy"];
			formatForName = @"yyyy";
		} else if (graphFormat == GraphFormatMonth) {
			[formatter setDateFormat:@"yyyy-MM"];
			formatForName = @"MMM";
		} else  if (graphFormat == GraphFormatWeek) {
			[formatter setDateFormat:@"yyyy-MM-EEEE"];
			formatForName = @"MMM dd";
		} else  if (graphFormat == GraphFormatDaily) {
			formatForName = @"MMM dd";
			[formatter setDateFormat:@"yyyy-MM-dd"];
		}
		
		// Get Date year
		NSDate *currentDate = [NSDate date];
		NSCalendar *calendar   = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
		NSDateComponents *components   = [calendar components:(NSYearCalendarUnit
																													 | NSMonthCalendarUnit
																													 | NSCalendarUnitWeekOfMonth
																													 | NSDayCalendarUnit
																													 | NSCalendarUnitWeekday) fromDate:currentDate];
		NSDateComponents *componentsLog   = [calendar components:(NSYearCalendarUnit
																															| NSMonthCalendarUnit
																															| NSCalendarUnitWeekOfMonth
																															| NSDayCalendarUnit
																															| NSCalendarUnitWeekday) fromDate:date];
		
		if (components.year == componentsLog.year || graphFormat == GraphFormatYear) {
			NSString *dateString = [formatter stringFromDate:date];
			if (![dateString isEqualToString:previousDate]) {
				item1 = [[ECGraphItem alloc] init];
				item1.color = [UIColor colorWithHexString:logResult.logDTO.logColor];
				item1.isPercentage = YES;
				item1.yValue = [logResult.resultValue intValue];
				item1.width = 1;
				item1.dateString = dateString;
				item1.year = componentsLog.year;
				item1.month = componentsLog.month;
				item1.week = componentsLog.weekOfMonth;
				item1.day = componentsLog.day;
				item1.weekDay = componentsLog.weekday;
				item1.dateValue = date;
				[formatter setDateFormat:formatForName];
				item1.name = [formatter stringFromDate:date];
				[barItems addObject:item1];
			} else {
				item1.yValue += [logResult.resultValue intValue];
			}
			previousDate = dateString;
			NSLog(@"Month String %@ \n\n", dateString);
		}
		//
		
		NSLog(@"Log Value : %d", [logResult.resultValue intValue]);
		NSLog(@"CreationTime : %@",logResult.creationTime);
		NSLog(@"Date object : %@", date);
		NSLog(@"");
	}];
	
	if (graphFormat == GraphFormatMonth) {
		[self getMonthsList:barItems];
	} else if (graphFormat == GraphFormatWeek) {
		barItems = [self getWeekObject:barItems];
	}
	return barItems;
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
	// Drawing code
	CGContextRef _context = UIGraphicsGetCurrentContext();
	if (graph != nil) {
		graph = nil;
	}
	graph = [[ECGraph alloc] initWithFrame:self.bounds
														 withContext:_context isPortrait:YES];
	NSMutableArray *barItems = [self getGraphObject];
	
	if (graphFormat == GraphFormatMonth) {
	} else if (graphFormat == GraphFormatWeek) {
		[barItems enumerateObjectsUsingBlock:^(ECGraphItem *item, NSUInteger idx, BOOL *stop) {
			NSLog(@"Item Date  : %@",item.dateValue);
			NSLog(@"Item Value : %f",item.yValue);
			
			NSCalendar* calendar = [NSCalendar currentCalendar];
			NSDateComponents* comps = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:item.dateValue]; // Get necessary date components
			
			// set last of month
			[comps setMonth:[comps month]+1];
			[comps setDay:0];
			NSDate *tDateMonth = [calendar dateFromComponents:comps];
			NSLog(@"%@", tDateMonth);
			if ([self doesBelongToWeek:item.dateValue referenceDate:tDateMonth]) {
				NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
				[formatter setDateFormat:@"MMM"];
				item.name = [formatter stringFromDate:item.dateValue];
				NSLog(@"Set Name Item Date  : %@",item.dateValue);
			} else {
				item.name = @"";
			}
		}];
		graph.graphFormatLine = GraphFormatLineWeek;
	}

	[graph setDelegate:self];
	[graph setBackgroundColor:[UIColor clearColor]];
	[graph drawHistogramWithItems:barItems lineWidth:1.0 color:[UIColor blackColor]];
}


@end
