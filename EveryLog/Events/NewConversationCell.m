//
//  NewConversation.m
//  EveryLog
//
//  Created by Kamlesh on 09/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "NewConversationCell.h"
#import "Global.h"

@interface NewConversationCell () {

	IBOutlet UIView *baseView;
	IBOutlet UIButton *addCommentBtn;
	IBOutlet UITextField *textField;
	IBOutlet UITextView *commentView;
}
- (IBAction)addCommentAction:(id)sender;
@end

@implementation NewConversationCell

@synthesize tableView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
			[self.contentView.layer setCornerRadius:10.0];

    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
	[baseView.layer setCornerRadius:5.0f];
	[baseView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[baseView.layer setBorderWidth:0.5];
	
	[addCommentBtn.layer setCornerRadius:5.0f];
}

- (IBAction)addCommentAction:(id)sender {
	[commentView resignFirstResponder];
	if (![commentView.text isEqualToString:@""]) {
		[GLOBAL addActivityTo:[(UIViewController *)tableView.delegate view] with:UIActivityIndicatorViewStyleGray];
		[APIMANAGER addComment:commentView.text];
	}
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
	textField.text = @" ";
}
- (void)textViewDidEndEditing:(UITextView *)textView {
	
}

@end
