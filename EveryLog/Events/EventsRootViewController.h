//
//  EventsRootViewController.h
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsRootViewController : UIViewController

- (void) setViewTitle:(NSString *) titleString;

@end
