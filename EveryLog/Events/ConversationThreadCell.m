//
//  ConversationThreadCell.m
//  EveryLog
//
//  Created by Kamlesh on 09/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "ConversationThreadCell.h"
#import "Global.h"

@interface ConversationThreadCell () {
	
	IBOutlet UIView *baseView;
	IBOutlet UILabel *userNameLbl;
	IBOutlet UILabel *commentLbl;
	IBOutlet UILabel *dateLbl;
	IBOutlet UILabel *timeLbl;
	IBOutlet UIImageView *thumbnailImg;
}
@end

@implementation ConversationThreadCell

@synthesize logCommentsModel = _logCommentsModel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setLogCommentsModel:(LogCommentsModel *)logCommentsModel {
	_logCommentsModel = logCommentsModel;
	
	userNameLbl.text = [NSString stringWithFormat:@"%@\n%@", _logCommentsModel.userInfo.firstName, _logCommentsModel.userInfo.lastName];
	commentLbl.text = _logCommentsModel.comment;
	dateLbl.text = _logCommentsModel.creationTime;
	__block NSString *userImgLink = [URL_SERVER stringByAppendingFormat:@"%@%@",URL_IMG_USERIMG,_logCommentsModel.userInfo.profileImageUrl];
	[IMAGEDOWNLOADER getImageForAPILink:userImgLink withCallback:^(UIImage *image, NSString *imgLink) {
		//if ([imgLink isEqualToString:userImgLink]) {
			[thumbnailImg setImage:image];
		//}
	}];

}

-(void) drawRect:(CGRect)rect {
	[baseView.layer setCornerRadius:5.0f];
	[baseView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[baseView.layer setBorderWidth:0.5];
}
@end
