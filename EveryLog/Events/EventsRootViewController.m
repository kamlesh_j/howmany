//
//  EventsRootViewController.m
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "EventsRootViewController.h"
#import "Global.h"

@interface EventsRootViewController () {
	UILabel *titleLbl;
}

@end

@implementation EventsRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) setViewTitle:(NSString *) titleString {
	[titleLbl setText:titleString];
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self.navigationController.tabBarController.tabBar setTintColor:COLOR_APP_GREEN];
	
	[self.view setBackgroundColor:COLOR_BG];
	UIView* navBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, FRAMECONSTANTS.appWidth, FRAMECONSTANTS.navHeight)];
	[navBar setBackgroundColor:COLOR_NAV];
	[self.view addSubview:navBar];
	
	titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(80, 20, FRAMECONSTANTS.appWidth - 2 * 80, FRAMECONSTANTS.navHeight - 20)];
	[titleLbl setTextAlignment:NSTextAlignmentCenter];
	[titleLbl setTextColor:[UIColor whiteColor]];
	[titleLbl setFont:[UIFont fontWithName:FONT_LIGHT size:35.0]];
	//	[titleLbl setText:@"CREATE A LOG"];
	[navBar addSubview:titleLbl];
	
	//new_log.png
	//menu-icon.png
	UIImage *buttonImage = [UIImage imageNamed:@"closeWhite"];
	UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[closeBtn addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
	[closeBtn setFrame:CGRectMake(FRAMECONSTANTS.appWidth - 44 - 4, 20 + (50 - 44)/2, 44, 44)];
	[closeBtn setImage:buttonImage forState:UIControlStateNormal];
	[navBar addSubview:closeBtn];
	
	// Do any additional setup after loading the view.
}

-(void) closeAction {
	[self.tabBarController dismissViewControllerAnimated:YES completion:^{
	}];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
