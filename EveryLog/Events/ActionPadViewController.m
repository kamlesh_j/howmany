//
//  ActionPadViewController.m
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "ActionPadViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ActionpadButton.h"
#import "Global.h"
#import <Social/SLComposeViewController.h>
#import <Social/SLServiceTypes.h>
#import <FacebookSDK/FacebookSDK.h>
#import "UIColor+Support.h"

@interface ActionPadViewController () {
	IBOutlet UILabel *eventCountLbl;
	IBOutlet UILabel *eventTypeTxt;
	IBOutlet UIImageView *eventLogo;
	IBOutlet UIView *headerView;
	IBOutlet UIScrollView *baseScrollView;
	
	IBOutlet ActionpadButton *logSettingBtn;
	IBOutlet ActionpadButton *deleteLogBtn;
	IBOutlet ActionpadButton *postOnTwitterBtn;
	IBOutlet ActionpadButton *postOnFBBtn;
	IBOutlet ActionpadButton *joinBtn;
	IBOutlet ActionpadButton *inviteFriendsBtn;
	BOOL m_postingInProgress;
}
- (IBAction)deleteLog:(id)sender;
- (IBAction)editLog:(id)sender;
- (IBAction)facebookPost:(id)sender;

@end

@implementation ActionPadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
	[self.tabBarController.tabBar setTintColor:COLOR_APP_GREEN];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
		[self setViewTitle:@"ACTION PAD"];
	
	eventTypeTxt.text = GLOBAL.selectedLogModal.logTopic;
	eventCountLbl.text = GLOBAL.selectedLogModal.resultValue;
	eventLogo.backgroundColor = [UIColor colorWithHexString:GLOBAL.selectedLogModal.logColor];

	[headerView.layer setCornerRadius:5.0];
	headerView.layer.borderWidth = 0.3;
	[headerView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[eventLogo.layer setCornerRadius:eventLogo.frame.size.width/2.0];
	
		
//	[logSettingBtn setIcons:@"logSettings.png" andTitle:@"Edit\nLog settings"];
//	[deleteLogBtn setIcons:@"trashIcon.png" andTitle:@"Delete this\nLog"];
//	[postOnTwitterBtn setIcons:nil andTitle:@"Tweet the status of this Log"];
//	[postOnFBBtn setIcons:nil andTitle:@"Post this Log to your timeline"];
//	[joinBtn setIcons:nil andTitle:@"Join this\nLog"];
//	[inviteFriendsBtn setIcons:nil andTitle:@"Invite others to join Log"];
	
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)deleteLog:(id)sender {
	[APIMANAGER startContentForAPI:APITypeDeleteLog withSearchQuery:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lodDeleted)
																							 name:N_LOG_DELETE object:nil];
}

- (IBAction)editLog:(id)sender {
	NSString * storyboardName = @"CreateLogStoryBoard";
	NSString * viewControllerID = @"LogBadgeVC";
	///CreateLogNav
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
	UIViewController * controller = (UIViewController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
	
	UINavigationController *navController =  [[UINavigationController alloc] initWithRootViewController:controller];
	navController.navigationBarHidden = YES;
	[self presentViewController:navController animated:YES completion:nil];
	navController = nil;
}

#pragma mark -
#pragma mark Btn actions
- (IBAction)facebookPost:(id)sender {
	//[self postWithText];
	
	NSString *description = [NSString stringWithFormat:@"Rank: %d",GLOBAL.selectedLogModal.rank];
	NSString *name = @"Everylog";
	NSString *caption = GLOBAL.selectedLogModal.logTopic;
	NSString *link = @"http://www.everylog.com";
	
	// Check if the Facebook app is installed and we can present the share dialog
	FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
	params.link = [NSURL URLWithString:link];
	params.name = name;
	params.caption = caption;
	params.description = description;
	
	// If the Facebook app is installed and we can present the share dialog
	if ([FBDialogs canPresentShareDialogWithParams:params]) {
		// Present the share dialog
		// Present share dialog
		[FBDialogs presentShareDialogWithLink:params.link
																		 name:params.name
																	caption:params.caption
															description:params.description
																	picture:params.picture
															clientState:nil
																	handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
																		if(error) {
																			// An error occurred, we need to handle the error
																			// See: https://developers.facebook.com/docs/ios/errors
																			//NSLog([NSString stringWithFormat:@"Error publishing story: %@", [error description]]);
																			NSLog(@"Error occurred");
																		} else {
																			// Success
																			NSLog(@"result %@", results);
																		}
																	}];
	} else {
		// Present the feed dialog
		// Put together the dialog parameters
		NSDictionary *paramsDict = @{@"name":name,
																 @"caption":caption,
																 @"description":description,
																 @"link":link
																 };
		
		// Show the feed dialog
		[FBWebDialogs presentFeedDialogModallyWithSession:nil
																					 parameters:paramsDict
																							handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
																								if (error) {
																									// An error occurred, we need to handle the error
																									// See: https://developers.facebook.com/docs/ios/errors
																									NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
																								} else {
																									if (result == FBWebDialogResultDialogNotCompleted) {
																										// User cancelled.
																										NSLog(@"User cancelled.");
																									} else {
																										// Handle the publish feed callback
																										NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
																										if (![urlParams valueForKey:@"post_id"]) {
																											// User cancelled.
																											NSLog(@"User cancelled.");
																											
																										} else {
																											// User clicked the Share button
																											NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
																											NSLog(@"result %@", result);
																										}
																									}
																								}
																							}];
	}
	
	

}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
  NSArray *pairs = [query componentsSeparatedByString:@"&"];
  NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
  for (NSString *pair in pairs) {
    NSArray *kv = [pair componentsSeparatedByString:@"="];
    NSString *val =
    [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    params[kv[0]] = val;
  }
  return params;
}

-(void) lodDeleted {
	[self dismissViewControllerAnimated:YES completion:NO];
}

- (void) viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter  defaultCenter] removeObserver:self];
}

#pragma mark -

-(void) postWithText {
	NSString *description = [NSString stringWithFormat:@"Rank: %d",GLOBAL.selectedLogModal.rank];
	NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
																 @"www.everylog.com", @"link",
																 @"EveryLog", @"name",
																 GLOBAL.selectedLogModal.logTopic, @"caption",
																 description, @"description",
																 nil];
}



@end
