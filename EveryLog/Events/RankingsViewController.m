//
//  RankingsViewController.m
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "RankingsViewController.h"
#import "RankingCell.h"
#import "Global.h"
#import "LogFeedModal.h"
#import "UIColor+Support.h"

@interface RankingsViewController () {
	IBOutlet UILabel *eventCount;
	IBOutlet UILabel *eventName;
	IBOutlet UIImageView *eventLogo;
	IBOutlet UIView *headerView;
	IBOutlet UITableView *baseTable;
	NSArray * _logResultList;
}

@property (nonatomic, strong) NSArray * logResultList;
@end

@implementation RankingsViewController

@synthesize logResultList = _logResultList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self setViewTitle:@"RANKINGS"];
	
	[headerView.layer setCornerRadius:5.0];
	headerView.layer.borderWidth = 0.3;
	[headerView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	
	[eventLogo.layer setCornerRadius:eventLogo.frame.size.width/2.0];
	
	eventName.text = GLOBAL.selectedLogModal.logTopic;
	eventCount.text = GLOBAL.selectedLogModal.resultValue;
	eventLogo.backgroundColor = [UIColor colorWithHexString:GLOBAL.selectedLogModal.logColor];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
	[self.tabBarController.tabBar setTintColor:COLOR_APP_GREEN];

//	UIImage *selecetdImg = [UIImage imageNamed:@"Rankings"];
//	[self.tabBarItem setSelectedImage:selecetdImg];

	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOGGER_FOR_LOG object:nil];
		[APIMANAGER startContentForAPI:APITypeLoggersForLog withSearchQuery:nil];
	}
}
- (void) didDownloadData:(NSNotification*) notification {
		[GLOBAL removeActivity:self.view];
	self.logResultList = (NSArray *) [notification object];
	
	if ([_logResultList count]) {
		LogFeedModal * logFeedModal = [_logResultList objectAtIndex:0];
		eventCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_logResultList count]];
		[baseTable reloadData];
	}
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
}

#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView setBackgroundColor:[UIColor clearColor]];
	if (indexPath.row == 0) {
		return 80.0f;
	}
	return 60.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (_logResultList == nil) {
		return 0;
	}
	return [_logResultList count] + 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellID = @"CellIdent";
	
	if (indexPath.row == 0) {
		NSString *cellIDdummy = @"CellIdentDummy";
		UITableViewCell *cellDummy = [tableView dequeueReusableCellWithIdentifier:cellIDdummy];
		if (cellDummy == nil) {
			cellDummy = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDdummy];
			[cellDummy.contentView setBackgroundColor:[UIColor clearColor]];
			[cellDummy setBackgroundColor:[UIColor clearColor]];
			cellDummy.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
		}
		return cellDummy;
	} else {		
		RankingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (cell == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"RankingCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			cell = [loadedViews lastObject];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[cell.contentView setBackgroundColor:[UIColor clearColor]];
			[cell setBackgroundColor:[UIColor clearColor]];
		}
		[cell setLogFeedModal:[_logResultList objectAtIndex:indexPath.row - 1]];
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
