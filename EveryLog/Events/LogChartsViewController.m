//
//  LogChartsViewController.m
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LogChartsViewController.h"
#import "LogChart.h"
#import "Global.h"
#import "UIColor+Support.h"

@interface LogChartsViewController () {
	IBOutlet UILabel *eventCount;
	IBOutlet UILabel *eventName;
	IBOutlet UIImageView *eventLogo;
	IBOutlet UIView *headerView;
}
@property (nonatomic, strong) LogChart *logChart;
@property (nonatomic, strong) NSArray *logResultList;
@end

@implementation LogChartsViewController

CGFloat const CPDBarWidth = 0.25f;
CGFloat const CPDBarInitialX = 0.25f;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) viewWillAppear:(BOOL)animated {
	[self.tabBarController.tabBar setTintColor:COLOR_APP_GREEN];
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] removeObserver:self
																										name:N_LOG_CHART object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOG_CHART object:nil];
		[APIMANAGER startContentForAPI:APITypeLogChart withSearchQuery:nil];
	}
}
- (void) didDownloadData:(NSNotification*) notification {
	[GLOBAL removeActivity:self.view];
	self.logResultList = (NSArray *) [notification object];
	if ([_logResultList count]) {
		//[baseTable reloadData];
		
		if (_logChart != nil) {
			[_logChart removeFromSuperview];
			self.logChart = nil;
		}
		self.logChart = [[LogChart alloc] initWithFrame:CGRectMake(0, 120, 320, 400)];
		[_logChart setGraphWith:_logResultList GraphType:GraphTypeBar GraphFormat:GraphFormatMonth];		
		[self.view addSubview:_logChart];
		
	}
}


- (void)viewDidLoad
{
    [super viewDidLoad];
		[self setViewTitle:@"LOG CHARTS"];

	eventName.text = GLOBAL.selectedLogModal.logTopic;
	eventCount.text = GLOBAL.selectedLogModal.resultValue;
	eventLogo.backgroundColor = [UIColor colorWithHexString:GLOBAL.selectedLogModal.logColor];
	
	[headerView.layer setCornerRadius:5.0];
	headerView.layer.borderWidth = 0.3;
	[headerView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
	[eventLogo.layer setCornerRadius:eventLogo.frame.size.width/2.0];

	
//	UIImage *selecetdImg = [UIImage imageNamed:@"tab_logs.png"];
//	[self.tabBarController.tabBarItem setSelectedImage:selecetdImg];
	
//	id temp = self.tabBarController.tabBarItem;
	
//	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Oops!!"
//																											message:@"Work in progress.."
//																										 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//	[alertView show];
//	alertView = nil;
	// Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
