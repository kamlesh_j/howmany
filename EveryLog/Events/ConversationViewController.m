//
//  ConversationViewController.m
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "ConversationViewController.h"
#import "NewConversationCell.h"
#import "ConversationThreadCell.h"
#import "Global.h"
#import "UIColor+Support.h"

@interface ConversationViewController () {
	IBOutlet UILabel *eventCount;
	IBOutlet UILabel *eventName;
	IBOutlet UIImageView *eventLogo;
	IBOutlet UIView *headerView;
	IBOutlet UITableView *baseTable;
	NSArray *_commentsList;
}

@property (nonatomic, strong) NSArray *commentsList;
@end

@implementation ConversationViewController

@synthesize commentsList = _commentsList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
		[self setViewTitle:@"CONVERSATION"];
	
		[headerView.layer setCornerRadius:5.0];
		headerView.layer.borderWidth = 0.3;
		[headerView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
		[eventLogo.layer setCornerRadius:eventLogo.frame.size.width/2.0];

	eventName.text = GLOBAL.selectedLogModal.logTopic;
	eventCount.text = GLOBAL.selectedLogModal.resultValue;
	eventLogo.backgroundColor = [UIColor colorWithHexString:GLOBAL.selectedLogModal.logColor];

	// Do any additional setup after loading the view.
}


- (void) viewWillAppear:(BOOL)animated {
	[self.tabBarController.tabBar setTintColor:COLOR_APP_GREEN];
	if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(didDownloadData:)
																								 name:N_LOG_COMMENTS object:nil];
		[APIMANAGER startContentForAPI:APITypeLogComments withSearchQuery:nil];
	}
}
- (void) didDownloadData:(NSNotification*) notification {
	[GLOBAL removeActivity:self.view];
	self.commentsList = (NSArray *) [notification object];
	[baseTable reloadData];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[self.view endEditing:YES];
	[baseTable endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView setBackgroundColor:[UIColor clearColor]];
	if (indexPath.row == 0) {
		return 80.0f;
	} else if (indexPath.row == 1) {
			return 175.0f;
	} else {
		return 135.0f + 10;
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_commentsList count] + 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellID = @"CellIdent";
	
	if (indexPath.row == 0) {
		NSString *cellIDdummy = @"CellIdentDummy";
		UITableViewCell *cellDummy = [tableView dequeueReusableCellWithIdentifier:cellIDdummy];
		if (cellDummy == nil) {
			cellDummy = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDdummy];
			[cellDummy.contentView setBackgroundColor:[UIColor clearColor]];
			[cellDummy setBackgroundColor:[UIColor clearColor]];
			cellDummy.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
		}
		return cellDummy;
	} else  if (indexPath.row == 1){
		NewConversationCell *newConversationCell = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (newConversationCell == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"NewConversationCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			newConversationCell = [loadedViews lastObject];
			[newConversationCell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[newConversationCell.contentView setBackgroundColor:[UIColor clearColor]];
			[newConversationCell setBackgroundColor:[UIColor clearColor]];
		}
		newConversationCell.tableView = tableView;

		return newConversationCell;
	} else {
		ConversationThreadCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
		if (cell == nil) {
			UINib* loadedNib = [UINib nibWithNibName:@"ConversationThreadCell" bundle:nil];
			NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
			cell = [loadedViews lastObject];
			[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[cell.contentView setBackgroundColor:[UIColor clearColor]];
			[cell setBackgroundColor:[UIColor clearColor]];
		}
		[cell setLogCommentsModel:[_commentsList objectAtIndex:indexPath.row-2]];
		return cell;
	}

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[self.view endEditing:YES];
	[baseTable endEditing:YES];
}


@end
