//
//  NewConversation.h
//  EveryLog
//
//  Created by Kamlesh on 09/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewConversationCell : UITableViewCell <UITextViewDelegate>

@property (nonatomic,strong) UITableView *tableView;
@end
