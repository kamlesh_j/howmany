//
//  RankingCell.h
//  EveryLog
//
//  Created by Kamlesh on 09/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogFeedModal.h"

@interface RankingCell : UITableViewCell {
	LogFeedModal *_logFeedModal;
}

@property (nonatomic, strong) LogFeedModal *logFeedModal;

@end
