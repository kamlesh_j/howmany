//
//  EventsViewController.m
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "EventsViewController.h"
#import "EventCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Global.h"
#import "LogResultModal.h"
#import "CustomActionSheet.h"
#import "UIColor+Support.h"

@interface EventsViewController () {
	IBOutlet UILabel *eventCount;
	IBOutlet UILabel *eventName;
	IBOutlet UIImageView *eventLogo;
	IBOutlet UIView *headerView;
	IBOutlet UITableView *baseTable;
	NSArray *_logResultList;
	EventCell *_eventEventEntryCell;
}
@property (nonatomic, strong) EventCell *eventEventEntryCell;
@property (nonatomic, strong) NSArray *logResultList;
@end

@implementation EventsViewController

@synthesize logResultList = _logResultList;
@synthesize eventEventEntryCell = _eventEventEntryCell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
	[self.tabBarController.tabBar setTintColor:COLOR_NAV];
    
	eventName.text = GLOBAL.selectedLogModal.logTopic;
	eventCount.text = GLOBAL.selectedLogModal.resultValue;
	eventLogo.backgroundColor = [UIColor colorWithHexString:GLOBAL.selectedLogModal.logColor];
    
	UIImage *selecetdImg = [UIImage imageNamed:@"icon_mainnav_logs_sel.png"];
	[self.navigationController.tabBarItem setSelectedImage:selecetdImg];
    
    [self refreshData];
}

- (void) refreshData {
    if (GLOBAL.userInfo != nil) {
		[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
		[[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didDownloadData:)
                                                     name:N_LOGRESULT_DOWNLOADED object:nil];
		[APIMANAGER startContentForAPI:APITypeResults withSearchQuery:nil];
	}
}
- (void) didDownloadData:(NSNotification*) notification {
	[GLOBAL removeActivity:self.view];
	self.logResultList = (NSArray *) [notification object];
	if ([_logResultList count]) {
		[baseTable reloadData];
	}
}


- (void) displayPhotoPicker:(UITableViewCell *) eventEventEntryCell {
	self.eventEventEntryCell = (EventCell*)eventEventEntryCell;
	CustomActionSheet *actionSheet = [[CustomActionSheet alloc] initWithTitle:@"" delegate:self
                                                            cancelButtonTitle:@"Cancel"
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:
                                      @"Take Photo",
                                      @"Choose existng", nil];
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
	actionSheet = nil;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	NSString *selectedOption = [actionSheet buttonTitleAtIndex:buttonIndex];
	
	if ([selectedOption isEqualToString:@"Take Photo"]) {
		UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
	} else if ([selectedOption isEqualToString:@"Choose existng"]) {
		UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
	}
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	//self.imageView.image = chosenImage;
	[_eventEventEntryCell setSelectedLogImage:info[UIImagePickerControllerEditedImage]];
	[picker dismissViewControllerAnimated:YES completion:NULL];
	
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	
	[picker dismissViewControllerAnimated:YES completion:NULL];
	
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self.tabBarController.tabBar setTintColor:COLOR_NAV];
    
	[self setViewTitle:@"EVENTS"];
    
	[headerView.layer setCornerRadius:5.0];
	headerView.layer.borderWidth = 0.3;
	[headerView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    
	[eventLogo.layer setCornerRadius:eventLogo.frame.size.width/2.0];
	
	[eventLogo setImage:[UIImage imageNamed:@"badgeTest"]];
	eventLogo.contentMode = UIViewContentModeCenter;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logEntryDeleted:)
                                                 name:N_LOGRESULT_DELETED object:nil];
	// Do any additional setup after loading the view.
}

- (void) logEntryDeleted:(NSNotification *) notification {
    BOOL isSuccess = [[notification object] boolValue];
    if (isSuccess) {
        [self refreshData];
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	EventCell* eventCell = (EventCell*)[baseTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
	if ([eventCell respondsToSelector:@selector(resignFirstResponderForTxt)]) {
		[eventCell resignFirstResponderForTxt];
	}
}

#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView setBackgroundColor:[UIColor clearColor]];
	if (indexPath.row == 0) {
		return 80.0f;
	} else if (indexPath.row == 1) {
		return 220.0f;
	} else {
		return 183.0;
	}
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_logResultList count] + 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellID = @"CellIdent";
    
	if (indexPath.row == 0) {
		NSString *cellIDdummy = @"CellIdentDummy";
		UITableViewCell *cellDummy = [tableView dequeueReusableCellWithIdentifier:cellIDdummy];
		if (cellDummy == nil) {
			cellDummy = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDdummy];
			[cellDummy.contentView setBackgroundColor:[UIColor clearColor]];
			[cellDummy setBackgroundColor:[UIColor clearColor]];
		}
		return cellDummy;
	} else {
        
		EventCell *cell = nil;
		if (indexPath.row == 1) {
			NSString *cellIdEntry = @"CellIdentEntry";
            
			cell = [tableView dequeueReusableCellWithIdentifier:cellIdEntry];
			
			if (cell == nil) {
				UINib* loadedNib = [UINib nibWithNibName:@"EventCell" bundle:nil];
				NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
				cell = [loadedViews lastObject];
                [cell setValue:cellIdEntry forKey:@"reuseIdentifier"];
			}
			
			LogResultModal *logResultModal = [_logResultList objectAtIndex:0];
			
			[cell setLogResultModal:logResultModal];
			__weak EventCell *cellTemp = cell;
			[cell setDisplayDateBlock:^{
				[self displayDatePicker:cellTemp];
			}];
			[cell displayEventEntry];
			cell.parentViewController = self;
		} else {
			cell = [tableView dequeueReusableCellWithIdentifier:cellID];
			
			if (cell == nil) {
				UINib* loadedNib = [UINib nibWithNibName:@"EventCell" bundle:nil];
				NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
				cell = [loadedViews lastObject];
				[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
				[cell.contentView setBackgroundColor:[UIColor clearColor]];
				[cell setBackgroundColor:[UIColor clearColor]];
			}
			
			[cell displayEvent];
			LogResultModal *logResultModal = [_logResultList objectAtIndex:indexPath.row - 2];
			[cell setLogResultModal:logResultModal];
		}
		
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		[cell.contentView setBackgroundColor:[UIColor clearColor]];
		[cell setBackgroundColor:[UIColor clearColor]];
        
		
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void) displayDatePicker:(EventCell *) eventCell {
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
