//
//  LoginViewController.m
//  EveryLog
//
//  Created by Kamlesh on 11/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LoginViewController.h"
#import "CustomButton.h"
#import "Global.h"

@interface LoginViewController () {
	
	IBOutlet UITextField *emailTxt;
	IBOutlet UITextField *passwordTxt;
	IBOutlet CustomButton *loginBtn;
	IBOutlet CustomButton *fbLoginBtn;
}
- (IBAction)signInWithFacebook:(id)sender;
- (IBAction)signInBtnAction:(id)sender;
- (IBAction)forgotPasswordAction:(id)sender;
@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setTitleForNav:@"LOG IN"];
	
	[loginBtn.layer setCornerRadius:5.0];
	[fbLoginBtn.layer setCornerRadius:5.0];
	// Do any additional setup after loading the view.
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[emailTxt resignFirstResponder];
	[passwordTxt resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signInBtnAction:(id)sender {
//	UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"Oops!!" message:@"Work in progress" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//	[alerView show];
//	alerView = nil;
//	[self dismissViewControllerAnimated:YES completion:^{}];

	NSString *errorMsg = nil;
//	if (![nonNilString(emailTxt.text) isEqualToString:@""]) {
//		errorMsg = @"Please enter a valid email.";
//	} else if (![nonNilString(passwordTxt.text) isEqualToString:@""]) {
//		errorMsg = @"Please enter a valid password.";
//	} else {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin)
																								 name:N_DIDLOGIN object:nil];
		
		[APIMANAGER doLogin:emailTxt.text andPassword:passwordTxt.text];
		return;
//	}
	
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!" message:errorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alertView show];
	alertView = nil;
}

- (void) didLogin {
	[self dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)forgotPasswordAction:(id)sender {
}

#pragma mark -
#pragma Facebook 
// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin)
																							 name:N_DIDLOGIN object:nil];

	[APIMANAGER fbLogin:user.id and:[FBSession activeSession].accessTokenData.accessToken];

	NSLog(@"Fb Login success...");
}

// Handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
  NSString *alertMessage, *alertTitle;
	
  // If the user should perform an action outside of you app to recover,
  // the SDK will provide a message for the user, you just need to surface it.
  // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
  if ([FBErrorUtility shouldNotifyUserForError:error]) {
    alertTitle = @"Facebook error";
    alertMessage = [FBErrorUtility userMessageForError:error];
		
		// This code will handle session closures that happen outside of the app
		// You can take a look at our error handling guide to know more about it
		// https://developers.facebook.com/docs/ios/errors
  } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
    alertTitle = @"Session Error";
    alertMessage = @"Your current session is no longer valid. Please log in again.";
		
    // If the user has cancelled a login, we will do nothing.
    // You can also choose to show the user a message if cancelling login will result in
    // the user not being able to complete a task they had initiated in your app
    // (like accessing FB-stored information or posting to Facebook)
  } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
    NSLog(@"user cancelled login");
		
    // For simplicity, this sample handles other errors with a generic message
    // You can checkout our error handling guide for more detailed information
    // https://developers.facebook.com/docs/ios/errors
  } else {
    alertTitle  = @"Something went wrong";
    alertMessage = @"Please try again later.";
    NSLog(@"Unexpected error:%@", error);
  }
	
  if (alertMessage) {
    [[[UIAlertView alloc] initWithTitle:alertTitle
                                message:alertMessage
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
  }
}

@end
