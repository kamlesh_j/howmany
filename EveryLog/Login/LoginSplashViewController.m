//
//  LoginSplashViewController.m
//  EveryLog
//
//  Created by Kamlesh on 11/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LoginSplashViewController.h"
#import "Global.h"

#define NOOFPAGES 3

@interface LoginSplashViewController () {
	
	IBOutlet UIPageControl *pageViewControl;
	IBOutlet UIButton *loginBtn;
	IBOutlet UIButton *signUpBtn;
	IBOutlet UIView *loginView;
	IBOutlet UIScrollView *baseScrollView;
}

@end

@implementation LoginSplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[loginView.layer setCornerRadius:5.0];
	[signUpBtn.layer setCornerRadius:5.0];
	[loginBtn.layer setCornerRadius:5.0];
	
	for (int index = 0; index < NOOFPAGES; index++) {
		UIImageView *imageView = [[UIImageView alloc] init];
		if (FRAMECONSTANTS.isIphone5) {
			[imageView setImage:[UIImage imageNamed:@"login-568"]];
			[imageView setFrame:CGRectMake(index*FRAMECONSTANTS.appWidth, - 20, FRAMECONSTANTS.appWidth, FRAMECONSTANTS.appHeight)];
		} else {
			[imageView setImage:[UIImage imageNamed:@"login"]];
			[imageView setFrame:CGRectMake(index*FRAMECONSTANTS.appWidth , -20, FRAMECONSTANTS.appWidth, FRAMECONSTANTS.appHeight)];
		}
		[baseScrollView addSubview:imageView];
	}
	
	[baseScrollView setContentSize:CGSizeMake(NOOFPAGES*FRAMECONSTANTS.appWidth, FRAMECONSTANTS.appHeight - 20)];
	[pageViewControl setNumberOfPages:NOOFPAGES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	int pageNo = scrollView.contentOffset.x? scrollView.contentOffset.x/FRAMECONSTANTS.appWidth : scrollView.contentOffset.x;
	[pageViewControl setCurrentPage:pageNo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
