//
//  LoginRootViewController.m
//  EveryLog
//
//  Created by Kamlesh on 11/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "LoginRootViewController.h"
#import "Global.h"

@interface LoginRootViewController () {
	UILabel *titleLbl;
}

@end

@implementation LoginRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void) setTitleForNav:(NSString *) titleStr {
	[titleLbl setText:[titleStr uppercaseString]];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self.view setBackgroundColor:COLOR_BG];
	UIView* navBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, FRAMECONSTANTS.appWidth, FRAMECONSTANTS.navHeight)];
	[navBar setBackgroundColor:COLOR_NAV];
	[self.view addSubview:navBar];
	
	titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, FRAMECONSTANTS.appWidth - 2 * 60, FRAMECONSTANTS.navHeight - 20)];
	[titleLbl setTextAlignment:NSTextAlignmentCenter];
	[titleLbl setTextColor:[UIColor whiteColor]];
	[titleLbl setFont:[UIFont fontWithName:FONT_LIGHT size:35.0]];
	[navBar addSubview:titleLbl];
	
	//new_log.png
	//menu-icon.png
	UIImage *buttonImage = [UIImage imageNamed:@"backWhite"];
	UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[backBtn addTarget:self action:@selector(backBtnAction) forControlEvents:UIControlEventTouchUpInside];
	[backBtn setFrame:CGRectMake(0, 20 + (50 - 44)/2, 44, 44)];
	[backBtn setImage:buttonImage forState:UIControlStateNormal];
	[navBar addSubview:backBtn];
	
	UIViewController *rootViewCnt = [[self.navigationController viewControllers] firstObject];
	
	if ([rootViewCnt isEqual:self]) {
		[backBtn setHidden:YES];
	}
	// Do any additional setup after loading the view.
}

-(void) backBtnAction {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
