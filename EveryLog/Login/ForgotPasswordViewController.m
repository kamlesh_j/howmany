 //
//  ForgotPasswordViewController.m
//  EveryLog
//
//  Created by Kamlesh on 11/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "CustomButton.h"

@interface ForgotPasswordViewController () {
	
	IBOutlet UITextField *emailTxt;
	IBOutlet CustomButton *emailBtn;
}
- (IBAction)emailBtnAction:(id)sender;

@end

@implementation ForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[emailTxt resignFirstResponder];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
		[self setTitleForNav:@"FORGOT PASSWORD"];
	[emailBtn.layer setCornerRadius:5.0];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)emailBtnAction:(id)sender {
}
@end
