//
//  LoginViewController.h
//  EveryLog
//
//  Created by Kamlesh on 11/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginRootViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface LoginViewController : LoginRootViewController <FBLoginViewDelegate>

@end
