//
//  SignUpViewController.m
//  EveryLog
//
//  Created by Kamlesh on 11/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "SignUpViewController.h"
#import "CustomButton.h"
#import "Global.h"

@interface SignUpViewController () {
	
	IBOutlet CustomButton *facebookLoginBtn;
	IBOutlet CustomButton *signUpBtn;
	IBOutlet UITextField *passwordTxt;
	IBOutlet UITextField *emailTxt;
	IBOutlet UITextField *firstNameTxt;
	IBOutlet UITextField *lastNameTxt;
	IBOutlet UITextField *zipcodeTxt;
	IBOutlet UIButton *birthdateBtn;
	IBOutlet UISegmentedControl *genderToggle;
}
- (IBAction)facebookLoginAction:(id)sender;
- (IBAction)signUpBtnaction:(id)sender;

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[emailTxt resignFirstResponder];
	[passwordTxt resignFirstResponder];
	[firstNameTxt resignFirstResponder];
	[lastNameTxt resignFirstResponder];
	[zipcodeTxt resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[facebookLoginBtn.layer setCornerRadius:5.0];
		[signUpBtn.layer setCornerRadius:5.0];
		[self setTitleForNav:@"SIGN UP"];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)facebookLoginAction:(id)sender {
	UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"Oops!!" message:@"Work in progress" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alerView show];
	alerView = nil;
	[self dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)signUpBtnaction:(id)sender {
		
		__block NSString *urlString = [URL_SERVER stringByAppendingString:URL_REGISTRATION];
		
		NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
		[params setObject:firstNameTxt.text forKey:@"firstName"];
		[params setObject:lastNameTxt.text forKey:@"lastName"];
		[params setObject:emailTxt.text forKey:@"emailAddress"];
		[params setObject:passwordTxt.text forKey:@"password"];
		[params setObject:emailTxt.text forKey:@"userName"];
	if (genderToggle.selectedSegmentIndex == 0) {
    [params setObject:@"MALE" forKey:@"gender"];
	} else {
		[params setObject:@"FEMALE" forKey:@"gender"];
	}
		
		[params setObject:zipcodeTxt.text forKey:@"zipCode"];
		[params setObject:@"1986-03-18" forKey:@"dateOfBirth"];
		
	//urlString = [GLOBAL setParameterFor:urlString andDict:params];
		
		
		NSURL *url = [NSURL URLWithString:urlString];
		
		NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
																																cachePolicy:NSURLRequestReloadIgnoringCacheData
																														timeoutInterval:30.0];
		
		NSError *error = nil;
		NSData *paraData = [NSJSONSerialization dataWithJSONObject:params
																											 options:NSJSONWritingPrettyPrinted
																												 error:&error];
		
		NSString *length = [NSString stringWithFormat:@"%lu", (unsigned long)[paraData length]];
		
		[request setHTTPMethod:@"PUT"];
		[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
		[request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
		[request setValue:length forHTTPHeaderField:@"Content-Length"];
		[request setHTTPBody:paraData];
		
		//no cookies.
		[request setHTTPShouldHandleCookies:NO];
		

	[GLOBAL addActivityTo:self.view with:UIActivityIndicatorViewStyleGray];
	NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
		
		if (data) {
			NSError *error= nil;
			NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
																																	 options:NSJSONReadingMutableContainers
																																		 error:&error];
			
			BOOL isSuccess = [[responseDict objectForKey:@"success"] boolValue];

			if (isSuccess) {
				dispatch_async(dispatch_get_main_queue(), ^{
					UIAlertView* alerView = [[UIAlertView alloc] initWithTitle:@""
																														 message:@"A verification email has been sent to your email address provided. Please verify your email and tap on OK."
																														delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
					[alerView show];
					[GLOBAL removeActivity:self.view];
				});
			}
		}
	}];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if (buttonIndex == 0) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin)
																								 name:N_DIDLOGIN object:nil];
		
		[APIMANAGER doLogin:emailTxt.text andPassword:passwordTxt.text];

	} else {
		[self dismissViewControllerAnimated:YES completion:^{}];
	}	
}

- (void) didLogin {
	[self dismissViewControllerAnimated:YES completion:^{}];
}

@end
