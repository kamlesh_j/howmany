//
//  DefinedConstants.h
//  EveryLog
//
//  Created by Kamlesh on 14/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#ifndef EveryLog_DefinedConstants_h
#define EveryLog_DefinedConstants_h

static id nonNilString(id value) { return value ? value : @"";}


typedef enum {
  APITypeLogin = 1,
  APITypeLogsList,
	APITypeLogFeed,
	APITypeLogggers,
	APITypeLogggersSearch,
	APITypeResults,
	APITypeLogChart,
	APITypeLoggersForLog,
	APITypeLogComments,
	APITypeJoin,
	APITypeFavouriteUser,
	APITypeDeleteLog
} APIType;

// APILinks
#define URL_SERVER @"http://iphone.everylog.com/EveryLog-Core/"
#define URL_LOGIN @"services/user/login?"
#define URL_REGISTRATION @"services/user/register"

#define URL_FB_LOGIN @"services/user/facebook?"

//http://www.everylog.com/EveryLog-Core/services/user/facebook?userID=1156405403&fbAccessToken=CAACOxlZApvI4BAJZCUsZBr1fjxeuTDaMdx0ZBtyJ1xnY1zqDuMNqfwYyjlBocTOTOCWXmI5g8iK2DVVw1D11ezKHlZBRuz5Af9yMiatNjh9nfiBZAjhF3YTRD1UlqZA7h0saUY5Vy8vB17lOzvpRXmIZCmZBFaRdxugaXOZAbjASSXharcjaMHCJvGGGZANWWsr5Q7aYpgCMXZBwAgZDZD

#define URL_LOGFEEDS @"services/log/list?"
#define URL_LOGLIST @"services/log/me?"

#define URL_LOGFEED_SEARCH @"services/log/search?"
#define URL_LOGGERS @"services/user/me/loggers?"
#define URL_LOGGERS_SEARCH @"services/user/search?"
///services/user/search?userId=528d1b89e4b05fa307ce69ba&query=joe&end=0&contextOf=528d1b89e4b05fa307ce69ba&ts=1397944396340

#define URL_LOGGERS_FOR_LOG @"services/user/loggersforlog?"
#define URL_LOGDETAILS @"services/result/logResult?"
#define URL_LOG_IMAGE_RESULT @"services/result/imageResults?"
#define URL_LOG_COMMENTS @"services/log/comments?"
#define URL_ADD_COMMENT @"services/log/logComment"

#define URL_LOG_CHART @"/services/result/me/log?"
///services/result/me/log?
//userId=528d1b89e4b05fa307ce69ba&logId=50a7f82be4b08a4ce29d9758&end=0&contextOf=528d1b89e4b05fa307ce69ba&ts=1396581174949

//resultId=53584cb0e4b021703e4e9224&userId=528d1b89e4b05fa307ce69ba&logId=518ed84ae4b0f38d6608e18e
#define URL_DELETE_LOG_ENTRY @"/services/result/deleteResult?"
#define URL_ADD_LOG @"services/result/add"
#define URL_LOG_JOIN @"/services/log/join"
#define URL_FAVOURITE_URER @"services/user/markFavorite"
#define URL_FAVOURITE_LOG @"services/log/favorite"
#define URL_DELETE_LOG @"services/log/deleteLog?"
//services/log/deleteLog?logId=5309345ee4b0a13be3db1470&userId=528d1b89e4b05fa307ce69ba

#define URL_CREATE_LOG @"services/log/addUpdate"
#define URL_UPLOAD_IMG @"services/image/uploadIMG?mode=2"
//userId=528d1b89e4b05fa307ce69ba&logId=50a7f82be4b08a4ce29d9758&end=0&contextOf=528d1b89e4b05fa307ce69ba&ts=1391666632028

#define URL_ISMEMBER @"services/log/isMember?"

#define URL_MINIPROFILE @"/services/user/miniprofile"

// /services/user/miniprofile/528d1b89e4b05fa307ce69ba?ts=1391378746732

// log/search?mode=logfeed&ts=1390863209&end=0&query=movie&userId=528d1b89e4b05fa307ce69ba&contextOf=528d1b89e4b05fa307ce69ba
// /log/search?mode=logfeed&userId=528d1b89e4b05fa307ce69ba&query=movies&end=0&contextOf=528d1b89e4b05fa307ce69ba&ts=1390861705625
//http://www.everylog.com/EveryLog-Core/services/log/recent?ts=1390859790957
//http://www.everylog.com/EveryLog-Core/services/log/me?userId=528d1b89e4b05fa307ce69ba&end=0&contextOf=528d1b89e4b05fa307ce69ba&ts=1390859809673


#define URL_IMG_USERIMG @"services/image/userImage?imageName="
#define URL_IMG_RESULTIMG @"services/image/resultImage?imageName="


// http://iphone.everylog.com/EveryLog-Core/services/image/userImage?imageName=52b8b348e4b0d3fbfb53cff6.jpg&ts=1390713347384

//function getProfileImagePath(imageUrl){
//	return SERVER_URL+ "services/image/userImage?imageName="+imageUrl + "&ts=" + PHOTO_VERSION_FLAG;
//}
//
//function getResultImagePath(imageUrl){
//	return SERVER_URL+ "services/image/resultImage?imageName="+imageUrl ;//+ "&ts=" + new Date().getTime();
//}

#define FB_APP_ID @"156982504438926" //"493614353988280"
#define LIST_LOAD_INTERVAL 1*24*60*60*1000 //1 day

//////////////////////////////// KEYS ////////////////////////////////////////////////////

//#define K_USERID @"UserID"
//#define K_AUTHTOKEN @"AuthToken"
//#define K_USERNAME @"UserName"
//#define K_FIRSTNAME @"firstName"
//#define K_LASTNAME @"lstName"

#define K_UserInfo @"UserInfoData"

//////////////////////////////// NOTIFICATIONS ////////////////////////////////////////////////////

#define N_DIDLOGIN @"DidLogin"
#define N_LOGIN_FAILED @"LoginFailed"

#define N_LOGS_DOWNLOADED @"LogsDownloaded"
#define N_LOGFEED_DOWNLOADED @"LogFeedDownloaded"
#define N_LOGGERS_DOWNLOADED @"LoggersDownloaded"
#define N_SHOW_MENU @"N_SHOW_MENU"

#define N_LOGRESULT_DELETED @"N_LOGRESULT_DELETED"
#define N_LOGRESULT_DOWNLOADED @"N_LOGRESULT_DOWNLOADED"
#define N_LOGGER_FOR_LOG @"N_LOGGER_FOR_LOG"
#define N_LOG_COMMENTS @"N_LOG_COMMENTS"
#define N_LOG_DELETE @"N_LOG_DELETE"
#define N_LOG_CHART @"N_LOG_CHART"
#define N_SHOW_LOGIN @"N_SHOW_LOGIN"
////////////////////////////////////////////////////////////////////////////////////


#define COLOR_NAV [UIColor colorWithRed:(241.0/255.0) green:(75.0/255.0) blue:(56.0/255.0) alpha:1.0]
#define COLOR_BG [UIColor colorWithRed:(240.0/255.0) green:(238.0/255.0) blue:(228.0/255.0) alpha:1.0]

#define COLOR_APP_GREEN [UIColor colorWithRed:(106.0/255.0) green:(168.0/255.0) blue:(79.0/255.0) alpha:1.0]


#define FONT_REGULAR @"ProximaNovaSoft-Regular"
#define FONT_LIGHT @"RamaGothicE-Light"
#define FONT_MEDIUM @"RamaGothicM-Thin"

#endif
