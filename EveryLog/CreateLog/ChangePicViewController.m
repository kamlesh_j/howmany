//
//  ChangePicViewController.m
//  EveryLog
//
//  Created by Kamlesh on 05/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "ChangePicViewController.h"
#import "CustomScrollView.h"
#import "Global.h"

@interface ChangePicViewController () {
	
	IBOutlet UIButton *changePicBtn;
	IBOutlet UIButton *skipBtn;
	IBOutlet UIButton *lockBtn;
	IBOutlet UIButton *starBtn;
	IBOutlet UILabel *logCount;
	IBOutlet UILabel *logTitle;
	IBOutlet UIImageView *logIcon;
	IBOutlet UIImageView *logBanner;
	IBOutlet CustomScrollView *baseScrollView;
	IBOutlet UIView *baseView;
}
- (IBAction)skipBtnAction:(id)sender;
- (IBAction)changePicAction:(id)sender;

@end

@implementation ChangePicViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	//	if ([[segue identifier] isEqualToString:@"LogSettings"])
	//	{
	//		LogSettingsViewController* logBadgeViewController = [segue destinationViewController];
	//		logBadgeViewController.logModal = _logModal;
	//	}
}

- (void) keyboardWillShow {
	
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	scrollViewFrame.size.height -= FRAMECONSTANTS.keyboardHeight;
	[baseScrollView setFrame:scrollViewFrame];
	
	if (FRAMECONSTANTS.isIphone5 == NO) {
		[baseScrollView setContentOffset:CGPointMake(0, 60) animated:YES];
	}
}

- (void) keyboardWillHide {
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	[baseScrollView setFrame:scrollViewFrame];
	
	[baseScrollView setContentSize:CGSizeMake(FRAMECONSTANTS.appWidth, CGRectGetMaxY(skipBtn.frame) + 20)];
}


- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self setNavTitle:@"CREATE A LOG"];

	[self keyboardWillHide];
	
	logTitle.text = [self logFeedModal].logTopic;
	[logIcon.layer setCornerRadius:logIcon.frame.size.width/2.0];
	
	[baseScrollView setContentOffset:CGPointMake(0,0)];
	[baseView.layer setCornerRadius:5.0];
	[baseView.layer setBorderWidth:1.0];
	[baseView.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];
	
	[changePicBtn.layer setCornerRadius:5.0];
	[changePicBtn.layer setBorderWidth:1.0];
	[changePicBtn.layer setBorderColor:[changePicBtn currentTitleColor].CGColor];
	

	[skipBtn.layer setCornerRadius:5.0];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow)
																							 name:UIKeyboardWillShowNotification
																						 object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide)
																							 name:UIKeyboardWillHideNotification
																						 object:nil];
	
}

- (IBAction)skipBtnAction:(id)sender {
	
}
- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}





@end
