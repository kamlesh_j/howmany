//
//  CreateLogRoot.h
//  EveryLog
//
//  Created by Kamlesh on 25/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogFeedModal.h"

@interface CreateLogRoot : UIViewController 

-(void) setNavTitle:(NSString *) navTitle;

- (LogFeedModal *) logFeedModal;

@end
