//
//  AuthorizeDeviceCell.m
//  EveryLog
//
//  Created by Kamlesh on 16/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "AuthorizeDeviceCell.h"


@interface AuthorizeDeviceCell () {
	
	IBOutlet UIView *deviceIcon;
	IBOutlet UIView *baseView;
}
- (IBAction)authorizeAction:(id)sender;
- (IBAction)cancelBtnAction:(id)sender;
@end

@implementation AuthorizeDeviceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) drawRect:(CGRect)rect {
	[baseView.layer setCornerRadius:5.0];
	[deviceIcon.layer setCornerRadius:deviceIcon.frame.size.width/2.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)authorizeAction:(id)sender {
}

- (IBAction)cancelBtnAction:(id)sender {
}
@end
