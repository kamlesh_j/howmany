//
//  AuthorizeDeviceViewController.m
//  EveryLog
//
//  Created by Kamlesh on 16/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "AuthorizeDeviceViewController.h"
#import "AuthorizeDeviceCell.h"

@interface AuthorizeDeviceViewController () {
	
	IBOutlet UIButton *doneBtn;
}
- (IBAction)doneBtnAction:(id)sender;

@end

@implementation AuthorizeDeviceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setNavTitle:@"CREATE AN AUTOLOG"];
	[doneBtn.layer setCornerRadius:5.0];
	// Do any additional setup after loading the view.
}

#pragma mark -
#pragma mark Tableview
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView setBackgroundColor:[UIColor clearColor]];
	return 210;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellID = @"CellIdent";
	
	AuthorizeDeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
	if (cell == nil) {
		UINib* loadedNib = [UINib nibWithNibName:@"AuthorizeDeviceCell" bundle:nil];
		NSArray *loadedViews = [loadedNib instantiateWithOwner:self options:nil];
		cell = [loadedViews lastObject];
		[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
		[cell.contentView setBackgroundColor:[UIColor clearColor]];
		[cell setBackgroundColor:[UIColor clearColor]];
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
}


- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)doneBtnAction:(id)sender {
}
@end
