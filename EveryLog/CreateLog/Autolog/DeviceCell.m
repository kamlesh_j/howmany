//
//  DeviceCell.m
//  EveryLog
//
//  Created by Kamlesh on 16/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "DeviceCell.h"

@interface DeviceCell () {

	IBOutlet UIView *baseView;
	IBOutlet UILabel *deviceNameLbl;
	IBOutlet UIView *deviceIcon;
	IBOutlet UIButton *checkBoxBtn;
}

- (IBAction)checkBoxAction:(id)sender;

@end

@implementation DeviceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) drawRect:(CGRect)rect {
	[super drawRect:rect];
	
	[baseView.layer setCornerRadius:5.0];
	[deviceIcon.layer setCornerRadius:CGRectGetWidth(deviceIcon.frame)/2.0];
	
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)checkBoxAction:(id)sender {
}
@end
