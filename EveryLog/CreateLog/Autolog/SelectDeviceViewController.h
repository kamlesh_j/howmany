//
//  SelectDeviceViewController.h
//  EveryLog
//
//  Created by Kamlesh on 15/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateLogRoot.h"

@interface SelectDeviceViewController : CreateLogRoot <UITableViewDataSource, UITableViewDelegate>

@end
