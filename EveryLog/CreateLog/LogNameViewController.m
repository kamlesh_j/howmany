//
//  LogNameViewController.m
//  EveryLog
//
//  Created by Kamlesh on 23/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LogNameViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Global.h"
#import "LogBadgeViewController.h"

@interface LogNameViewController () {
	
	IBOutlet UIImageView *iconImg;
	IBOutlet UIButton *nextBtn;
	IBOutlet UITextField *logNameTxt;
	IBOutlet UIScrollView *baseScrollview;
	IBOutlet UIView *baseView;
}
- (IBAction)nextBtnAction:(id)sender;

@end

@implementation LogNameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setNavTitle:@"CREATE A LOG"];

	[self keyboardWillHide];
	
	[iconImg.layer setCornerRadius:iconImg.frame.size.width/2.0];
	[nextBtn.layer setCornerRadius:5.0];
	[baseScrollview setContentSize:self.view.frame.size];
	[baseScrollview setContentOffset:CGPointMake(0, 0)];
	[baseView.layer setCornerRadius:5.0];
	[baseView.layer setBorderWidth:1.0];
	[baseView.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];

	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow)
																							 name:UIKeyboardDidShowNotification
																						 object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide)
																							 name:UIKeyboardWillHideNotification
																						 object:nil];
// Do any additional setup after loading the view.
}

- (void) keyboardWillShow {
		
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	scrollViewFrame.size.height -= 220;
	[baseScrollview setFrame:scrollViewFrame];
	
	[baseScrollview setContentSize:baseScrollview.frame.size];
	
	if (FRAMECONSTANTS.isIphone5 == NO) {
		[baseScrollview setContentOffset:CGPointMake(0, 50) animated:YES];
	}
}

- (void) keyboardWillHide {
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	[baseScrollview setFrame:scrollViewFrame];
	[baseScrollview setContentSize:baseScrollview.frame.size];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[logNameTxt resignFirstResponder];
}
- (void) viewWillAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"LogBadgeSeque"])
	{
		[self logFeedModal].logTopic = logNameTxt.text;
		LogBadgeViewController* logBadgeViewController = [segue destinationViewController];
	}
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
	if ([identifier isEqualToString:@"LogBadgeSeque"]) {		
		if (logNameTxt.text == nil || [logNameTxt.text isEqualToString:@""]) {
			return NO;
		} else {
			return YES;
		}
	}
	return YES;
}

- (IBAction) nextBtnAction:(UIButton*) sender {
	NSLog(@"");
	NSLog(@"");
}

@end
