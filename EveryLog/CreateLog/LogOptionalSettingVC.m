//
//  LogOptionalSettingVC.m
//  EveryLog
//
//  Created by Kamlesh on 25/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LogOptionalSettingVC.h"
#import "CustomScrollView.h"
#import "Global.h"
#import "InviteFriendsViewController.h"
#import "CustomActionSheet.h"

@interface LogOptionalSettingVC () {
	
	IBOutlet UIButton *nextBtn;
	IBOutlet UIButton *field2Btn;
	IBOutlet UITextField *field2Txt;
	IBOutlet UIButton *field1Btn;
	IBOutlet UITextField *field1Txt;
	IBOutlet UIButton *lockBtn;
	IBOutlet UIButton *starBtn;
	IBOutlet UILabel *countLbl;
	IBOutlet UILabel *logNameLbl;
	IBOutlet UIImageView *logIcon;
	IBOutlet UIImageView *logBG;
	IBOutlet UIView *baseView;
	IBOutlet CustomScrollView *baseScrollView;
}

- (IBAction)nextBtnAction:(id)sender;
- (IBAction)extraField1Action:(id)sender;
- (IBAction)extraField2Action:(id)sender;

@end

@implementation LogOptionalSettingVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"LogSetInviteFriends"]) {
		//		InviteFriendsViewController* inviteFriendsViewController = [segue destinationViewController];
	}
}

- (void) keyboardWillShow {
	
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	scrollViewFrame.size.height -= FRAMECONSTANTS.keyboardHeight;
	[baseScrollView setFrame:scrollViewFrame];
	
	//	[baseScrollView setContentSize:baseScrollView.frame.size];
	
	if (FRAMECONSTANTS.isIphone5 == NO) {
		[baseScrollView setContentOffset:CGPointMake(0, 60) animated:YES];
	}
}

- (void) keyboardWillHide {
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	[baseScrollView setFrame:scrollViewFrame];
	
	[baseScrollView setContentSize:CGSizeMake(FRAMECONSTANTS.appWidth, CGRectGetMaxY(nextBtn.frame) + 20)];
}


- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setNavTitle:@"CREATE A LOG"];

	[self keyboardWillHide];
	
	logNameLbl.text = [self logFeedModal].logTopic;
	//	[logIcon setBackgroundColor:[UIColor colorwi]]
	[logIcon.layer setCornerRadius:logIcon.frame.size.width/2.0];
	
	[baseScrollView setContentOffset:CGPointMake(0, 0)];
	[baseView.layer setCornerRadius:5.0];
	[baseView.layer setBorderWidth:1.0];
	[baseView.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];
	
	[nextBtn.layer setCornerRadius:5.0];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow)
																							 name:UIKeyboardWillShowNotification
																						 object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide)
																							 name:UIKeyboardWillHideNotification
																						 object:nil];
	
	[field1Btn.layer setCornerRadius:5.0];
	[field1Btn setBackgroundColor:[UIColor whiteColor]];
	[field1Btn.layer setBorderWidth:1.0];
	[field1Btn.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];

	[field2Btn.layer setCornerRadius:5.0];
	[field2Btn setBackgroundColor:[UIColor whiteColor]];
	[field2Btn.layer setBorderWidth:1.0];
	[field2Btn.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#define TAG_DATA1_TYPE 300
#define TAG_DATA2_TYPE 302


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	int actionSheetTag = actionSheet.tag;
	
	NSString *selectedOption = [actionSheet buttonTitleAtIndex:buttonIndex];
	
	if (actionSheetTag == TAG_DATA1_TYPE) {
		[field1Btn setTitle:selectedOption forState:UIControlStateNormal];
	} else if (actionSheetTag == TAG_DATA2_TYPE) {
		[field2Btn setTitle:selectedOption forState:UIControlStateNormal];
	}
}

- (IBAction)nextBtnAction:(id)sender {
	
}
- (IBAction)extraField1Action:(id)sender {
	CustomActionSheet *actionSheet = [[CustomActionSheet alloc] initWithTitle:@"What type of Info will you log?" delegate:self
																									cancelButtonTitle:@"Cancel"
																						 destructiveButtonTitle:nil
																
																									otherButtonTitles:
																@"Words (Italy, Bon Jovi)",
																@"Numbers (42, 567)",
																@"Currency ($5.00, $81.30)",
																@"Time (12:45, 01:25)", nil];
	actionSheet.tag = TAG_DATA1_TYPE;
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
	actionSheet = nil;
}

- (IBAction)extraField2Action:(id)sender {
	CustomActionSheet *actionSheet = [[CustomActionSheet alloc] initWithTitle:@"What type of Info will you log?" delegate:self
																									cancelButtonTitle:@"Cancel"
																						 destructiveButtonTitle:nil
																
																									otherButtonTitles:
																@"Words (Italy, Bon Jovi)",
																@"Numbers (42, 567)",
																@"Currency ($5.00, $81.30)",
																@"Time (12:45, 01:25)", nil];
	actionSheet.tag = TAG_DATA2_TYPE;
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
	actionSheet = nil;
}
@end
