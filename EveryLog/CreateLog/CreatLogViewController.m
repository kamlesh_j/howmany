//
//  CreatLogViewController.m
//  EveryLog
//
//  Created by Kamlesh on 15/01/14.
//  Copyright (c) 2014 Kamlesh. All rights reserved.
//

#import "CreatLogViewController.h"
#import "CustomScrollView.h"
#import "Global.h"

@interface CreatLogViewController () {
	
	IBOutlet CustomScrollView *baseScrollView;
	IBOutlet UIView *logView;
	IBOutlet UIView *autoLogView;
	IBOutlet UIButton *createLogBtn;
	IBOutlet UIButton *createAutoLogBtn;
}
- (IBAction)createLogAction:(id)sender;
- (IBAction)createAutoLogAction:(id)sender;

@end

@implementation CreatLogViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self setNavTitle:@"CREATE A LOG"];

	[logView.layer setCornerRadius:5.0];
	[autoLogView.layer setCornerRadius:5.0];
	[createLogBtn.layer setCornerRadius:5.0];
	[createAutoLogBtn.layer setCornerRadius:5.0];
	
	[baseScrollView setContentSize:CGSizeMake(FRAMECONSTANTS.appWidth, CGRectGetMaxY(autoLogView.frame))];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)createLogAction:(id)sender {
}

- (IBAction)createAutoLogAction:(id)sender {
}
@end
