//
//  LogSettingsViewController.m
//  EveryLog
//
//  Created by Kamlesh on 25/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LogSettingsViewController.h"
#import "CustomScrollView.h"
#import "Global.h"
#import "UIColor+Support.h"
#import "LogOptionalSettingVC.h"
#import "InviteFriendsViewController.h"
#import "CustomActionSheet.h"

@interface LogSettingsViewController () {
	IBOutlet UIButton *nextBtn;
	IBOutlet UISearchBar *searchBar;
	IBOutlet UILabel *logNameLbl;
	IBOutlet UIImageView *badgeView;
	IBOutlet UIView *baseView;
	IBOutlet CustomScrollView *baseScrollView;
	
	IBOutlet UIButton *chooseDataTypeBtn;
	IBOutlet UIButton *dataSummarizeBtn;
	IBOutlet UIButton *logTypeBtn;
	IBOutlet UIButton *tagBtn;

}
- (IBAction)lockBtnAction:(id)sender;
- (IBAction)dataTypeAction:(id)sender;
- (IBAction)dataSumarizeAction:(id)sender;
- (IBAction)logType:(id)sender;
- (IBAction)logTagAction:(id)sender;

@end

@implementation LogSettingsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"OptionalLogSettings"])
	{
		//		LogOptionalSettingVC* logOptionalSettingVC = [segue destinationViewController];
	} else if ([[segue identifier] isEqualToString:@"LogSetInviteFriends"]) {
		//		InviteFriendsViewController* inviteFriendsViewController = [segue destinationViewController];
	}
}

- (void) keyboardWillShow {
	
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	scrollViewFrame.size.height -= FRAMECONSTANTS.keyboardHeight;
	[baseScrollView setFrame:scrollViewFrame];
	
	//	[baseScrollView setContentSize:baseScrollView.frame.size];
	
	if (FRAMECONSTANTS.isIphone5 == NO) {
		[baseScrollView setContentOffset:CGPointMake(0, 60) animated:YES];
	}
}

- (void) keyboardWillHide {
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	[baseScrollView setFrame:scrollViewFrame];
	
	[baseScrollView setContentSize:CGSizeMake(FRAMECONSTANTS.appWidth, CGRectGetMaxY(nextBtn.frame) + 20)];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[searchBar resignFirstResponder];
}
- (void)viewDidLoad
{
	[self logFeedModal].logUnit = @"TEXT";
	[self logFeedModal].logMeasurement = @"TOTAL";
	[self logFeedModal].type = @"COMPETI";

	// TODO: Remove Hardcoded
	[self logFeedModal].visibility = @"PRIVATELOG";
	[self logFeedModal].tags = [NSMutableArray array];//@[ @"Animals", @"Art & Design"];

	[[self logFeedModal].tags addObject:@"Animals"];
	[[self logFeedModal].tags addObject:@"Art & Design"];
	//
	
	[super viewDidLoad];
	
	[self setNavTitle:@"CREATE A LOG"];

	[self keyboardWillHide];
	
	logNameLbl.text = [self logFeedModal].logTopic;
	[badgeView.layer setCornerRadius:badgeView.frame.size.width/2.0];
	
	[baseScrollView setContentOffset:CGPointMake(0, 0)];
	[baseView.layer setCornerRadius:5.0];
	[baseView.layer setBorderWidth:1.0];
	[baseView.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];
	
	[badgeView setBackgroundColor:[UIColor colorWithHexString:[self logFeedModal].logColor]];
	
	[nextBtn.layer setCornerRadius:5.0];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow)
																							 name:UIKeyboardWillShowNotification
																						 object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide)
																							 name:UIKeyboardWillHideNotification
																						 object:nil];
	
	
	[chooseDataTypeBtn.layer setCornerRadius:5.0];
	[chooseDataTypeBtn setBackgroundColor:[UIColor whiteColor]];
	[chooseDataTypeBtn.layer setBorderWidth:1.0];
	[chooseDataTypeBtn.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];
	
	[dataSummarizeBtn.layer setCornerRadius:5.0];
		[dataSummarizeBtn setBackgroundColor:[UIColor whiteColor]];
	[dataSummarizeBtn.layer setBorderWidth:1.0];
	[dataSummarizeBtn.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];

	[logTypeBtn.layer setCornerRadius:5.0];
		[logTypeBtn setBackgroundColor:[UIColor whiteColor]];
	[logTypeBtn.layer setBorderWidth:1.0];
	[logTypeBtn.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];

	[tagBtn.layer setCornerRadius:5.0];
		[tagBtn setBackgroundColor:[UIColor whiteColor]];
	[tagBtn.layer setBorderWidth:1.0];
	[tagBtn.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];
	
	// Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#define TAG_DATE_TYPE 300
#define TAG_SUMMARIZE 400
#define TAG_LOG_TYPE 311
#define TAG_TAG 500

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	NSInteger actionSheetTag = actionSheet.tag;
	
	NSString *selectedOption = [actionSheet buttonTitleAtIndex:buttonIndex];
	
	if (actionSheetTag == TAG_DATE_TYPE) {
		[chooseDataTypeBtn setTitle:selectedOption forState:UIControlStateNormal];
		/*
		 @"Text (Italy, Bon Jovi)",
		 @"Numbers (42, 567)",
		 @"Percentage (10%, 80%)",
		 @"Currency ($5.00, $81.30)",
		 @"Time (12:45, 01:25)",
		 */
		
		if (buttonIndex == 0) {
			[self logFeedModal].logUnit = @"TEXT";
		}else if (buttonIndex == 1) {
			[self logFeedModal].logUnit = [@"NUMBER" uppercaseString];
		}else if (buttonIndex == 2) {
			[self logFeedModal].logUnit = @"PERCENTAGE";
		}else if (buttonIndex == 3) {
			[self logFeedModal].logUnit = @"CURRENCY";
		}else if (buttonIndex == 4) {
			[self logFeedModal].logUnit = @"TIME";
		}
	} else if (actionSheetTag == TAG_SUMMARIZE) {
		[dataSummarizeBtn setTitle:selectedOption forState:UIControlStateNormal];
		/*
		 @"Total (this is the most common)",
		 @"Count",
		 @"Minimum",
		 @"Maximum",
		 @"Lowest Average",
		 @"Highest Average"
		 */
		if (buttonIndex == 0) {
			[self logFeedModal].logMeasurement = @"TOTAL";
		} else if (buttonIndex == 1) {
			[self logFeedModal].logMeasurement = @"COUNT";
		} else if (buttonIndex == 2) {
			[self logFeedModal].logMeasurement = @"MINIMUM";
		} else if (buttonIndex == 3) {
			[self logFeedModal].logMeasurement = @"MAXIMUM";
		} else if (buttonIndex == 4) {
			[self logFeedModal].logMeasurement = @"LOWESTAVERAGE";
		} else if (buttonIndex == 5) {
			[self logFeedModal].logMeasurement = @"HIGHESTAVERAGE";
		}
	} else if (actionSheetTag == TAG_LOG_TYPE) {
		if (buttonIndex == 0) {
			[self logFeedModal].type = @"COMPETI";
		} else if (buttonIndex == 1) {
			[self logFeedModal].type = @"TEAMLOG";
		} else if (buttonIndex == 2) {
			[self logFeedModal].type = @"PERSONAL";
		}
		[logTypeBtn setTitle:selectedOption forState:UIControlStateNormal];
	} else if (actionSheetTag == TAG_TAG) {
		[tagBtn setTitle:selectedOption forState:UIControlStateNormal];
	}
	
}

- (IBAction)lockBtnAction:(id)sender {
}

- (IBAction)dataTypeAction:(id)sender {
	CustomActionSheet *actionSheet = [[CustomActionSheet alloc] initWithTitle:@"What type of Info will you log?" delegate:self
																									cancelButtonTitle:@"Cancel"
																						 destructiveButtonTitle:nil
																									
																									otherButtonTitles:
																@"Text (Italy, Bon Jovi)",
																@"Numbers (42, 567)",
																@"Percentage (10%, 80%)",
																@"Currency ($5.00, $81.30)",
																@"Time (12:45, 01:25)", nil];
	actionSheet.tag = TAG_DATE_TYPE;
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
	actionSheet = nil;
}


- (IBAction)dataSumarizeAction:(id)sender {
	CustomActionSheet *actionSheet = [[CustomActionSheet alloc] initWithTitle:@"How should the info be displayed?" delegate:self
																									cancelButtonTitle:@"Cancel"
																						 destructiveButtonTitle:nil
																									otherButtonTitles:
																@"Total (this is the most common)",
																@"Count",
																@"Minimum",
																@"Maximum",
																@"Lowest Average",
																@"Highest Average",nil];
	actionSheet.tag = TAG_SUMMARIZE;
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
	actionSheet = nil;
}


- (IBAction)logType:(id)sender {
	CustomActionSheet *actionSheet = [[CustomActionSheet alloc] initWithTitle:@"Choose the type of Log" delegate:self
																									cancelButtonTitle:@"Cancel"
																						 destructiveButtonTitle:nil
																
																									otherButtonTitles:
																@"Competilog (to compete)",
																@"Teamlog (to team up)",
																@"Log (for yourself)", nil];
	actionSheet.tag = TAG_LOG_TYPE;
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
	actionSheet = nil;
}


- (IBAction)logTagAction:(id)sender {
	CustomActionSheet *actionSheet = [[CustomActionSheet alloc] initWithTitle:@"Choose at least one tag" delegate:self
																									cancelButtonTitle:@"Cancel"
																						 destructiveButtonTitle:nil
																
																									otherButtonTitles:
																@"Choose one", nil];
	actionSheet.tag = TAG_TAG;
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
	actionSheet = nil;
}
@end
