//
//  LogBadgeColorViewController.m
//  EveryLog
//
//  Created by Kamlesh on 24/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "LogBadgeColorViewController.h"
#import "CustomScrollView.h"
#import "Global.h"
#import "BadgeUtilityView.h"
#import "UIColor+Support.h"
#import "LogSettingsViewController.h"

@interface LogBadgeColorViewController () {
	IBOutlet UIButton *nextBtn;
	IBOutlet BadgeUtilityView *badgeUtil;
	IBOutlet UISearchBar *searchBar;
	IBOutlet UILabel *logNameLbl;
	IBOutlet UIImageView *badgeView;
	IBOutlet UIView *baseView;
	IBOutlet CustomScrollView *baseScrollView;
}

@end

@implementation LogBadgeColorViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"LogSettings"])
	{
	}
}

- (void) keyboardWillShow {
	
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	scrollViewFrame.size.height -= FRAMECONSTANTS.keyboardHeight;
	[baseScrollView setFrame:scrollViewFrame];
	
	//	[baseScrollView setContentSize:baseScrollView.frame.size];
	
	if (FRAMECONSTANTS.isIphone5 == NO) {
		[baseScrollView setContentOffset:CGPointMake(0, 60) animated:YES];
	}
}

- (void) keyboardWillHide {
	CGRect scrollViewFrame = FRAMECONSTANTS.viewFrame;
	[baseScrollView setFrame:scrollViewFrame];
	
	[baseScrollView setContentSize:CGSizeMake(FRAMECONSTANTS.appWidth, CGRectGetMaxY(nextBtn.frame) + 20)];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[searchBar resignFirstResponder];
}
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setNavTitle:@"CREATE A LOG"];

	[self keyboardWillHide];
	
	logNameLbl.text = [self logFeedModal].logTopic;
	[badgeView.layer setCornerRadius:badgeView.frame.size.width/2.0];
	
	[baseScrollView setContentOffset:CGPointMake(0, 0)];
	[baseView.layer setCornerRadius:5.0];
	[baseView.layer setBorderWidth:1.0];
	[baseView.layer setBorderColor:[UIColor colorWithRed:(223.0/255.0) green:(223.0/255.0) blue:(223.0/255.0) alpha:1.0].CGColor];
	
	[badgeUtil initializeBadgeWithColor:^(int colorIndex) {
		NSString *settingPath = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
		NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:settingPath];
		NSArray *colorList = [settings objectForKey:@"BadgeColor"];
		if (colorIndex <= [colorList count]) {
			NSString *colorString = [colorList objectAtIndex:colorIndex];
			[self logFeedModal].logColor = [UIColor getColorHexFromString:colorString];
			[badgeView setBackgroundColor:[UIColor colorWithHexFromString:colorString]];
		}
	}];
	[nextBtn.layer setCornerRadius:5.0];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow)
																							 name:UIKeyboardWillShowNotification
																						 object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide)
																							 name:UIKeyboardWillHideNotification
																						 object:nil];
	
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
