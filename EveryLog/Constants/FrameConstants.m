//
//  FrameConstants.m
//  EveryLog
//
//  Created by Kamlesh on 18/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import "FrameConstants.h"

@implementation FrameConstants

@synthesize appHeight;
@synthesize appWidth;
@synthesize navHeight;
@synthesize isIphone5;
@synthesize keyboardHeight;
@synthesize viewFrame;
@synthesize openMenuWidth;

+(FrameConstants *)sharedFrameConstants {
	static FrameConstants *frameConstats = nil;
	if ( frameConstats == nil ) {
		frameConstats = [[FrameConstants alloc] init];
		
		// TODO: Remove hard coded call to method
		[frameConstats loadConstantsForPhone];
	}
	return frameConstats;
}

- (void) commonConstants {
	UIScreen * screen = [UIScreen mainScreen];
	self.appWidth = CGRectGetWidth([screen bounds]);
	self.appHeight = CGRectGetHeight([screen bounds]);
	self.navHeight = 50 + 20;
	
	self.keyboardHeight = 215;
	
	self.openMenuWidth = 55.0;
}
- (void) loadConstantsForPhone {
	[self commonConstants];
	if (self.appHeight > 480) {
		self.isIphone5 = YES;
	} else {
		self.isIphone5 = NO;
	}
	
	int height = self.appHeight - self.navHeight;
	
	self.viewFrame = CGRectMake(0, self.navHeight, self.appHeight, height);

}

@end
