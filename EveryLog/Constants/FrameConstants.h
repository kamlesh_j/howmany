//
//  FrameConstants.h
//  EveryLog
//
//  Created by Kamlesh on 18/12/13.
//  Copyright (c) 2013 Kamlesh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FrameConstants : NSObject {
	float appWidth;
	float appHeight;
	float navHeight;
	BOOL isIphone5;
	float keyboardHeight;
	CGRect viewFrame;
	float openMenuWidth;
}

@property (nonatomic) float openMenuWidth;
@property (nonatomic) CGRect viewFrame;
@property (nonatomic) float keyboardHeight;
@property (nonatomic) BOOL isIphone5;
@property (nonatomic) float appWidth;
@property (nonatomic) float appHeight;
@property (nonatomic) float navHeight;


+(FrameConstants *)sharedFrameConstants;

@end
